module.exports = {
  projects: ["<rootDir>/src"],
  collectCoverageFrom: [
    "<rootDir>/src/**/*.{ts,tsx}",
    "!<rootDir>/src/**/*.{d.ts}",
    "!<rootDir>/src/types/*.d.ts",
    "!<rootDir>/src/config.ts",
    "!<rootDir>/src/index.ts"
  ],
  testRegex: "\\.test\\.ts",
  testPathIgnorePatterns: ["/node_modules/", "<rootDir>/tsclib/"],
  testURL: "http://localhost/",
  testEnvironment: "jsdom",
  clearMocks: true,
  preset: "ts-jest",
  verbose: true,
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1"
  }
};
