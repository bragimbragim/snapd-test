import {ConnectionOptions} from "typeorm";

export default {
    type: "sqlite",
    // database: ":memory:",
    database: "./database.sqlite",
    synchronize: true,
    logging: false,
    entities: ["src/entity/**/*.ts"],
    migrations: ["src/migrations/**/*.ts"],
} as ConnectionOptions;
