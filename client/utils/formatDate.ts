export function getDateWithoutTimezone(value: string): Date {
    const date = new Date(value);
    const userTimezoneOffset = Math.abs(date.getTimezoneOffset() * 60000);
    return new Date(date.getTime() - userTimezoneOffset);
}

export function formatDate(value: string): string {

    const date = getDateWithoutTimezone(value);

    const formatter = new Intl.DateTimeFormat(undefined, {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    });

    return formatter.format(date);
}
