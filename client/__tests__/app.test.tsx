import * as React from "react";
import {render} from "@testing-library/react";
import App from '../App';

describe(`
    Given <App/>
    When rendering component 
    Then should be render without crashing`, () => {
    it('simple', () => {
        const {container} = render(<App/>);
        expect(container).toBeInstanceOf(HTMLElement);
    })
})
