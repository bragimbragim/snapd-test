import {IUser} from '../shared/types/IUser';
import {UserRole} from '../shared/types/UserRole';

class UserStorage {

    constructor() {
    }

    set(user: IUser) {
        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('isLoggedIn', 'true');
    }

    remove() {
        localStorage.removeItem('user');
        localStorage.removeItem('isLoggedIn');
    }

    getUser(): IUser {
        return JSON.parse(localStorage.getItem('user'));
    }

    getRole(): UserRole {
        return this.getUser()?.role;
    }
}

export const userStorage = new UserStorage();
