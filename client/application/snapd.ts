import {UserRole} from '../shared/types/UserRole';
import {ISideBarListItem} from '../shared/types/ISideBarListItem';
import {RouterLinks} from '../shared/types/RouterLinks';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import SettingsIcon from '@material-ui/icons/Settings';
import {userStorage} from './UserStorage';

class Snapd {

    getMenuItems(): ISideBarListItem[] {
        const defaultMenuItems: ISideBarListItem[] = [
            {
                caption: 'Photo Gallery',
                icon: PhotoLibraryIcon,
                link: RouterLinks.GALLERY
            }
        ];
        const role = userStorage.getRole();
        switch (role) {
            case UserRole.ADMIN:
                return [
                    ...defaultMenuItems,
                    {
                        caption: 'Snapd Config',
                        icon: SettingsIcon,
                        link: RouterLinks.CONFIG
                    }
                ];
            case UserRole.USER:
                return defaultMenuItems;
            default:
                return defaultMenuItems;
        }
    }

}

export const snapd = new Snapd();
