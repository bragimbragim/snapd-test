import axios, {CancelToken} from 'axios';
import {CheckPhotoRes} from '../../../globalTypes/CheckPhotosRes';
import {ICameraConfig} from '../../../globalTypes/ICameraConfig';
import {IServiceStatus} from '../../../globalTypes/ServiceStatus';

export class CameraAPI {

    constructor() {
    }

    reloadCamera(): Promise<IServiceStatus | Error> {
        return axios
            .post<IServiceStatus>('camera/reload')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    checkCamera(): Promise<any> {
        return axios
            .post('camera/check')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    startVideoServer(): Promise<any> {
        return axios
            .post('video/server/start')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getCameraOptions(): Promise<ICameraConfig> {
        return axios
            .get<ICameraConfig>('camera/options')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    setCameraOptions(options: ICameraConfig): Promise<string> {
        return axios
            .post<string>('camera/options', options)
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

}
