import {AuthAPI} from './auth';
import {CameraAPI} from './camera';
import {ReaderAPI} from './reader';
import {GalleryAPI} from './gallery';
import {TagsAPI} from './tags';
import {SystemAPI} from './system';
import {VideoAPI} from './video';
import {AmazonAPI} from './amazon';
import {MemoryAPI} from './memory';
import {MqttAPI} from './mqtt';
import {LogsAPI} from './logs';
import {WifiAPI} from './wifi';

export const api = {
    auth: new AuthAPI(),
    camera: new CameraAPI(),
    video: new VideoAPI(),
    reader: new ReaderAPI(),
    gallery: new GalleryAPI(),
    tags: new TagsAPI(),
    logs: new LogsAPI(),
    system: new SystemAPI(),
    amazon: new AmazonAPI(),
    memory: new MemoryAPI(),
    mqtt: new MqttAPI(),
    wifi: new WifiAPI()
}
