import axios from 'axios';

export class TagsAPI {

    constructor() {
    }

    getTags(): Promise<any> {
        return axios
            .get<any>('log/tags')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }
}
