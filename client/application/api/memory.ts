import axios from 'axios';
import {MemoryData} from '../../../globalTypes/MemoryData';
import {IServiceStatus} from '../../../globalTypes/ServiceStatus';

export class MemoryAPI {

    constructor() {
    }

    reloadService(): Promise<IServiceStatus | Error> {
        return axios
            .post<IServiceStatus>('memory/reload')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getData(): Promise<MemoryData> {
        return axios
            .get<MemoryData>('memory/data')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

}
