import axios from 'axios';
import {IServiceStatus} from '../../../globalTypes/ServiceStatus';

export class MqttAPI {

    constructor() {
    }

    reloadService(): Promise<IServiceStatus | Error> {
        return axios
            .post<IServiceStatus>('mqtt/reload')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

}
