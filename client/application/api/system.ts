import axios from 'axios';
import {SystemDate} from '../../../globalTypes/SystemDate';
import {ISnapdServices} from '../../../globalTypes/ISnapdServices';
import {ISnapdConfig} from '../../../globalTypes/ISnapdConfig';

export class SystemAPI {

    constructor() {
    }

    getSystemTime(): Promise<string> {
        return axios
            .get<string>('time')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err));
    }

    setSystemTime(date: SystemDate): Promise<string> {
        return axios
            .post<string>('time', {date})
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    rebootRaspberrypi() {
        return axios.post('reboot')
    }

    powerOffRaspberrypi() {
        return axios.post('poweroff');
    }

    getServicesStatus(): Promise<ISnapdServices> {
        return axios
            .get('services/status')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getUnitName() {
        return axios
            .get<ISnapdConfig>('unitname/get')
            .then(({data}) => Promise.resolve(data.unitName))
            .catch((err: string) => Promise.reject(err))
    }

    setUnitName(unitName: string) {
        return axios
            .post('unitname/set', {unitName})
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }
}
