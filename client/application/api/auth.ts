import {ISignUpReq} from '../../shared/types/ISignUpReq';
import {ISignUpRes} from '../../shared/types/ISignUpRes';
import axios from 'axios';
import {ISignInReq} from '../../shared/types/ISignInReq';
import {ISignInRes} from '../../shared/types/ISignInRes';
import {LogoutReason} from '../../shared/types/LogoutReason';
import history from '../../utils/history';
import {userStorage} from '../UserStorage';

export class AuthAPI {

    constructor() {
    }

    signUp(requestBody: ISignUpReq): Promise<ISignUpRes> {
        return axios
            .post<ISignUpRes>('auth/signup', requestBody)
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    signIn(requestBody: ISignInReq): Promise<ISignInRes> {
        return axios
            .post<ISignInRes>('auth/signin', requestBody)
            .then(({data}) => {
                userStorage.set(data.user);
                return Promise.resolve(data);
            })
            .catch((err: string) => Promise.reject(err))
    }

    logout(reason: LogoutReason = LogoutReason.NO_REASON): Promise<void> {
        return axios
            .get('auth/logout')
            .then(() => {
                userStorage.remove();
                const queryParams = (reason !== LogoutReason.NO_REASON)
                                    ? `?reason=${reason}&backUrl=${window.location.pathname}`
                                    : '';
                history.push({pathname: '/login', search: queryParams})
                return Promise.resolve();
            })
            .catch((err: string) => Promise.reject(err));
    }

}
