import {ReaderMode} from '../../../globalTypes/ReaderMode';
import {ReaderStatus} from '../../../globalTypes/ReaderStatus';
import axios from 'axios';
import {ReaderControlCommand} from '../../../globalTypes/ReaderControlCommand';
import {IReaderConfig} from '../../../globalTypes/IReaderConfig';

export class ReaderAPI {

    constructor() {
    }

    enable(mode: ReaderMode): Promise<ReaderStatus> {
        return axios
            .post<ReaderStatus>('reader/enable', {mode})
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    toggleReading(control: ReaderControlCommand): Promise<ReaderStatus> {
        return axios
            .post<ReaderStatus>('/reader/toggle', {control})
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    reload(): Promise<ReaderStatus> {
        return axios
            .post<ReaderStatus>('reader/reload')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getStatus(): Promise<ReaderStatus> {
        return axios
            .get<ReaderStatus>('reader/status')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getCurrConfig(): Promise<IReaderConfig> {
        return axios
            .get<IReaderConfig>('reader/options')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    setOptions(options: IReaderConfig): Promise<string> {
        return axios
            .post<string>('reader/options', options)
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err));
    }

}
