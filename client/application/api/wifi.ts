import axios from 'axios';
import {
    IWifiItemResponse,
    IWifiRequest
} from '../../../globalTypes/IWifi';

export class WifiAPI {

    constructor() {
    }

    connectToNewNetwork(request: IWifiRequest): Promise<string> {
        return axios
            .post('wifi/connect/new', request)
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    connectToSavedNetwork(ssid: string): Promise<string> {
        return axios
            .post('wifi/connect/saved', {ssid})
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    removeNetwork(item: IWifiItemResponse): Promise<string> {
        return axios
            .post('wifi/remove', {ssid: item.ssid})
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getListOfNetwork(): Promise<IWifiItemResponse[] | Error> {
        return axios
            .get<IWifiItemResponse[]>('wifi/list')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getConnectionStatus(): Promise<boolean> {
        return axios
            .get('wifi/connection/status')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }
}
