import axios from 'axios';
import {IAmazonConfig} from '../../../globalTypes/IAmazonConfig';
import {IAmazonServiceData} from '../../../globalTypes/IAmazonServiceData';
import {IServiceStatus} from '../../../globalTypes/ServiceStatus';

export class AmazonAPI {

    constructor() {
    }

    reloadService(): Promise<IServiceStatus | Error> {
        return axios
            .post<IServiceStatus>('amazon/reload')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getData(): Promise<IAmazonServiceData | Error> {
        return axios
            .get('/amazon/service/data')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    clearQueue(): Promise<string | Error> {
        return axios
            .post('/amazon/queue/clear')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getOptions(): Promise<IAmazonConfig | Error> {
        return axios
            .get('/amazon/options')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    setOptions(body: IAmazonConfig): Promise<string | Error> {
        return axios
            .post('/amazon/options', body)
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

}
