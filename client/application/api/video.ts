import axios, {CancelToken} from 'axios';

export class VideoAPI {

    constructor() {
    }

    startVideoServer(): Promise<any> {
        return axios
            .post('video/server/start')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    stopVideoServer(): Promise<any> {
        return axios
            .post('video/server/stop')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    getShutterData(): Promise<any> {
        return axios
            .get('video/server/data')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

}
