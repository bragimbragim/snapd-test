import {UserRole} from '../../shared/types/UserRole';
import axios from 'axios';
import {IPhoto} from '../../shared/types/IPhoto';
import config from '../../config';
import {formatDate} from '../../utils/formatDate';
import {IPhotoGallery} from '../../routes/PhotoGallery/types/IPhotoGallery';

export class GalleryAPI {

    constructor() {
    }

    getPhotos(role: UserRole = UserRole.USER): Promise<IPhotoGallery[]> {

        let mapFn = this.mapUserPhotos;
        let url = 'gallery/photos';

        if (role === UserRole.ADMIN) {
            url = 'gallery/photos/all';
            mapFn = this.mapAdminPhotos;
        }
        return axios
            .get<IPhoto[]>(url)
            .then(({data}) => data.map(mapFn))
            .then((data) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    downloadPhotos(photos: IPhoto[]) {
        return axios
            .post(`gallery/photos/download`, {photos}, {
                responseType: 'blob',
                headers: {'Content-Type': 'application/json'}
            })
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    protected mapUserPhotos(photo: IPhoto): IPhotoGallery {
        return {
            ...photo,
            src: `${config.API_URL}/api/gallery/photo/${photo.id}`,
            thumbnail: `${config.API_URL}/api/gallery/photo/${photo.id}/thumbnail`,
            thumbnailWidth: 600,
            thumbnailHeight: 400,
            tags: [{value: formatDate(photo.date), title: formatDate(photo.date)}],
            caption: formatDate(photo.date)
        }
    }

    protected mapAdminPhotos(photo: IPhoto): IPhotoGallery {
        return {
            ...photo,
            src: `${config.API_URL}/api/gallery/photo/${photo.id}`,
            thumbnail: `${config.API_URL}/api/gallery/photo/${photo.id}/thumbnail`,
            thumbnailWidth: 600,
            thumbnailHeight: 400,
            tags: [
                {value: `Tags: ${photo.tagName}`, title: `Tag: ${photo.tagName}`}
                // {value: `User email: ${photo.userEmail}`, title: `User email: ${photo.userEmail}`}
            ],
            caption: formatDate(photo.date)
        }
    }

    deleteTestPhotos(): Promise<string> {
        return axios
            .post<string>('gallery/photo/check/clear')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err));
    }

    deletePhotos(photos: IPhoto[]) {

        const requests = [];
        let currRequest = [];

        for (let i = 0; i < photos.length; i++) {
            currRequest.push(photos[i]);
            if (currRequest.length >= 30) {
                requests.push(axios.post<string>('gallery/photos/delete', {photos: currRequest}));
                currRequest = [];
            }
        }
        requests.push(axios.post<string>('gallery/photos/delete', {photos: currRequest}));

        return axios
            .all(requests)
            .then(axios.spread((...responses) => {
                return Promise.resolve(responses[0].data);
            })).catch(errors => {
                return Promise.reject(errors)
            })
    }

    deleteAllPhotosFromUnit(): Promise<string> {
        return axios
            .post<string>('gallery/photos/delete/all')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

}
