import axios from 'axios';

export class LogsAPI {

    constructor() {
    }

    getLog(limit: number): Promise<any> {
        return axios
            .get('log', {params: {limit}})
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }

    deleteLog(): Promise<string> {
        return axios
            .post('log/delete')
            .then(({data}) => Promise.resolve(data))
            .catch((err: string) => Promise.reject(err))
    }
}
