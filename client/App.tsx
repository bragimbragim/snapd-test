import React, {ReactElement} from 'react';
import store from './state/store';
import {MuiThemeProvider, ThemeProvider} from '@material-ui/core';
import theme from './theme';
import SnackBarBox from './shared/components/SnackBarBox/component';
import {Provider} from 'react-redux';
import Router from './routes';
import CssBaseline from '@material-ui/core/CssBaseline';
import {DebugModal} from './shared/components/debug/debug';
export default function App(): ReactElement {

    return (
        <Provider store={store}>
            <MuiThemeProvider theme={theme}>
                <ThemeProvider theme={theme}>
                    <CssBaseline/>
                    <Router/>
                    <SnackBarBox/>
                    <DebugModal/>
                </ThemeProvider>
            </MuiThemeProvider>
        </Provider>
    )
}
