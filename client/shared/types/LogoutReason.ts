export enum LogoutReason {
    NO_REASON,
    SESSION_EXPIRED,
    UPDATE_SYSTEM_TIME
}
