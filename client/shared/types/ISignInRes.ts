import {IUser} from './IUser';

export interface ISignInRes {
    user: IUser;
}
