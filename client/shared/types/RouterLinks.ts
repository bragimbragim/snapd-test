export enum RouterLinks {
    LOGIN = '/login',
    REGISTRATION = '/registration',
    GALLERY = '/gallery',
    MANAGE_TAGS = '/tags',
    ACCOUNT_INFO = '/account',
    ACCOUNT_LIST = '/account-list',
    CONFIG = '/config'
}

