import {IUser} from './IUser';

export interface ISignUpRes {
    user: IUser;
}
