export interface ISignUpReq {
    tagId: string;
    email: string;
    password: string;
}
