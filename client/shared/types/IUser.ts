import {UserRole} from './UserRole';

export interface IUser {
    username: string;
    tagId: string;
    email: string;
    role: UserRole;
}
