
export interface IGallery {
    src: string;
    thumbnail: string;
    thumbnailWidth?: number;
    thumbnailHeight?: number;
    thumbnailCaption?: string;
    isSelected?: boolean;
    tags?: any[];
    caption?: string;
    nano?: string;
}
