export interface ISignInReq {
    login: string;
    password: string;
}
