import {ElementType} from 'react';

export interface ISideBarListItem {
    caption: string;
    link: string;
    icon: ElementType;
}
