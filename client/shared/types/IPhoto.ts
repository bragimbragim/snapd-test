export interface IPhoto {
    id?: number;
    date?: string;
    path?: string;
    name?: string;
    tagName?: string;
    userEmail?: string;
}
