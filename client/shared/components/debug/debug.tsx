import React, {useEffect, useState} from "react";
import {openModal} from '../../../state/modal';
import {ConfirmDialog} from '../ConfirmDialog/component';
import {useDispatch} from 'react-redux';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import {Box, Typography} from '@material-ui/core';
import {Alert} from '@material-ui/lab';
import DialogContent from '@material-ui/core/DialogContent';

let keyWord: string[] = 'debug'.split('');
let currWord: string[] = keyWord;
let counter: number = keyWord.length;

export function DebugModal() {

    const dispatch = useDispatch();

    useEffect(() => {
        document.addEventListener('keydown', (event) => {
            if (event.key === currWord[0]) {
                counter -= 1;
                if (!counter) {
                    openConfirm();
                }
                currWord = currWord.filter((letter) => letter !== event.key);

                return;
            }
            counter = keyWord.length;
            currWord = keyWord;
        })
    })

    const openConfirm = () => {
        dispatch(
            openModal(
                <DialogContent
                    style={{width: '500px', height: '400px'}}
                >IT IS A DEBUG MODE</DialogContent>
            )
        );
    }

    return <>
    </>
}
