import React from "react";
import {Button, CircularProgress} from '@material-ui/core';
import {ICircularButtonProps} from './types/ICircularButton';
import useStyles from './styles';

export function CircularButton(
    {
        loading,
        children,
        ...rest
    }: ICircularButtonProps
) {

    const classes = useStyles();

    return <>
        <Button{...rest}>
            {children}
            {loading ? <CircularProgress size={24} className={classes.btnSpinner}/> : null}
        </Button>
    </>
}
