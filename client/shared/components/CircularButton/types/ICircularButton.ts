import {IDefaultComponentProps} from '../../../types/IDefaultComponentProps';
import {ButtonProps} from '@material-ui/core';

export interface ICircularButtonProps extends IDefaultComponentProps, ButtonProps {
    loading: boolean;
}
