import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {green} from '@material-ui/core/colors';

export default makeStyles((theme: Theme) =>
    createStyles({
        btnSpinner: {
            color: green[600],
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: -12,
            marginLeft: -12,
        }
    })
);
