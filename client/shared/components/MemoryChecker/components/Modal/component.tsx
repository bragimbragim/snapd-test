import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import {useHistory} from 'react-router-dom';

export default function MemoryCheckerDialog() {

    const [open, setOpen] = React.useState(true);

    const history = useHistory();

    const goToMemoryTab = () => {
        setOpen(false);
        localStorage.setItem('activeConfigTab', '4');
        history.push('/config');
    };

    return (
        <div>
            <Dialog
                open={open}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">You should clear your memory</DialogTitle>
                <DialogActions>
                    <Button onClick={goToMemoryTab} color="primary" autoFocus>
                        Go To Memory Tab
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
