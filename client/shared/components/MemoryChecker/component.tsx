import React, {useEffect} from "react";
import {useSelector} from 'react-redux';
import MemoryCheckerDialog from './components/Modal/component';
import {getMemoryData} from '../../../state/services/memory/selectors';

export function MemoryChecker() {

    const memory = useSelector(getMemoryData);

    return <>
        {memory.warning !== null && <MemoryCheckerDialog/>}
    </>
}
