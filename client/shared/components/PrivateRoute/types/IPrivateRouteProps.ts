import {ReactNode} from 'react';
import {RouteProps} from 'react-router-dom';
import {RouterLinks} from '../../../types/RouterLinks';
import {UserRole} from '../../../types/UserRole';

export interface IPrivateRouteProps extends RouteProps {
    children: ReactNode;
    path: RouterLinks;
    role: UserRole[];
}
