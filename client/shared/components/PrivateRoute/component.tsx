import React from "react";
import {Route, Redirect} from "react-router-dom";
import {IPrivateRouteProps} from './types/IPrivateRouteProps';
import {snapd} from '../../../application/snapd';
import {userStorage} from '../../../application/UserStorage';

export default function PrivateRoute({children, ...rest}: IPrivateRouteProps) {

    const isLoggedIn = localStorage.getItem('isLoggedIn');
    const role = userStorage.getRole();
    const hasAccess = rest.role.includes(role);

    const render = () => !isLoggedIn || !hasAccess
                         ? <Redirect to={{pathname: "/login"}}/>
                         : children;

    return <Route {...rest} render={render}/>
}
