import React from "react";
import {useDispatch, useSelector} from 'react-redux';
import {getModal} from '../../../state/modal/selectors';
import {Dialog} from '@material-ui/core';
import {closeModal} from '../../../state/modal';

export function ModalContainer() {

    const modal = useSelector(getModal);

    const dispatch = useDispatch();

    const handleClose = () => {
        dispatch(closeModal());
    }

    return <>
        <Dialog
            open={modal.open}
            onClose={handleClose}
            {...modal.props}
        >
            {modal.component}
        </Dialog>
    </>
}
