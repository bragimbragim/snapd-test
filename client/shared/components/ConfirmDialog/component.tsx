import React from "react";
import {Button, DialogActions, DialogContent, DialogContentText, DialogTitle} from '@material-ui/core';
import {IConfirmDialogProps} from './types/IConfirmDialogProps';
import {useDispatch} from 'react-redux';
import {closeModal} from '../../../state/modal';

export function ConfirmDialog(
    {onConfirm, title, note, confirmBtnCaption}: IConfirmDialogProps
) {

    const dispatch = useDispatch();

    const handleDisagree = () => {
        onConfirm(false);
        dispatch(closeModal());
    }

    const handleAgree = () => {
        onConfirm(true);
        dispatch(closeModal());
    }

    return <>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
            {note && <DialogContentText>{note}</DialogContentText>}
        </DialogContent>
        <DialogActions>
            <Button onClick={handleDisagree} color="primary" autoFocus>Cancel</Button>
            <Button onClick={handleAgree} color="secondary">{confirmBtnCaption}</Button>
        </DialogActions>
    </>
}
