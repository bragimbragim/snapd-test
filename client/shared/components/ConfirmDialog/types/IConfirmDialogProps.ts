export interface IConfirmDialogProps {
    title: string;
    note?: string;
    confirmBtnCaption: string;
    onConfirm: (value: boolean) => void;
}
