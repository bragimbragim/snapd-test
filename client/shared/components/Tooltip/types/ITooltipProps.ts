import {IDefaultComponentProps} from '../../../types/IDefaultComponentProps';
import {ReactElement} from 'react';

export interface ITooltipProps extends IDefaultComponentProps {
    children: ReactElement;
    definition: {
        title?: string;
        text?: string;
        textArray?: string[];
    }
}
