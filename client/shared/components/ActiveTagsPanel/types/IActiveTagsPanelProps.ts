export interface IActiveTagsPanelProps {
    onSelect?: (newValue: string) => void;
    activeTags?: string[];
}
