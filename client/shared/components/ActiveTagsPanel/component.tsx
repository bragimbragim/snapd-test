import React, {useEffect, useState} from "react";
import {List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import TagIcon from '@material-ui/icons/LocalOffer';
import {IActiveTagsPanelProps} from './types/IActiveTagsPanelProps';
import {api} from '../../../application/api';
import {useIsMounted} from 'react-tidy';

let interval;

export function ActiveTagsPanel({onSelect, activeTags}: IActiveTagsPanelProps) {

    const [tags, setTags] = useState<string[]>([]);

    const isMounted = useIsMounted();

    const listenActiveTags = () => {
        interval = setInterval(() => {
            api.tags.getTags()
                .then((tags) => {
                    if (!isMounted) {
                        return;
                    }
                    setTags(tags);
                })
                .catch(() => clearInterval(interval))
        }, 1000);
    }

    const listItems = () => {
        return tags.map((tag, index) => (
            <ListItem
                key={index}
                button
                onClick={(e) => onSelect && onSelect(tag)}
            >
                <ListItemIcon><TagIcon/></ListItemIcon>
                <ListItemText primary={tag}/>
            </ListItem>
        ));
    }

    useEffect(() => {
        if (activeTags) {
            setTags(activeTags);
        } else {
            listenActiveTags();
        }

        return () => {
            clearInterval(interval);
        }
    }, [useState]);

    return <>
        {
            tags.length > 0 &&
            <List component="nav">
                {listItems()}
            </List>
        }
    </>
}
