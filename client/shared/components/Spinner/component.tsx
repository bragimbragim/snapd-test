import React from "react";
import useStyles from './styles';
import {Box, CircularProgress} from '@material-ui/core';

export function Spinner() {

    const classes = useStyles();

    return <>
        <Box className={classes.container}><CircularProgress/></Box>
    </>
}
