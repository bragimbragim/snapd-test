import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) =>
    createStyles({
        logContainer: {
            width: 400,
            height: 200,
            padding: 10,
            overflowY: 'auto',
            backgroundColor: 'rgb(232, 244, 253)',
            color: theme.palette.info.dark,
            borderRadius: theme.shape.borderRadius
        }
    })
);
