import React, {ReactElement, useCallback, useEffect, useMemo} from 'react';
import {Alert, Color} from '@material-ui/lab';
import {useDispatch, useSelector} from 'react-redux';
import {ReaderControlCommand} from '../../../../globalTypes/ReaderControlCommand';
import {ReaderStatusCode} from '../../../../globalTypes/ReaderStatus';
import ReaderButton from './components/Button/component';
import {ReaderStatusCaption} from './components/Status/component';
import {Box} from '@material-ui/core';
import ReaderErrorIcon from './components/ErrorIcon/component';
import {IReaderControlProps} from './types/IReaderControlProps';
import {enableReader, reloadReader, toggleReading, updateReaderStatusAction} from '../../../state/reader';
import {getReaderLoading, getReaderStatus} from '../../../state/reader/selectors';
import {api} from '../../../application/api';
import {openAlert} from '../../../state/alert';
import {AlertType} from '../../../state/alert/types/AlertType';
import {DependentModulesModal} from './components/DependentModulesModal/component';
import {openModal} from '../../../state/modal';

let interval;
let PING_INTERVAL_MS = 5000;

export default function ReaderControl({mode}: IReaderControlProps): ReactElement {

    const reader = useSelector(getReaderStatus);
    const loading = useSelector(getReaderLoading);

    const dispatch = useDispatch();

    const enable = () => {
        dispatch(enableReader(mode));
    }

    const reload = () => {
        dispatch(reloadReader(mode));
    }

    useEffect(() => {
        switch (reader.code) {

            case ReaderStatusCode.INITIALIZED:
                enable();
                break;

            case ReaderStatusCode.ACTIVE:
            case ReaderStatusCode.INACTIVE:
            case ReaderStatusCode.NOT_INITIALIZED:
                pingReader();
                break;

            case ReaderStatusCode.GLOBAL_ERROR:
                clearInterval(interval);
                break;

        }
    }, [reader.code]);

    const startReading = () => {
        dispatch(toggleReading(ReaderControlCommand.START_READ));
    }

    const stopReading = () => {
        dispatch(toggleReading(ReaderControlCommand.STOP_READ));
    }

    const pingReader = () => {
        clearInterval(interval);
        interval = setInterval(() => {
            api.reader.getStatus()
                .then((status) => {
                    dispatch(updateReaderStatusAction(status));
                })
                .catch((err) => {
                    clearInterval(interval);
                    dispatch(openAlert(`Ping Request Error`, AlertType.ERROR));
                })
        }, PING_INTERVAL_MS)
    }

    const openModulesStatus = () => {
        dispatch(openModal(<DependentModulesModal/>));
    }

    const getAlertType = useMemo((): Color => {
        switch (reader.code) {
            case ReaderStatusCode.INACTIVE:
                return 'warning'
            case ReaderStatusCode.ACTIVE:
                return 'success'
            case ReaderStatusCode.GLOBAL_ERROR:
                return 'error'
            case ReaderStatusCode.NOT_INITIALIZED:
                return 'info'
        }
    }, [reader]);

    const btnHandler = useCallback(() => {
        switch (reader.code) {
            case ReaderStatusCode.INACTIVE:
                return startReading;
            case ReaderStatusCode.ACTIVE:
                return stopReading;
            case ReaderStatusCode.GLOBAL_ERROR:
                return reload;
            case ReaderStatusCode.NOT_INITIALIZED:
                return openModulesStatus;
        }
    }, [reader])

    useEffect(() => {
        enable();
        return () => {
            clearInterval(interval);
        };
    }, []);

    return <>
        <Alert
            severity={getAlertType}
            action={
                <ReaderButton
                    loading={loading}
                    disabled={loading}
                    status={reader.code}
                    onClick={btnHandler()}
                />
            }
        >
            <Box display='flex'>
                <ReaderStatusCaption status={reader.code}/>
                <ReaderErrorIcon error={reader.error}/>
            </Box>
        </Alert>
    </>

}
