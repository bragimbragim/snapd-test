import {IDefaultComponentProps} from '../../../types/IDefaultComponentProps';
import {ReaderMode} from '../../../../../globalTypes/ReaderMode';

export interface IReaderControlProps extends IDefaultComponentProps {
    mode: ReaderMode;
}
