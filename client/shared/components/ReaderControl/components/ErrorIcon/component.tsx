import React, {ReactElement} from 'react';
import {Tooltip} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import useStyles from './styles';
import {IReaderErrorProps} from './types/IReaderErrorProps';

export default function ReaderErrorIcon({error}: IReaderErrorProps): ReactElement {

    const classes = useStyles();

    return <>
        {
            error &&
            <Tooltip title={error}>
                <InfoIcon className={classes.logBtn}/>
            </Tooltip>
        }
    </>

}
