import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) =>
    createStyles({
        logBtn: {
            cursor: 'pointer',
            marginLeft: 5
        }
    })
);
