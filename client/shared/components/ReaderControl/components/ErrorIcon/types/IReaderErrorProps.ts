import {IDefaultComponentProps} from '../../../../../types/IDefaultComponentProps';

export interface IReaderErrorProps extends IDefaultComponentProps {
    error: string;
}
