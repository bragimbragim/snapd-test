import React, {useMemo} from "react";
import {Typography} from '@material-ui/core';
import {IReaderStatusCaptionProps} from './types/IReaderStatusCaptionProps';
import {ReaderStatusCode} from '../../../../../../globalTypes/ReaderStatus';

export function ReaderStatusCaption({status}: IReaderStatusCaptionProps) {

    const getReaderStatusCaption = useMemo(() => {
        if (status === ReaderStatusCode.INACTIVE) {
            return 'Reader is inactive now';
        }
        if (status === ReaderStatusCode.ACTIVE) {
            return 'Reader is active now';
        }
        if (status === ReaderStatusCode.GLOBAL_ERROR) {
            return 'Reader has error';
        }
        if (status === ReaderStatusCode.NOT_INITIALIZED) {
            return 'Reader not initialized';
        }
    }, [status]);

    return <>
        <Typography>{getReaderStatusCaption}</Typography>
    </>
}
