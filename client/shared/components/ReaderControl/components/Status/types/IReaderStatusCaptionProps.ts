import {IDefaultComponentProps} from '../../../../../types/IDefaultComponentProps';
import {ReaderStatusCode} from '../../../../../../../globalTypes/ReaderStatus';

export interface IReaderStatusCaptionProps extends IDefaultComponentProps {
    status: ReaderStatusCode;
}
