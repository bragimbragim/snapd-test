import React, {ReactElement, useMemo} from 'react';
import {IReaderButtonProps} from './types/IReaderButtonProps';
import {ReaderStatusCode} from '../../../../../../globalTypes/ReaderStatus';
import {CircularButton} from '../../../CircularButton/component';
import {PropTypes} from '@material-ui/core';

export default function ReaderButton(
    {
        loading,
        status,
        ...rest
    }: IReaderButtonProps
): ReactElement {

    const getBtnCaption = useMemo(() => {
        switch (status) {
            case ReaderStatusCode.INACTIVE:
                return 'Start Reader'
            case ReaderStatusCode.ACTIVE:
                return 'Stop Reader'
            case ReaderStatusCode.GLOBAL_ERROR:
                return 'Reload Reader'
            case ReaderStatusCode.NOT_INITIALIZED:
                return 'Check dependent modules'
        }
    }, [status]);

    const btnColor = useMemo((): PropTypes.Color => {
        switch (status) {
            case ReaderStatusCode.GLOBAL_ERROR:
                return 'secondary';
            default:
                return 'primary';
        }
    }, [status]);

    return <>
        <CircularButton
            loading={loading}
            color={btnColor}
            variant='outlined'
            size="small"
            {...rest}
        >
            {getBtnCaption}
        </CircularButton>
    </>

}
