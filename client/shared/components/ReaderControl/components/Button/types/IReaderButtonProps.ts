import {IDefaultComponentProps} from '../../../../../types/IDefaultComponentProps';
import {ReaderStatusCode} from '../../../../../../../globalTypes/ReaderStatus';
import {ButtonProps} from '@material-ui/core';

export interface IReaderButtonProps extends IDefaultComponentProps, ButtonProps {
    loading: boolean;
    status: ReaderStatusCode;
}
