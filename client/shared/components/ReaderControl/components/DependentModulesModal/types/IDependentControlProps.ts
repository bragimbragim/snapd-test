import {IServiceStatus} from '../../../../../../../globalTypes/ServiceStatus';
import {ReactNode} from 'react';

export interface IDependentControlProps {
    title: string;
    serviceStatus: IServiceStatus;
    action: ReactNode;
    loading: boolean;
}
