import React, {useCallback} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {DependentModuleControl} from '../DependentModuleControl/component';
import {getMemoryServiceLoading, getMemoryServiceStatus} from '../../../../../../../state/services/memory/selectors';
import {ServiceStatusCode} from '../../../../../../../../globalTypes/ServiceStatus';
import {useHistory} from 'react-router-dom';
import {reloadMemoryService} from '../../../../../../../state/services/memory';
import {closeModal} from '../../../../../../../state/modal';
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';


export function MemoryControl() {

    const serviceStatus = useSelector(getMemoryServiceStatus);
    const loading = useSelector(getMemoryServiceLoading);

    const dispatch = useDispatch();
    const history = useHistory();

    const reloadService = () => {
        dispatch(reloadMemoryService());
    }

    const goToServiceTab = () => {
        localStorage.setItem('activeConfigTab', '4');
        history.push('/config');
        dispatch(closeModal());
    }

    const getActionBtn = useCallback(() => {

        if (serviceStatus.code === ServiceStatusCode.ERROR) {
            return <IconButton color="secondary" onClick={reloadService}>
                <RefreshIcon/>
            </IconButton>
        }

        if (serviceStatus.code === ServiceStatusCode.WARNING) {
            return <IconButton color="secondary" onClick={goToServiceTab}>
                <ArrowForwardIosIcon/>
            </IconButton>
        }
    }, [serviceStatus.code]);

    return <>
        <DependentModuleControl
            title='Memory Service'
            serviceStatus={serviceStatus}
            action={getActionBtn()}
            loading={loading}
        />
    </>
}
