import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) =>
    createStyles({
        alert: {
            position: 'relative'
        },
        spinnerContainer: {
            position: 'absolute',
            top: 0,
            left: 0,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: '100%',
            borderRadius: 4,
            backgroundColor: ' #c59b9b38',
            zIndex: 2
        }
    })
);
