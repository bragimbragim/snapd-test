import React, {useMemo} from "react";
import {Alert, Color} from '@material-ui/lab';
import {Box, CircularProgress, Typography} from '@material-ui/core';
import {IDependentControlProps} from '../../types/IDependentControlProps';
import {ServiceStatusCode} from '../../../../../../../../globalTypes/ServiceStatus';
import useStyles from './style';

export function DependentModuleControl({title, serviceStatus, action, loading}: IDependentControlProps) {

    const classes = useStyles();

    const getAlertType = useMemo((): Color => {
        switch (serviceStatus.code) {
            case ServiceStatusCode.SUCCESS:
                return 'success';
            case ServiceStatusCode.ERROR:
                return 'error';
            case ServiceStatusCode.WARNING:
                return 'warning';
            case ServiceStatusCode.NOT_INITIALIZED:
                return 'info';
        }
    }, [serviceStatus.code]);

    return <>
        <Box mb={2}>
            <Alert className={classes.alert} severity={getAlertType} action={action}>
                {loading && <Box className={classes.spinnerContainer}><CircularProgress/></Box>}
                <Typography>{title}</Typography>
                {serviceStatus?.caption || ''}
            </Alert>
        </Box>
    </>
}
