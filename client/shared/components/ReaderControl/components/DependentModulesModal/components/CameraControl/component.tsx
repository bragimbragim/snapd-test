import React, {useCallback} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {getMqttServiceStatus} from '../../../../../../../state/services/mqtt/selectors';
import {DependentModuleControl} from '../DependentModuleControl/component';
import {Button, Typography} from '@material-ui/core';
import {getCameraServiceLoading, getCameraServiceStatus} from '../../../../../../../state/services/camera/selectors';
import {reloadCameraService} from '../../../../../../../state/services/camera';
import {ServiceStatusCode} from '../../../../../../../../globalTypes/ServiceStatus';
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';


export function CameraControl() {

    const serviceStatus = useSelector(getCameraServiceStatus);
    const loading = useSelector(getCameraServiceLoading);

    const dispatch = useDispatch();

    const reloadService = () => {
        dispatch(reloadCameraService());
    }

    const getReloadBtn = useCallback(() => {
        if (serviceStatus.code === ServiceStatusCode.ERROR) {
            return <IconButton color="secondary" onClick={reloadService}>
                <RefreshIcon/>
            </IconButton>
        }
    }, [serviceStatus.code]);

    return <>
        <DependentModuleControl
            title='Camera module'
            serviceStatus={serviceStatus}
            action={getReloadBtn()}
            loading={loading}
        />
    </>
}
