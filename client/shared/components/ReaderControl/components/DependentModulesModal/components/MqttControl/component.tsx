import React, {useCallback} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {getMqttServiceLoading, getMqttServiceStatus} from '../../../../../../../state/services/mqtt/selectors';
import {DependentModuleControl} from '../DependentModuleControl/component';
import {reloadMqttService} from '../../../../../../../state/services/mqtt';
import {ServiceStatusCode} from '../../../../../../../../globalTypes/ServiceStatus';
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';


export function MqttControl() {

    const serviceStatus = useSelector(getMqttServiceStatus);
    const loading = useSelector(getMqttServiceLoading);
    const dispatch = useDispatch();

    const reloadService = () => {
        dispatch(reloadMqttService());
    }

    const getReloadBtn = useCallback(() => {
        if (serviceStatus.code === ServiceStatusCode.ERROR) {
            return <IconButton color="secondary" onClick={reloadService}>
                <RefreshIcon/>
            </IconButton>
        }
    }, [serviceStatus.code]);

    return <>
        <DependentModuleControl
            title='MQTT Service'
            serviceStatus={serviceStatus}
            action={getReloadBtn()}
            loading={loading}
        />
    </>
}
