import React, {useEffect, useMemo, useState} from "react";
import DialogContent from '@material-ui/core/DialogContent';
import {Box, CircularProgress, Divider, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {ISnapdServices} from '../../../../../../globalTypes/ISnapdServices';
import {api} from '../../../../../application/api';
import {openAlert} from '../../../../../state/alert';
import {AlertType} from '../../../../../state/alert/types/AlertType';
import {updateAmazonServiceStatusAction} from '../../../../../state/services/amazon';
import {updateMemoryServiceStatusAction} from '../../../../../state/services/memory';
import {MqttControl} from './components/MqttControl/component';
import {updateMqttServiceStatusAction} from '../../../../../state/services/mqtt';
import {CameraControl} from './components/CameraControl/component';
import {MemoryControl} from './components/MemoryControl/component';
import useStyles from './style';
import {getMqttServiceStatus} from '../../../../../state/services/mqtt/selectors';
import {getMemoryServiceStatus} from '../../../../../state/services/memory/selectors';
import {ServiceStatusCode} from '../../../../../../globalTypes/ServiceStatus';
import ReaderButton from '../Button/component';
import {getReaderLoading} from '../../../../../state/reader/selectors';
import {ReaderStatusCode} from '../../../../../../globalTypes/ReaderStatus';
import {reloadReader} from '../../../../../state/reader';
import {ReaderMode} from '../../../../../../globalTypes/ReaderMode';
import {updateCameraServiceStatusAction} from '../../../../../state/services/camera';
import {getCameraServiceStatus} from '../../../../../state/services/camera/selectors';
import {closeModal} from '../../../../../state/modal';

export function DependentModulesModal() {

    const [loading, setLoading] = useState<boolean>(false);

    const readerLoading = useSelector(getReaderLoading);
    const mqttStatus = useSelector(getMqttServiceStatus);
    const memoryStatus = useSelector(getMemoryServiceStatus);
    const cameraStatus = useSelector(getCameraServiceStatus);
    const dependentModules = [mqttStatus, cameraStatus, memoryStatus];

    const dispatch = useDispatch();
    const classes = useStyles();

    const getAllServices = () => {
        setLoading(true);
        api.system.getServicesStatus()
            .then((data: ISnapdServices) => {
                dispatch(updateMqttServiceStatusAction(data.mqtt));
                dispatch(updateCameraServiceStatusAction(data.camera));
                dispatch(updateMemoryServiceStatusAction(data.memory));
                dispatch(updateAmazonServiceStatusAction(data.amazon));
            })
            .catch((err) => {
                dispatch(openAlert(err, AlertType.ERROR));
            })
            .finally(() => setLoading(false))
    }

    const servicesHasError = useMemo((): boolean => {
        return dependentModules.some((service) => {
            return [ServiceStatusCode.ERROR, ServiceStatusCode.WARNING].includes(service.code);
        });
    }, dependentModules);

    const reload = () => {
        (dispatch(reloadReader(ReaderMode.MAKE_PHOTO)) as unknown as Promise<any>)
            .then(() => dispatch(closeModal()))
    }

    useEffect(() => {
        getAllServices();
    }, []);

    return <>
        {loading && <Box className={classes.spinnerContainer}><CircularProgress/></Box>}
        <DialogContent>
            {
                !loading &&
                <Box>
                    <Box>
                        <MqttControl/>
                        <CameraControl/>
                        <MemoryControl/>
                    </Box>

                    <Box>
                        <Box mt={2} mb={2}>
                            <Divider/>
                            <Typography
                                variant='subtitle2'
                                color='textSecondary'
                            >Start all services to start the reader</Typography>
                        </Box>

                        <Box mt={1} mb={1} display='flex' justifyContent='center'>
                            <ReaderButton
                                loading={readerLoading}
                                disabled={readerLoading || servicesHasError}
                                status={ReaderStatusCode.GLOBAL_ERROR}
                                onClick={reload}
                            />
                        </Box>
                    </Box>
                </Box>
            }
        </DialogContent>
    </>
}
