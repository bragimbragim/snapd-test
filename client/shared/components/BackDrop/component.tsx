import React, {ReactElement} from 'react';
import {Backdrop, Box, CircularProgress} from '@material-ui/core';
import useStyles from './style';
import {IDefaultComponentProps} from '../../types/IDefaultComponentProps';

export function BackDrop({children}: IDefaultComponentProps): ReactElement {

    const classes = useStyles();

    return <>
        <Backdrop className={classes.backdrop} open={true}>
            <Box display='flex' flexDirection='column' alignItems='center'>
                <CircularProgress color="inherit"/>
                <Box mt={3} display='flex' flexDirection='column' alignItems='center'>
                    {children}
                </Box>
            </Box>
        </Backdrop>
    </>

}
