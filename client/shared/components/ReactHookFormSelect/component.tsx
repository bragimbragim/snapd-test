import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import {Controller} from "react-hook-form";
import {FormControl} from '@material-ui/core';
import React from "react";
import {IReactHookFormSelectProps} from './types/IReactHookFormSelectProps';

const ReactHookFormSelect = (
    {
        name,
        label = null,
        control,
        defaultValue,
        children,
        ...props
    }: IReactHookFormSelectProps
) => {
    const labelId = `${name}-label`;
    return (
        <FormControl {...props}>
            {label ? <InputLabel id={labelId}>{label}</InputLabel> : null}
            <Controller
                as={
                    <Select labelId={labelId} label={label}>
                        {children}
                    </Select>
                }
                name={name}
                control={control}
                defaultValue={defaultValue}
            />
        </FormControl>
    );
};
export default ReactHookFormSelect;
