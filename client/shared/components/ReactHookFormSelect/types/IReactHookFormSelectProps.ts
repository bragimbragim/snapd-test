import {IDefaultComponentProps} from '../../../types/IDefaultComponentProps';

export interface IReactHookFormSelectProps extends IDefaultComponentProps {
    name: string;
    label?: string;
    disabled?: boolean;
    control: any;
    defaultValue: any;
    [p: string]: any;
}
