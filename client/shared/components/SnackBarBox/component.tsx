import React from 'react';
import {Snackbar} from '@material-ui/core';
import {Alert, Color} from '@material-ui/lab';
import {useDispatch, useSelector} from 'react-redux';
import {getAlert} from '../../../state/alert/selectors';
import {closeAlert} from '../../../state/alert';
import {AlertType} from '../../../state/alert/types/AlertType';

export default function SnackBarBox() {

    let {isOpen, message, severity} = useSelector(getAlert);
    const dispatch = useDispatch();

    const handleClose = () => dispatch(closeAlert());

    const getMessage = () => {
        if (typeof message === 'object') {
            severity = AlertType.ERROR;
            message = 'Unhandled Error';
        }
        return message;
    }

    return <>
        <Snackbar open={isOpen} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity={severity as Color}>
                {getMessage()}
            </Alert>
        </Snackbar>
    </>
}
