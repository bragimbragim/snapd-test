import React from 'react';
import {render} from 'react-dom';
import App from './App';
import bootstrap from './bootstrap';

bootstrap();

render(<App/>, document.getElementById('app'));
