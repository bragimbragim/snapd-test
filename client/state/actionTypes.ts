export const ALERT = {
    OPEN: 'ALERT/OPEN',
    CLOSE: 'ALERT/CLOSE'
} as const;

export const MODAL = {
    OPEN: 'MODAL/OPEN',
    CLOSE: 'MODAL/CLOSE'
} as const;

export const GALLERY = {
    PHOTOS: {
        GET: {
            REQUEST: "/PHOTOS/GET/REQUEST",
            SUCCESS: "/PHOTOS/GET/SUCCESS",
            ERROR: "/PHOTOS/GET/ERROR"
        },
        DOWNLOAD: {
            REQUEST: "/PHOTOS/DOWNLOAD/REQUEST",
            SUCCESS: "/PHOTOS/DOWNLOAD/SUCCESS",
            ERROR: "/PHOTOS/DOWNLOAD/ERROR"
        },
        DELETE: {
            SELECTED: {
                REQUEST: "/PHOTOS/DELETE/SELECTED/REQUEST",
                SUCCESS: "/PHOTOS/DELETE/SELECTED/SUCCESS",
                ERROR: "/PHOTOS/DELETE/SELECTED/ERROR"
            },
            ALL: {
                REQUEST: "/PHOTOS/DELETE/ALL/REQUEST",
                SUCCESS: "/PHOTOS/DELETE/ALL/SUCCESS",
                ERROR: "/PHOTOS/DELETE/ALL/ERROR"
            }
        },
        TOGGLE: {
            ONE: '/PHOTOS/SELECT/ONE',
            ALL: '/PHOTOS/SELECT/ALL'
        }
    }
} as const;

export const READER = {
    ENABLE: {
        REQUEST: "/READER/ENABLE/REQUEST",
        SUCCESS: "/READER/ENABLE/SUCCESS",
        ERROR: "/READER/ENABLE/ERROR"
    },
    TOGGLE: {
        REQUEST: "/READER/START/REQUEST",
        SUCCESS: "/READER/START/SUCCESS",
        ERROR: "/READER/START/ERROR"
    },
    RELOAD: {
        REQUEST: "/READER/STATUS/REQUEST",
        SUCCESS: "/READER/STATUS/SUCCESS",
        ERROR: "/READER/STATUS/ERROR"
    },
    DATA: {
        GET: {
            REQUEST: "/READER/STATUS/GET/REQUEST",
            SUCCESS: "/READER/STATUS/GET/SUCCESS",
            ERROR: "/READER/STATUS/GET/ERROR"
        },
        UPDATE: '/READER/STATUS'
    }
} as const;

export const MEMORY = {
    RELOAD: {
        REQUEST: "/MEMORY/SERVICE/RELOAD/REQUEST",
        SUCCESS: "/MEMORY/SERVICE/RELOAD/SUCCESS",
        ERROR: "/MEMORY/SERVICE/RELOAD/ERROR"
    },
    SERVICE_STATUS: {
        FETCH: {
            REQUEST: "/MEMORY/SERVICE_STATUS/FETCH/REQUEST",
            SUCCESS: "/MEMORY/SERVICE_STATUS/FETCH/SUCCESS",
            ERROR: "/MEMORY/SERVICE_STATUS/FETCH/STATUS"
        },
        UPDATE: "/MEMORY/SERVICE_STATUS/UPDATE"
    },
    DATA: {
        UPDATE: {
            REQUEST: "/MEMORY/DATA/UPDATE/REQUEST",
            SUCCESS: "/MEMORY/DATA/UPDATE/SUCCESS",
            ERROR: "/MEMORY/DATA/UPDATE/ERROR"
        }
    }
} as const;

export const AMAZON = {
    RELOAD: {
        REQUEST: "/AMAZON/RELOAD/REQUEST",
        SUCCESS: "/AMAZON/RELOAD/SUCCESS",
        ERROR: "/AMAZON/RELOAD/ERROR"
    },
    SERVICE_STATUS: {
        FETCH: {
            REQUEST: "/AMAZON/SERVICE_STATUS/FETCH/REQUEST",
            SUCCESS: "/AMAZON/SERVICE_STATUS/FETCH/SUCCESS",
            ERROR: "/AMAZON/SERVICE_STATUS/FETCH/ERROR"
        },
        UPDATE: "/AMAZON/SERVICE_STATUS/UPDATE"
    },
    QUEUE: {
        UPDATE: {
            COUNT: "/AMAZON/QUEUE/UPDATE/COUNT",
            LIST: "/AMAZON/QUEUE/UPDATE/LIST"
        }
    }
} as const;

export const MQTT = {
    RELOAD: {
        REQUEST: "/MQTT/RELOAD/REQUEST",
        SUCCESS: "/MQTT/RELOAD/SUCCESS",
        ERROR: "/MQTT/RELOAD/ERROR"
    },
    SERVICE_STATUS: {
        FETCH: {
            REQUEST: "/MQTT/SERVICE_STATUS/FETCH/REQUEST",
            SUCCESS: "/MQTT/SERVICE_STATUS/FETCH/SUCCESS",
            ERROR: "/MQTT/SERVICE_STATUS/FETCH/ERROR"
        },
        UPDATE: "/MQTT/SERVICE_STATUS/UPDATE"
    }
} as const;

export const CAMERA = {
    RELOAD: {
        REQUEST: "/CAMERA/RELOAD/REQUEST",
        SUCCESS: "/CAMERA/RELOAD/SUCCESS",
        ERROR: "/CAMERA/RELOAD/ERROR"
    },
    SERVICE_STATUS: {
        FETCH: {
            REQUEST: "/CAMERA/SERVICE_STATUS/FETCH/REQUEST",
            SUCCESS: "/CAMERA/SERVICE_STATUS/FETCH/SUCCESS",
            ERROR: "/CAMERA/SERVICE_STATUS/FETCH/ERROR"
        },
        UPDATE: "/CAMERA/SERVICE_STATUS/UPDATE"
    }
} as const;

export const WIFI = {
    LIST: {
        GET: {
            REQUEST: "/WIFI/LIST/GET/REQUEST",
            SUCCESS: "/WIFI/LIST/GET/SUCCESS",
            ERROR: "/WIFI/LIST/GET/ERROR"
        }
    },
    CONNECT: {
        NEW: {
            REQUEST: "/WIFI/CONNECT/NEW/REQUEST",
            SUCCESS: "/WIFI/CONNECT/NEW/SUCCESS",
            ERROR: "/WIFI/CONNECT/NEW/ERROR"
        },
        SAVED: {
            REQUEST: "/WIFI/CONNECT/SAVED/REQUEST",
            SUCCESS: "/WIFI/CONNECT/SAVED/SUCCESS",
            ERROR: "/WIFI/CONNECT/SAVED/ERROR"
        }
    },
    REMOVE: {
        REQUEST: "/WIFI/REMOVE/REQUEST",
        SUCCESS: "/WIFI/REMOVE/SUCCESS",
        ERROR: "/WIFI/REMOVE/ERROR"
    }
} as const;
