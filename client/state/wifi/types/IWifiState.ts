import {IWifiItemResponse} from '../../../../globalTypes/IWifi';

export interface IWifiState {
    list: IWifiItemResponse[];
    loading: boolean;
}
