import {ActionType, createAsyncAction} from 'typesafe-actions';
import {WIFI} from '../actionTypes';
import {Thunk} from '../types/Thunk';
import {AlertAction, openAlert} from '../alert';
import {IError} from '../../shared/types/IError';
import {api} from '../../application/api';
import {AlertType} from '../alert/types/AlertType';
import {IWifiState} from './types/IWifiState';
import {IWifiItemResponse, IWifiRequest} from '../../../globalTypes/IWifi';

const initialState: IWifiState = {
    list: [],
    loading: false
}


const getListOfNetworksAction = createAsyncAction(
    WIFI.LIST.GET.REQUEST,
    WIFI.LIST.GET.SUCCESS,
    WIFI.LIST.GET.ERROR
)<undefined, IWifiItemResponse[], string>();

type GetListOfNetworksAction = ActionType<typeof getListOfNetworksAction>;

export const getListOfNetworks = (): Thunk<GetListOfNetworksAction | AlertAction> => (
    dispatch
): Promise<IWifiItemResponse[] | void | IError> => {

    // @ts-ignore
    dispatch(getListOfNetworksAction.request());
    return api.wifi.getListOfNetwork()
        .then((payload: IWifiItemResponse[]) => {
            dispatch(getListOfNetworksAction.success(payload))
        })
        .catch((err: string) => {
            dispatch(getListOfNetworksAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR))
        })
}


const connectToNewNetworkAction = createAsyncAction(
    WIFI.CONNECT.NEW.REQUEST,
    WIFI.CONNECT.NEW.SUCCESS,
    WIFI.CONNECT.NEW.ERROR
)<undefined, string, string>();

type ConnectToNewNetworkAction = ActionType<typeof connectToNewNetworkAction>;

export const connectToNewNetwork = (request: IWifiRequest): Thunk<ConnectToNewNetworkAction | AlertAction> => (
    dispatch
): Promise<string | void | IError> => {

    // @ts-ignore
    dispatch(connectToNewNetworkAction.request());
    return api.wifi.connectToNewNetwork(request)
        .then((payload) => {
            dispatch(connectToNewNetworkAction.success(request.ssid))
            dispatch(openAlert(payload, AlertType.SUCCESS));
            dispatch(getListOfNetworks());

            return Promise.resolve();
        })
        .catch((err: string) => {
            dispatch(connectToNewNetworkAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));

            if (err === 'No ip address was assigned' || err === 'Unable to connect this network') {
                dispatch(getListOfNetworks());
            }

            return Promise.reject(err);
        })
}


const connectToSavedNetworkAction = createAsyncAction(
    WIFI.CONNECT.SAVED.REQUEST,
    WIFI.CONNECT.SAVED.SUCCESS,
    WIFI.CONNECT.SAVED.ERROR
)<undefined, string, string>();

type ConnectToSavedNetworkAction = ActionType<typeof connectToSavedNetworkAction>;

export const connectToSavedNetwork = (ssid: string): Thunk<ConnectToSavedNetworkAction | AlertAction> => (
    dispatch
): Promise<string | void | IError> => {

    // @ts-ignore
    dispatch(connectToSavedNetworkAction.request());
    return api.wifi.connectToSavedNetwork(ssid)
        .then((payload) => {
            dispatch(connectToSavedNetworkAction.success(ssid));
            dispatch(openAlert(payload, AlertType.SUCCESS));
            dispatch(getListOfNetworks());

            return Promise.resolve();
        })
        .catch((err: string) => {
            dispatch(connectToSavedNetworkAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));

            if (err === 'No ip address was assigned' || err === 'Unable to connect this network') {
                dispatch(getListOfNetworks());
            }

            return Promise.reject(err);
        })
}


const removeNetworkAction = createAsyncAction(
    WIFI.REMOVE.REQUEST,
    WIFI.REMOVE.SUCCESS,
    WIFI.REMOVE.ERROR
)<undefined, void, string>();

type RemoveNetworkAction = ActionType<typeof removeNetworkAction>;

export const removeNetwork = (item: IWifiItemResponse): Thunk<RemoveNetworkAction | AlertAction> => (
    dispatch
): Promise<string | void | IError> => {

    // @ts-ignore
    dispatch(removeNetworkAction.request());
    return api.wifi.removeNetwork(item)
        .then((payload) => {
            dispatch(removeNetworkAction.success());
            dispatch(openAlert(payload, AlertType.SUCCESS));
            dispatch(getListOfNetworks());

            return Promise.resolve();
        })
        .catch((err: string) => {
            dispatch(removeNetworkAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));

            return Promise.reject(err);
        })
}

type WifiReducerActions =
    | GetListOfNetworksAction
    | ConnectToNewNetworkAction
    | ConnectToSavedNetworkAction
    | RemoveNetworkAction;


export function wifiReducer(
    state: IWifiState = initialState,
    action: WifiReducerActions
): IWifiState {
    switch (action.type) {

        case WIFI.LIST.GET.REQUEST:
        case WIFI.CONNECT.NEW.REQUEST:
        case WIFI.CONNECT.SAVED.REQUEST:
        case WIFI.REMOVE.REQUEST:
            return {...state, loading: true}


        case WIFI.LIST.GET.SUCCESS:
            return {...state, loading: false, list: action.payload}

        case WIFI.REMOVE.ERROR:
        case WIFI.LIST.GET.ERROR:
        case WIFI.CONNECT.NEW.ERROR:
        case WIFI.CONNECT.SAVED.ERROR:
        case WIFI.CONNECT.NEW.SUCCESS:
        case WIFI.CONNECT.SAVED.SUCCESS:
        case WIFI.REMOVE.SUCCESS:
            return {...state, loading: false}

        default:
            return {...state}

    }
}
