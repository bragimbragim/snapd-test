import {RootState} from '../types/RootState';
import {IWifiItemResponse} from '../../../globalTypes/IWifi';

export const getNewNetworksListItems = (state: RootState): IWifiItemResponse[] => {
    return state.wifi.list
        .filter((item) => !item.saved)
        .sort((a, b) => a.signalLevel > b.signalLevel ? -1 : 1)
}

export const getSavedNetworksListItems = (state: RootState): IWifiItemResponse[] => {
    return state.wifi.list
        .filter((item) => item.saved)
        .sort((a, b) => a.signalLevel > b.signalLevel ? -1 : 1)
}

export const getWifiLoading = (state: RootState): boolean => state.wifi.loading;
