import {RootState} from '../types/RootState';
import {IReaderState} from './types/IReaderState';

export const getReaderStatus = (state: RootState): IReaderState => state.reader;

export const getReaderLoading = (state: RootState): boolean => state.reader.loading
