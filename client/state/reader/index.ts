import {IReaderState} from './types/IReaderState';
import {ReaderStatus, ReaderStatusCode} from '../../../globalTypes/ReaderStatus';
import {ActionType, createAction, createAsyncAction} from 'typesafe-actions';
import {READER} from '../actionTypes';
import {IError} from '../../shared/types/IError';
import {Thunk} from '../types/Thunk';
import {AlertAction, openAlert} from '../alert';
import {AlertType} from '../alert/types/AlertType';
import {ReaderMode} from '../../../globalTypes/ReaderMode';
import {ReaderControlCommand} from '../../../globalTypes/ReaderControlCommand';
import {api} from '../../application/api';

const initialState: IReaderState = {
    mode: ReaderMode.MAKE_PHOTO,
    code: ReaderStatusCode.NOT_INITIALIZED,
    loading: false
}


const enableReaderAction = createAsyncAction(
    READER.ENABLE.REQUEST,
    READER.ENABLE.SUCCESS,
    READER.ENABLE.ERROR
)<undefined, ReaderStatus, string>();

type EnableReaderAction = ActionType<typeof enableReaderAction>;

export const enableReader = (mode: ReaderMode): Thunk<EnableReaderAction | AlertAction> => (
    dispatch
): Promise<ReaderStatus | void | IError> => {

    // @ts-ignore
    dispatch(enableReaderAction.request());
    return api.reader.enable(mode)
        .then((payload) => {
            dispatch(enableReaderAction.success(payload))
        })
        .catch((err: string) => {
            dispatch(enableReaderAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR))
        })
}


const toggleReadingAction = createAsyncAction(
    READER.TOGGLE.REQUEST,
    READER.TOGGLE.SUCCESS,
    READER.TOGGLE.ERROR
)<undefined, ReaderStatus, string>();

type ToggleReadingAction = ActionType<typeof toggleReadingAction>;

export const toggleReading = (control: ReaderControlCommand): Thunk<ToggleReadingAction | AlertAction> => (
    dispatch
): Promise<ReaderStatus | void | IError> => {

    // @ts-ignore
    dispatch(enableReaderAction.request());
    return api.reader.toggleReading(control)
        .then((payload) => {
            dispatch(toggleReadingAction.success(payload))
        })
        .catch((err: string) => {
            dispatch(toggleReadingAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR))
        });
}


const reloadReaderAction = createAsyncAction(
    READER.RELOAD.REQUEST,
    READER.RELOAD.SUCCESS,
    READER.RELOAD.ERROR
)<undefined, ReaderStatus, string>();

type ReloadReaderAction = ActionType<typeof reloadReaderAction>;

export const reloadReader = (mode: ReaderMode): Thunk<ReloadReaderAction | AlertAction> => (
    dispatch
): Promise<ReaderStatus | void | IError> => {
    // @ts-ignore
    dispatch(reloadReaderAction.request());
    return api.reader.reload()
        .then((payload: ReaderStatus) => {
            // TODO: should be fixed
            setTimeout(() => {
                dispatch(enableReader(mode))
            }, 200);
        })
        .catch((err: string) => {
            dispatch(reloadReaderAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR))
        })
}

export const updateReaderStatusAction = createAction(
    READER.DATA.UPDATE,
    (status: ReaderStatus) => (status)
)();

type UpdateReaderStatusAction = ActionType<typeof updateReaderStatusAction>;

type ReaderReducerActions =
    | EnableReaderAction
    | UpdateReaderStatusAction
    | ReloadReaderAction
    | ToggleReadingAction;


export function readerReducer(
    state: IReaderState = initialState,
    action: ReaderReducerActions
): IReaderState {
    switch (action.type) {

        case READER.ENABLE.REQUEST:
        case READER.RELOAD.REQUEST:
        case READER.TOGGLE.REQUEST:
            return {...state, loading: true}

        case  READER.ENABLE.SUCCESS:
        case READER.RELOAD.SUCCESS:
        case READER.TOGGLE.SUCCESS:
            return {...state, loading: false, ...action.payload}

        case READER.ENABLE.ERROR:
        case READER.RELOAD.ERROR:
        case READER.TOGGLE.ERROR:
            return {...state, loading: false}

        case READER.DATA.UPDATE:
            return {...state, ...action.payload}

        default:
            return state;
    }
}
