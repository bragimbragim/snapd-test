import {ReaderStatus} from '../../../../globalTypes/ReaderStatus';

export interface IReaderState extends ReaderStatus {
    loading: boolean
}
