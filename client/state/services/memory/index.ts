import {IMemoryState} from './types/IMemoryState';
import {ActionType, createAction, createAsyncAction} from 'typesafe-actions';
import {initServiceStatus, IServiceStatus} from '../../../../globalTypes/ServiceStatus';
import {MEMORY} from '../../actionTypes';
import {Thunk} from '../../types/Thunk';
import {AlertAction, openAlert} from '../../alert';
import {IError} from '../../../shared/types/IError';
import {api} from '../../../application/api';
import {AlertType} from '../../alert/types/AlertType';
import {MemoryData} from '../../../../globalTypes/MemoryData';

const initialState: IMemoryState = {
    serviceStatus: initServiceStatus,
    memoryData: {
        size: null,
        available: null,
        warning: null
    },
    requestError: false,
    loading: false
}


const fetchMemoryServiceStatusAction = createAsyncAction(
    MEMORY.SERVICE_STATUS.FETCH.REQUEST,
    MEMORY.SERVICE_STATUS.FETCH.SUCCESS,
    MEMORY.SERVICE_STATUS.FETCH.ERROR
)<undefined, IServiceStatus, string>();

type FetchMemoryServiceStatusAction = ActionType<typeof fetchMemoryServiceStatusAction>;

export const fetchMemoryServiceStatus = (): Thunk<FetchMemoryServiceStatusAction | AlertAction> => (
    dispatch
): Promise<IServiceStatus | void | IError> => {

    // @ts-ignore
    dispatch(fetchMemoryServiceStatusAction.request());
    return api.system.getServicesStatus()
        .then((payload) => {
            dispatch(fetchMemoryServiceStatusAction.success(payload.memory));
        })
        .catch((err) => {
            dispatch(fetchMemoryServiceStatusAction.failure(err))
            dispatch(openAlert(err, AlertType.ERROR));
        });
}


const reloadMemoryServiceAction = createAsyncAction(
    MEMORY.RELOAD.REQUEST,
    MEMORY.RELOAD.SUCCESS,
    MEMORY.RELOAD.ERROR
)<undefined, IServiceStatus, string>();

type ReloadMemoryServiceAction = ActionType<typeof reloadMemoryServiceAction>;

export const reloadMemoryService = (): Thunk<ReloadMemoryServiceAction | AlertAction> => (
    dispatch
): Promise<IServiceStatus | void | IError> => {

    // @ts-ignore
    dispatch(reloadMemoryServiceAction.request());
    return api.memory.reloadService()
        .then((payload: IServiceStatus) => {
            dispatch(reloadMemoryServiceAction.success(payload));
        })
        .catch((err) => {
            dispatch(reloadMemoryServiceAction.failure(err))
            dispatch(openAlert(err, AlertType.ERROR));
            dispatch(fetchMemoryServiceStatus());
        });
}


export const updateMemoryDataAction = createAction(
    MEMORY.DATA.UPDATE.SUCCESS,
    (status: MemoryData) => (status)
)();

type UpdateMemoryDataAction = ActionType<typeof updateMemoryDataAction>;

export const updateMemoryServiceStatusAction = createAction(
    MEMORY.SERVICE_STATUS.UPDATE,
    (status: IServiceStatus) => (status)
)();

type UpdateMemoryServiceStatusAction = ActionType<typeof updateMemoryServiceStatusAction>;

type MemoryReducerActions =
    | UpdateMemoryDataAction
    | ReloadMemoryServiceAction
    | FetchMemoryServiceStatusAction
    | UpdateMemoryServiceStatusAction;

export function memoryReducer(
    state: IMemoryState = initialState,
    action: MemoryReducerActions
): IMemoryState {
    switch (action.type) {

        case MEMORY.RELOAD.REQUEST:
        case MEMORY.SERVICE_STATUS.FETCH.REQUEST:
            return {...state, loading: true}

        case MEMORY.RELOAD.SUCCESS:
        case MEMORY.SERVICE_STATUS.FETCH.SUCCESS:
            return {...state, serviceStatus: action.payload, loading: false}

        case MEMORY.RELOAD.ERROR:
        case MEMORY.SERVICE_STATUS.FETCH.ERROR:
            return {...state, loading: false}

        case MEMORY.DATA.UPDATE.SUCCESS:
            return {...state, memoryData: action.payload}

        case MEMORY.SERVICE_STATUS.UPDATE:
            return {...state, serviceStatus: action.payload}

        default:
            return state;
    }
}
