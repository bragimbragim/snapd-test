import {RootState} from '../../types/RootState';
import {MemoryData} from '../../../../globalTypes/MemoryData';
import {IServiceStatus} from '../../../../globalTypes/ServiceStatus';

export const getMemoryServiceStatus = (state: RootState): IServiceStatus => state.memory.serviceStatus;

export const getMemoryData = (state: RootState): MemoryData => state.memory.memoryData;

export const getMemoryServiceLoading = (state: RootState): boolean => state.memory.loading;

export const getMemoryDataRequestError = (state: RootState): boolean => state.memory.requestError;

