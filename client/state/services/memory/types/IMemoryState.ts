import {MemoryData} from '../../../../../globalTypes/MemoryData';
import {IServiceStatus} from '../../../../../globalTypes/ServiceStatus';

export interface IMemoryState {
    memoryData: MemoryData & {};
    serviceStatus: IServiceStatus;
    requestError: boolean;
    loading: boolean;
}
