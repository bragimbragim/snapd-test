import {IServiceStatus} from '../../../../../globalTypes/ServiceStatus';

export interface IMqttState extends IServiceStatus {
    loading: boolean;
}
