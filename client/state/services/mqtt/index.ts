import {ActionType, createAction, createAsyncAction} from 'typesafe-actions';
import {initServiceStatus, IServiceStatus} from '../../../../globalTypes/ServiceStatus';
import {AMAZON, MQTT} from '../../actionTypes';
import {Thunk} from '../../types/Thunk';
import {AlertAction, openAlert} from '../../alert';
import {IError} from '../../../shared/types/IError';
import {api} from '../../../application/api';
import {ISnapdServices} from '../../../../globalTypes/ISnapdServices';
import {AlertType} from '../../alert/types/AlertType';
import {IMqttState} from './types/IMqttState';

const initialState: IMqttState = {
    ...initServiceStatus,
    loading: false
}


const fetchMqttServiceStatusAction = createAsyncAction(
    MQTT.SERVICE_STATUS.FETCH.REQUEST,
    MQTT.SERVICE_STATUS.FETCH.SUCCESS,
    MQTT.SERVICE_STATUS.FETCH.ERROR
)<undefined, IServiceStatus, string>();

type FetchMqttServiceStatusAction = ActionType<typeof fetchMqttServiceStatusAction>;

export const fetchMqttServiceStatus = (): Thunk<FetchMqttServiceStatusAction | AlertAction> => (
    dispatch
): Promise<IServiceStatus | void | IError> => {

    // @ts-ignore
    dispatch(fetchMqttServiceStatusAction.request());
    return api.system.getServicesStatus()
        .then((payload: ISnapdServices) => {
            dispatch(fetchMqttServiceStatusAction.success(payload.mqtt))
        })
        .catch((err: string) => {
            dispatch(fetchMqttServiceStatusAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));
        })
}


const reloadMqttServiceAction = createAsyncAction(
    MQTT.RELOAD.REQUEST,
    MQTT.RELOAD.SUCCESS,
    MQTT.RELOAD.ERROR
)<undefined, IServiceStatus, string>();

type ReloadMqttServiceAction = ActionType<typeof reloadMqttServiceAction>;

export const reloadMqttService = (): Thunk<ReloadMqttServiceAction | AlertAction> => (
    dispatch
): Promise<IServiceStatus | void | IError> => {

    // @ts-ignore
    dispatch(reloadMqttServiceAction.request());
    return api.mqtt.reloadService()
        .then((payload: IServiceStatus) => {
            dispatch(reloadMqttServiceAction.success(payload))
        })
        .catch((err: string) => {
            dispatch(reloadMqttServiceAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));
            dispatch(fetchMqttServiceStatus());
        })
}

export const updateMqttServiceStatusAction = createAction(
    MQTT.SERVICE_STATUS.UPDATE,
    (status: IServiceStatus) => (status)
)();

type UpdateMqttServiceStatusAction = ActionType<typeof updateMqttServiceStatusAction>;

type MqttReducerActions =
    | ReloadMqttServiceAction
    | FetchMqttServiceStatusAction
    | UpdateMqttServiceStatusAction;


export function mqttReducer(
    state: IMqttState = initialState,
    action: MqttReducerActions
): IMqttState {
    switch (action.type) {

        case MQTT.RELOAD.REQUEST:
        case MQTT.SERVICE_STATUS.FETCH.REQUEST:
            return {...state, loading: true}

        case MQTT.RELOAD.SUCCESS:
        case MQTT.SERVICE_STATUS.FETCH.SUCCESS:
            return {...state, ...action.payload, loading: false}

        case MQTT.RELOAD.ERROR:
        case MQTT.SERVICE_STATUS.FETCH.ERROR:
            return {...state, loading: false}

        case MQTT.SERVICE_STATUS.UPDATE:
            return {...state, ...action.payload}

        default:
            return state;
    }
}
