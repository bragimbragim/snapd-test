import {RootState} from '../../types/RootState';
import {IServiceStatus} from '../../../../globalTypes/ServiceStatus';

export const getMqttServiceStatus = (state: RootState): IServiceStatus => {
    const serviceStatus = {...state.mqtt};
    delete serviceStatus.loading;
    return serviceStatus;
};

export const getMqttServiceLoading = (state: RootState): boolean => state.mqtt.loading;
