import {RootState} from '../../types/RootState';
import {AmazonStatusCode} from '../../../../src/types/AmazonStatus';
import {IAmazonQueueItemStatus} from '../../../../globalTypes/AmazonQueueItemStatus';
import {IServiceStatus} from '../../../../globalTypes/ServiceStatus';

export const getAmazonServiceStatus = (state: RootState): IServiceStatus => state.amazon.serviceStatus;

export const getAmazonQueueCount = (state: RootState): string => state.amazon.queueCount;

export const getAmazonServiceLoading = (state: RootState): boolean => state.amazon.loading;

export const getAmazonQueueList = (state: RootState): IAmazonQueueItemStatus[] => state.amazon.queueList;

export const getAmazonQueueErrors = (state: RootState): number =>
    state.amazon.queueList.filter((item) => item.code === AmazonStatusCode.ERROR).length;
