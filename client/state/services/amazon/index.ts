import {IAmazonState} from './types/IAmazonState';
import {ActionType, createAction, createAsyncAction} from 'typesafe-actions';
import {initServiceStatus, IServiceStatus} from '../../../../globalTypes/ServiceStatus';
import {AMAZON} from '../../actionTypes';
import {Thunk} from '../../types/Thunk';
import {AlertAction, openAlert} from '../../alert';
import {IError} from '../../../shared/types/IError';
import {api} from '../../../application/api';
import {ISnapdServices} from '../../../../globalTypes/ISnapdServices';
import {AlertType} from '../../alert/types/AlertType';
import {IAmazonQueueItemStatus} from '../../../../globalTypes/AmazonQueueItemStatus';

const initialState: IAmazonState = {
    loading: false,
    queueCount: '0',
    queueList: [],
    serviceStatus: initServiceStatus
}


const fetchAmazonServiceStatusAction = createAsyncAction(
    AMAZON.SERVICE_STATUS.FETCH.REQUEST,
    AMAZON.SERVICE_STATUS.FETCH.SUCCESS,
    AMAZON.SERVICE_STATUS.FETCH.ERROR
)<undefined, IServiceStatus, string>();

type FetchAmazonServiceStatusAction = ActionType<typeof fetchAmazonServiceStatusAction>;

export const fetchAmazonServiceStatus = (): Thunk<FetchAmazonServiceStatusAction | AlertAction> => (
    dispatch
): Promise<IServiceStatus | void | IError> => {

    // @ts-ignore
    dispatch(fetchAmazonServiceStatusAction.request());
    return api.system.getServicesStatus()
        .then((payload: ISnapdServices) => {
            dispatch(fetchAmazonServiceStatusAction.success(payload.amazon))
        })
        .catch((err: string) => {
            dispatch(fetchAmazonServiceStatusAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));
        })
}


const reloadAmazonServiceAction = createAsyncAction(
    AMAZON.RELOAD.REQUEST,
    AMAZON.RELOAD.SUCCESS,
    AMAZON.RELOAD.ERROR
)<undefined, IServiceStatus, string>();

type ReloadAmazonServiceAction = ActionType<typeof reloadAmazonServiceAction>;

export const reloadAmazonService = (): Thunk<ReloadAmazonServiceAction | AlertAction> => (
    dispatch
): Promise<IServiceStatus | void | IError> => {

    // @ts-ignore
    dispatch(reloadAmazonServiceAction.request());
    return api.amazon.reloadService()
        .then((payload: IServiceStatus) => {
            dispatch(reloadAmazonServiceAction.success(payload))
        })
        .catch((err: string) => {
            dispatch(reloadAmazonServiceAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));
            dispatch(fetchAmazonServiceStatus());
        })
}


export const updateAmazonServiceStatusAction = createAction(
    AMAZON.SERVICE_STATUS.UPDATE,
    (status: IServiceStatus) => (status)
)();

type UpdateAmazonServiceStatusAction = ActionType<typeof updateAmazonServiceStatusAction>;


export const updateAmazonQueueListAction = createAction(
    AMAZON.QUEUE.UPDATE.LIST,
    (status: IAmazonQueueItemStatus[]) => (status)
)();

type UpdateAmazonQueueListAction = ActionType<typeof updateAmazonQueueListAction>;


export const updateAmazonQueueCountAction = createAction(
    AMAZON.QUEUE.UPDATE.COUNT,
    (queue: string) => (queue)
)();

type UpdateAmazonQueueCountAction = ActionType<typeof updateAmazonQueueCountAction>;

type AmazonReducerActions =
    | ReloadAmazonServiceAction
    | FetchAmazonServiceStatusAction
    | UpdateAmazonQueueCountAction
    | UpdateAmazonServiceStatusAction
    | UpdateAmazonQueueListAction;


export function amazonReducer(
    state: IAmazonState = initialState,
    action: AmazonReducerActions
): IAmazonState {
    switch (action.type) {

        case AMAZON.RELOAD.REQUEST:
        case AMAZON.SERVICE_STATUS.FETCH.REQUEST:
            return {...state, loading: true}

        case  AMAZON.RELOAD.SUCCESS:
        case AMAZON.SERVICE_STATUS.FETCH.SUCCESS:
            return {...state, loading: false, serviceStatus: action.payload}

        case AMAZON.QUEUE.UPDATE.COUNT:
            return {...state, queueCount: action.payload}

        case AMAZON.SERVICE_STATUS.UPDATE:
            return {...state, serviceStatus: action.payload}

        case AMAZON.QUEUE.UPDATE.LIST:
            return {...state, queueList: action.payload}

        case AMAZON.RELOAD.ERROR:
        case AMAZON.SERVICE_STATUS.FETCH.ERROR:
            return {...state, loading: false}

        default:
            return state;
    }
}
