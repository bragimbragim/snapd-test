import {IAmazonQueueItemStatus} from '../../../../../globalTypes/AmazonQueueItemStatus';
import {IServiceStatus} from '../../../../../globalTypes/ServiceStatus';

export interface IAmazonState {
    loading: boolean;
    queueCount: string;
    queueList: IAmazonQueueItemStatus[];
    serviceStatus: IServiceStatus;
}
