import {ActionType, createAction, createAsyncAction} from 'typesafe-actions';
import {initServiceStatus, IServiceStatus} from '../../../../globalTypes/ServiceStatus';
import {CAMERA} from '../../actionTypes';
import {Thunk} from '../../types/Thunk';
import {AlertAction, openAlert} from '../../alert';
import {IError} from '../../../shared/types/IError';
import {api} from '../../../application/api';
import {ISnapdServices} from '../../../../globalTypes/ISnapdServices';
import {AlertType} from '../../alert/types/AlertType';
import {ICameraState} from './types/ICameraState';

const initialState: ICameraState = {
    ...initServiceStatus,
    loading: false
}


const fetchCameraServiceStatusAction = createAsyncAction(
    CAMERA.SERVICE_STATUS.FETCH.REQUEST,
    CAMERA.SERVICE_STATUS.FETCH.SUCCESS,
    CAMERA.SERVICE_STATUS.FETCH.ERROR
)<undefined, IServiceStatus, string>();

type FetchCameraServiceStatusAction = ActionType<typeof fetchCameraServiceStatusAction>;

export const fetchCameraServiceStatus = (): Thunk<FetchCameraServiceStatusAction | AlertAction> => (
    dispatch
): Promise<IServiceStatus | void | IError> => {

    // @ts-ignore
    dispatch(fetchCameraServiceStatusAction.request());
    return api.system.getServicesStatus()
        .then((payload: ISnapdServices) => {
            dispatch(fetchCameraServiceStatusAction.success(payload.camera))
        })
        .catch((err: string) => {
            dispatch(fetchCameraServiceStatusAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));
        })
}


const reloadCameraServiceAction = createAsyncAction(
    CAMERA.RELOAD.REQUEST,
    CAMERA.RELOAD.SUCCESS,
    CAMERA.RELOAD.ERROR
)<undefined, IServiceStatus, string>();

type ReloadCameraServiceAction = ActionType<typeof reloadCameraServiceAction>;

export const reloadCameraService = (): Thunk<ReloadCameraServiceAction | AlertAction> => (
    dispatch
): Promise<IServiceStatus | void | IError> => {

    // @ts-ignore
    dispatch(reloadCameraServiceAction.request());
    return api.camera.reloadCamera()
        .then((payload: IServiceStatus) => {
            dispatch(reloadCameraServiceAction.success(payload))
        })
        .catch((err: string) => {
            dispatch(reloadCameraServiceAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR));
            dispatch(fetchCameraServiceStatus());
        })
}

export const updateCameraServiceStatusAction = createAction(
    CAMERA.SERVICE_STATUS.UPDATE,
    (status: IServiceStatus) => (status)
)();

type UpdateCameraServiceStatusAction = ActionType<typeof updateCameraServiceStatusAction>;

type MqttReducerActions =
    | ReloadCameraServiceAction
    | FetchCameraServiceStatusAction
    | UpdateCameraServiceStatusAction;


export function cameraReducer(
    state: ICameraState = initialState,
    action: MqttReducerActions
): ICameraState {
    switch (action.type) {

        case CAMERA.RELOAD.REQUEST:
        case CAMERA.SERVICE_STATUS.FETCH.REQUEST:
            return {...state, loading: true}

        case CAMERA.RELOAD.SUCCESS:
        case CAMERA.SERVICE_STATUS.FETCH.SUCCESS:
            return {...state, ...action.payload, loading: false}

        case CAMERA.RELOAD.ERROR:
        case CAMERA.SERVICE_STATUS.FETCH.ERROR:
            return {...state, loading: false}

        case CAMERA.SERVICE_STATUS.UPDATE:
            return {...state, ...action.payload}

        default:
            return state;
    }
}
