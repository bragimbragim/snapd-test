import {RootState} from '../../types/RootState';
import {IServiceStatus} from '../../../../globalTypes/ServiceStatus';

export const getCameraServiceStatus = (state: RootState): IServiceStatus => {
    const serviceStatus = {...state.camera};
    delete serviceStatus.loading;
    return serviceStatus;
};

export const getCameraServiceLoading = (state: RootState): boolean => state.camera.loading;
