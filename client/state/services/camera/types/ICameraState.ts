import {IServiceStatus} from '../../../../../globalTypes/ServiceStatus';

export interface ICameraState extends IServiceStatus {
    loading: boolean;
}
