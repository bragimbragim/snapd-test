import {RootState} from '../types/RootState';
import {IGalleryState} from './types/IGalleryState';
import {IPhotoGallery} from '../../routes/PhotoGallery/types/IPhotoGallery';

export const getGallery = (state: RootState): IGalleryState => state.gallery;

export const getSelectedPhotos = (state: RootState): IPhotoGallery[] => {
    return state.gallery.photos.filter((photo) => photo.isSelected);
}

export const getGalleryLoading = (state: RootState): boolean => state.gallery.loading;
