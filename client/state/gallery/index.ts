import {IGalleryState} from './types/IGalleryState';
import {ActionType, createAction, createAsyncAction} from 'typesafe-actions';
import {GALLERY} from '../actionTypes';
import {IPhotoGallery} from '../../routes/PhotoGallery/types/IPhotoGallery';
import {Thunk} from '../types/Thunk';
import {AlertAction, openAlert} from '../alert';
import {IError} from '../../shared/types/IError';
import {api} from '../../application/api';
import {AlertType} from '../alert/types/AlertType';
import {UserRole} from '../../shared/types/UserRole';

const initialState: IGalleryState = {
    photos: [],
    loading: false,
    isSelectedAll: false
}


const getPhotosAction = createAsyncAction(
    GALLERY.PHOTOS.GET.REQUEST,
    GALLERY.PHOTOS.GET.SUCCESS,
    GALLERY.PHOTOS.GET.ERROR
)<undefined, IPhotoGallery[], string>();

type GetPhotosAction = ActionType<typeof getPhotosAction>;

export const getPhotos = (role: UserRole): Thunk<GetPhotosAction | AlertAction> => (
    dispatch
): Promise<IPhotoGallery[] | void | IError> => {

    // @ts-ignore
    dispatch(getPhotosAction.request());
    return api.gallery.getPhotos(role)
        .then((payload: IPhotoGallery[]) => {
            dispatch(getPhotosAction.success(payload))
        })
        .catch((err: string) => {
            dispatch(getPhotosAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR))
        })
}


export const selectPhoto = createAction(
    GALLERY.PHOTOS.TOGGLE.ONE,
    (id: number) => ({id})
)();

type SelectPhotoAction = ActionType<typeof selectPhoto>;

export const selectAllPhotos = createAction(
    GALLERY.PHOTOS.TOGGLE.ALL,
    (value: boolean) => ({value})
)();

type SelectAllPhotos = ActionType<typeof selectAllPhotos>;


const downloadPhotosAction = createAsyncAction(
    GALLERY.PHOTOS.DOWNLOAD.REQUEST,
    GALLERY.PHOTOS.DOWNLOAD.SUCCESS,
    GALLERY.PHOTOS.DOWNLOAD.ERROR
)<undefined, undefined, string>();

type DownloadPhotosAction = ActionType<typeof downloadPhotosAction>;

export const downloadPhotos = (photosForDownload: IPhotoGallery[]): Thunk<DownloadPhotosAction | AlertAction> => (
    dispatch
): Promise<any | void | IError> => {

    // @ts-ignore
    dispatch(downloadPhotosAction.request());
    return api.gallery.downloadPhotos(photosForDownload)
        .then((file: any) => {
            // @ts-ignore
            dispatch(downloadPhotosAction.success());
            return file;
        })
        .catch((err: string) => {
            dispatch(downloadPhotosAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR))
        })
}


const deleteSelectedPhotosAction = createAsyncAction(
    GALLERY.PHOTOS.DELETE.SELECTED.REQUEST,
    GALLERY.PHOTOS.DELETE.SELECTED.SUCCESS,
    GALLERY.PHOTOS.DELETE.SELECTED.ERROR
)<undefined, IPhotoGallery[], string>();

type DeleteSelectedPhotosAction = ActionType<typeof deleteSelectedPhotosAction>;

export const deleteSelectedPhotos = (photosForDelete: IPhotoGallery[]): Thunk<DeleteSelectedPhotosAction | AlertAction> => (
    dispatch
): Promise<any | void | IError> => {

    // @ts-ignore
    dispatch(deleteSelectedPhotosAction.request());
    return api.gallery.deletePhotos(photosForDelete)
        .then((payload: string) => {
            dispatch(deleteSelectedPhotosAction.success(photosForDelete));
            dispatch(openAlert(payload, AlertType.SUCCESS));
        })
        .catch((err: string) => {
            dispatch(deleteSelectedPhotosAction.failure(err));
            dispatch(openAlert(err, AlertType.ERROR))
        })
}

const deleteAllPhotosAction = createAsyncAction(
    GALLERY.PHOTOS.DELETE.ALL.REQUEST,
    GALLERY.PHOTOS.DELETE.ALL.SUCCESS,
    GALLERY.PHOTOS.DELETE.ALL.ERROR
)<undefined, IPhotoGallery[], string>();

type DeleteAllPhotosAction = ActionType<typeof deleteAllPhotosAction>;

export const deleteAllPhotos = (): Thunk<DeleteAllPhotosAction | AlertAction> => (
    dispatch
): Promise<any | void | IError> => {

    // @ts-ignore
    dispatch(deleteAllPhotosAction.request());
    return api.gallery.deleteAllPhotosFromUnit()
        .then((payload: string) => {
            dispatch(deleteAllPhotosAction.success([]))
            dispatch(openAlert(payload, AlertType.SUCCESS))
        })
        .catch((err: string) => {
            dispatch(deleteAllPhotosAction.failure(err))
            dispatch(openAlert(err, AlertType.ERROR))
        })
}


type GalleryReducerActions =
    | GetPhotosAction
    | SelectPhotoAction
    | SelectAllPhotos
    | DownloadPhotosAction
    | DeleteSelectedPhotosAction
    | DeleteAllPhotosAction;


export function galleryReducer(
    state: IGalleryState = initialState,
    action: GalleryReducerActions
): IGalleryState {
    switch (action.type) {

        case GALLERY.PHOTOS.GET.REQUEST:
        case GALLERY.PHOTOS.DOWNLOAD.REQUEST:
        case GALLERY.PHOTOS.DELETE.SELECTED.REQUEST:
        case GALLERY.PHOTOS.DELETE.ALL.REQUEST:
            return {...state, loading: true}

        case GALLERY.PHOTOS.GET.SUCCESS:
            return {...state, photos: action.payload, loading: false}

        case GALLERY.PHOTOS.DOWNLOAD.SUCCESS:
            return {...state, loading: false, isSelectedAll: false}

        case GALLERY.PHOTOS.DELETE.SELECTED.SUCCESS:
            const removedPhotos = action.payload;
            return {
                ...state,
                loading: false,
                photos: state.photos.filter((photo) => removedPhotos.indexOf(photo) < 0),
                isSelectedAll: false
            }

        case GALLERY.PHOTOS.DELETE.ALL.SUCCESS:
            return {...state, loading: false, photos: [], isSelectedAll: false}

        case GALLERY.PHOTOS.GET.ERROR:
        case GALLERY.PHOTOS.DOWNLOAD.ERROR:
        case GALLERY.PHOTOS.DELETE.SELECTED.ERROR:
        case GALLERY.PHOTOS.DELETE.ALL.ERROR:
            return {...state, loading: false}

        case GALLERY.PHOTOS.TOGGLE.ONE:
            const photos = state.photos
                .map((photo, index) => {
                    if (index === action.payload.id) {
                        photo.isSelected = !photo.isSelected;
                    }
                    return photo;
                });
            const isSelectedAll = photos.every((photo) => photo.isSelected);
            return {...state, isSelectedAll, photos}

        case GALLERY.PHOTOS.TOGGLE.ALL:
            return {
                ...state,
                photos: state.photos
                    .map((photo) => {
                        photo.isSelected = action.payload.value;
                        return photo;
                    }),
                isSelectedAll: action.payload.value
            }

        default:
            return {...state}

    }
}
