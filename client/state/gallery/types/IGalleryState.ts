import {IPhotoGallery} from '../../../routes/PhotoGallery/types/IPhotoGallery';

export interface IGalleryState {
    photos: IPhotoGallery[];
    loading: boolean;
    isSelectedAll: boolean;
}
