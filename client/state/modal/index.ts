import {IModalState} from './types/IModalState';
import {ActionType, createAction} from 'typesafe-actions';
import {MODAL} from '../actionTypes';
import {ReactElement} from 'react';

const initialState: IModalState = {
    open: false,
    component: null
}

export const openModal = createAction(
    MODAL.OPEN,
    (
        component: ReactElement,
        props: any = {}
    ) => ({component, props})
)();
type OpenModalAction = ActionType<typeof openModal>;

export const closeModal = createAction(MODAL.CLOSE)();
type CloseModalAction = ActionType<typeof closeModal>;

export type ModalAction =
    | OpenModalAction
    | CloseModalAction;

export function modalReducer(
    state = initialState,
    action: ModalAction
): IModalState {
    switch (action.type) {
        case MODAL.OPEN:
            return {...state, open: true, ...action.payload}
        case  MODAL.CLOSE:
            return {...state, open: false, component: null}
        default:
            return state;
    }
}
