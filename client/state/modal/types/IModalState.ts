import {ReactElement, ReactNode} from 'react';
import {DialogProps} from '@material-ui/core';

export interface IModalState {
    open: boolean;
    component: ReactElement;
    props?: DialogProps;
}
