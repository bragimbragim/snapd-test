import {RootState} from '../types/RootState';
import {IModalState} from './types/IModalState';

export const getModal = (state: RootState): IModalState => state.modal;
