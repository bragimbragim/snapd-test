import {Action} from 'typesafe-actions';
import {ThunkAction} from 'redux-thunk';
import {RootState} from './RootState';

export type Thunk<A extends Action, ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, A>;
