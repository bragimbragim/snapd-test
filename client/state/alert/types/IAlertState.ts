import {AlertType} from './AlertType';

export interface IAlertState {
    message: string;
    severity: AlertType;
    isOpen: boolean;
}
