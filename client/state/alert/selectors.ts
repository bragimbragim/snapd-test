import {RootState} from '../types/RootState';
import {IAlertState} from './types/IAlertState';

export const getAlert = (state: RootState): IAlertState => state.alert;
