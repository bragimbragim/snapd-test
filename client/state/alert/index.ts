import {IAlertState} from './types/IAlertState';
import {ActionType, createAction} from 'typesafe-actions';
import {ALERT} from '../actionTypes';
import {AlertType} from './types/AlertType';

const initialState: IAlertState = {
    message: '',
    severity: AlertType.INFO,
    isOpen: false
}

export const openAlert = createAction(
    ALERT.OPEN,
    (
        message: string,
        severity: AlertType = AlertType.INFO
    ) => ({message, severity, isOpen: true})
)();
type OpenAlertAction = ActionType<typeof openAlert>;

export const closeAlert = createAction(ALERT.CLOSE)();
type CloseAlertAction = ActionType<typeof closeAlert>;

export type AlertAction =
    | OpenAlertAction
    | CloseAlertAction;


export function alertReducer(
    state = initialState,
    action: AlertAction
): IAlertState {
    switch (action.type) {
        case ALERT.OPEN:
            return {...state, ...action.payload}
        case  ALERT.CLOSE:
            return {...state, isOpen: false}
        default:
            return state;
    }
}
