import {combineReducers} from "redux";
import {modalReducer} from './modal';
import {alertReducer} from './alert';
import {readerReducer} from './reader';
import {galleryReducer} from './gallery';
import {memoryReducer} from './services/memory';
import {amazonReducer} from './services/amazon';
import {mqttReducer} from './services/mqtt';
import {cameraReducer} from './services/camera';
import {wifiReducer} from './wifi';


export default combineReducers({
    alert: alertReducer,
    modal: modalReducer,
    reader: readerReducer,
    gallery: galleryReducer,
    mqtt: mqttReducer,
    camera: cameraReducer,
    memory: memoryReducer,
    amazon: amazonReducer,
    wifi: wifiReducer
});
