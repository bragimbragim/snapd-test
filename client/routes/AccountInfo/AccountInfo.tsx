import React, {ReactElement} from 'react';
import {Typography} from '@material-ui/core';

export default function AccountInfo(): ReactElement {

    return <>
        <Typography align='center' variant='h1'>ACCOUNT INFO</Typography>
    </>
}
