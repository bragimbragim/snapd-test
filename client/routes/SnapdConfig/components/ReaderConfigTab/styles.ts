import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    select: {
        width: '250px'
    },
    autoStartControl: {
        marginLeft: '40px'
    }
}));
