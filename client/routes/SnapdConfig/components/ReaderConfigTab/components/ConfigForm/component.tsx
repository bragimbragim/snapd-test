import React, {ChangeEvent, useEffect, useState} from "react";
import {Controller, useForm} from 'react-hook-form';
import {
    Box,
    Button,
    Divider,
    FormControl,
    FormControlLabel,
    FormLabel,
    MenuItem,
    Radio,
    RadioGroup,
    Slider
} from '@material-ui/core';
import {useDispatch} from 'react-redux';
import CustomTooltip from '../../../../../../shared/components/Tooltip/component';
import {IReaderConfig} from '../../../../../../../globalTypes/IReaderConfig';
import {AlertType} from '../../../../../../state/alert/types/AlertType';
import ReactHookFormSelect from '../../../../../../shared/components/ReactHookFormSelect/component';
import {openAlert} from '../../../../../../state/alert';
import useStyles from './styles';
import {IReaderConfigTabChildProps} from '../../types/IReaderConfigTabChildProps';
import {api} from '../../../../../../application/api';

export default function ReaderConfigForm({config, disabled, onLoading}: IReaderConfigTabChildProps) {

    const {
        watch,
        reset,
        handleSubmit,
        control
    } = useForm<IReaderConfig>({
        mode: 'onBlur',
        defaultValues: config
    });

    const [sliderCaption, setSliderCaption] = useState<number>(10);

    const dispatch = useDispatch();
    const classes = useStyles();

    const qField = watch('Q');
    const rfPowerValues = [
        {value: 5, label: '5dBm'},
        {value: 30, label: '30.0dBm'}
    ];

    const onSubmit = (data: IReaderConfig) => {
        data = {...data, autoStart: config.autoStart};
        onLoading(true);
        if (data.Q === 'DynamicQ') {
            data.staticQ = 0;
        }
        data.BLF = 'LINK250KHZ';
        data.target = 'A';
        api.reader.setOptions(data)
            .then((data: string) => dispatch(openAlert(data, AlertType.SUCCESS)))
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => onLoading(false))
    }

    const updateSliderCaption = (event, newValue: number) => {
        setSliderCaption(Math.round(Math.pow(10, newValue / 10)));
    }

    useEffect(() => {
        reset(config);
        updateSliderCaption(null, config.rfPower);
    }, [reset, config]);

    return <>
        <form onSubmit={handleSubmit(onSubmit)}>
            <Box display='flex' flexDirection='column' flexWrap='wrap'>

                <Box mt={2}>
                    <FormLabel component="legend" className={classes.formLabel}>RF Power</FormLabel>
                    <FormControl disabled={disabled} className={classes.formControl}>
                        <Box className={[classes.controller, classes.slider].join(' ')}>
                            <Controller
                                name="rfPower"
                                control={control}
                                render={({value, onChange}) => (
                                    <Slider
                                        disabled={disabled}
                                        value={value}
                                        defaultValue={10}
                                        step={0.5}
                                        min={5}
                                        max={30}
                                        marks={rfPowerValues}
                                        aria-labelledby="discrete-slider-custom"
                                        valueLabelDisplay="auto"
                                        onChange={(e: ChangeEvent, value: number) => onChange(value)}
                                        onChangeCommitted={updateSliderCaption}
                                    />
                                )}
                            />
                        </Box>

                        <span className={classes.sliderCaption}>({sliderCaption}) mW</span>
                    </FormControl>
                    <Divider/>
                </Box>

                <Box>
                    <FormControl disabled={disabled} className={classes.formControl}>
                        <FormLabel component="legend" className={classes.formLabel}>BLF</FormLabel>
                        <Controller
                            name="BLF"
                            control={control}
                            className={classes.controller}
                            as={<RadioGroup aria-label="BLF">
                                <FormControlLabel
                                    value="LINK250KHZ"
                                    control={<Radio/>}
                                    label="LINK250KHZ"
                                />
                                <FormControlLabel
                                    value="LINK640KHZ"
                                    control={<Radio/>}
                                    label="LINK640KHZ"
                                />
                            </RadioGroup>}
                        />

                    </FormControl>
                    <Divider/>
                </Box>

                <Box>
                    <FormControl disabled={disabled} className={classes.formControl}>
                        <FormLabel component="legend" className={classes.formLabel}>Tari</FormLabel>
                        <Controller
                            name="tari"
                            control={control}
                            className={classes.controller}
                            as={<RadioGroup aria-label="tari">
                                <FormControlLabel
                                    value="TARI6_25US"
                                    control={<Radio/>}
                                    label="TARI6_25US"
                                />
                                <FormControlLabel
                                    value="TARI12_5US"
                                    control={<Radio/>}
                                    label="TARI12_5US"
                                />
                                <FormControlLabel
                                    value="TARI25US"
                                    control={<Radio/>}
                                    label="TARI25US"
                                />
                            </RadioGroup>}
                        />

                    </FormControl>
                    <Divider/>
                </Box>

                <Box>
                    <FormControl disabled={disabled} className={classes.formControl}>
                        <FormLabel component="legend" className={classes.formLabel}>Tag Encoding</FormLabel>
                        <Controller
                            name="tagEncoding"
                            control={control}
                            className={classes.controller}
                            as={<RadioGroup aria-label="tagEncoding">
                                <FormControlLabel
                                    value="FM0"
                                    control={<Radio/>}
                                    label="FM0"
                                />
                                <FormControlLabel
                                    value="M2"
                                    control={<Radio/>}
                                    label="M2"
                                />
                                <FormControlLabel
                                    value="M4"
                                    control={<Radio/>}
                                    label="M4"
                                />
                                <FormControlLabel
                                    value="M8"
                                    control={<Radio/>}
                                    label="M8"
                                />
                            </RadioGroup>}
                        />

                    </FormControl>
                    <Divider/>
                </Box>

                <Box>
                    <FormControl disabled={disabled} className={classes.formControl}>
                        <FormLabel component="legend" className={classes.formLabel}>Session</FormLabel>
                        <Controller
                            name="session"
                            control={control}
                            className={classes.controller}
                            as={<RadioGroup aria-label="session">
                                <FormControlLabel
                                    value="S0"
                                    control={<Radio/>}
                                    label="S0"
                                />
                                <FormControlLabel
                                    value="S1"
                                    control={<Radio/>}
                                    label="S1"
                                />
                                <FormControlLabel
                                    value="S2"
                                    control={<Radio/>}
                                    label="S2"
                                />
                                <FormControlLabel
                                    value="S3"
                                    control={<Radio/>}
                                    label="S3"
                                />
                            </RadioGroup>}
                        />

                    </FormControl>
                    <Divider/>
                </Box>

                <Box>
                    <FormControl disabled={disabled} className={classes.formControl}>
                        <FormLabel component="legend" className={classes.formLabel}>Target</FormLabel>
                        <Controller
                            name="target"
                            control={control}
                            className={classes.controller}
                            as={<RadioGroup aria-label="target">
                                <FormControlLabel
                                    value="A"
                                    control={<Radio/>}
                                    label="A"
                                />
                                <FormControlLabel
                                    value="B"
                                    control={<Radio/>}
                                    label="B"
                                />
                                <FormControlLabel
                                    value="AB"
                                    control={<Radio/>}
                                    label="AB"
                                />
                                <FormControlLabel
                                    value="BA"
                                    control={<Radio/>}
                                    label="BA"
                                />
                            </RadioGroup>}
                        />

                    </FormControl>
                    <Divider/>
                </Box>

                <Box>
                    <FormControl disabled={disabled} className={classes.formControl}>
                        <FormLabel component="legend" className={classes.formLabel}>Q</FormLabel>
                        <Controller
                            name="Q"
                            control={control}
                            className={classes.controller}
                            as={<RadioGroup aria-label="Q">
                                <FormControlLabel
                                    value="DynamicQ"
                                    control={<Radio/>}
                                    label="DynamicQ"
                                />
                                <FormControlLabel
                                    value="StaticQ"
                                    control={<Radio/>}
                                    label="StaticQ"
                                />
                            </RadioGroup>}
                        />

                        <Box display='flex' flexDirection='row'>
                            <ReactHookFormSelect
                                id="mode"
                                name="staticQ"
                                control={control}
                                defaultValue='0'
                                disabled={qField !== 'StaticQ'}
                            >
                                {
                                    Array.from(
                                        {length: 16},
                                        (_, i) => (<MenuItem key={i} value={i}>{i}</MenuItem>)
                                    )
                                }
                            </ReactHookFormSelect>
                        </Box>

                    </FormControl>
                </Box>

            </Box>

            <Box mt={2}>
                <Button
                    type='submit'
                    variant='contained'
                    color='primary'
                >Save config</Button>
            </Box>
        </form>
    </>
}
