import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    formControl: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%'
    },
    formLabel: {
        position: 'absolute'
    },
    controller: {
        flexDirection: 'row',
        marginLeft: '150px'
    },
    slider: {
        width: '50%'
    },
    sliderCaption: {
        position: 'absolute'
    }
}));
