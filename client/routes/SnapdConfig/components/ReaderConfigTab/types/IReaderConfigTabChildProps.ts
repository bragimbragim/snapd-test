import {IDefaultComponentProps} from '../../../../../shared/types/IDefaultComponentProps';
import {IReaderConfig} from '../../../../../../globalTypes/IReaderConfig';

export interface IReaderConfigTabChildProps extends IDefaultComponentProps {
    config: IReaderConfig;
    disabled: boolean;
    onLoading: (newValue: boolean) => void;
}
