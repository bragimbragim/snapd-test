import React, {useEffect, useState} from "react";
import {
    Box,
    FormControl,
    FormControlLabel,
    InputLabel,
    MenuItem,
    Select,
    Switch
} from '@material-ui/core';
import {IReaderConfig} from '../../../../../globalTypes/IReaderConfig';
import ReaderConfigForm from './components/ConfigForm/component';
import {
    DefaultConfig,
    MaxDistanceConfig,
    MaxSpeedConfig
} from '../../../../../src/configs/ReaderConfig';
import useStyles from './styles';
import CustomTooltip from '../../../../shared/components/Tooltip/component';
import {CAMERA as CameraOptionsDefinition} from '../../../../application/OptionsDefinitions';
import {ConfigType} from './types/ConfigType';
import {openAlert} from '../../../../state/alert';
import {AlertType} from '../../../../state/alert/types/AlertType';
import {useDispatch} from 'react-redux';
import {BackDrop} from '../../../../shared/components/BackDrop/component';
import {api} from '../../../../application/api';

export default function ReaderConfigTab() {

    const [currReaderConfig, setCurrReaderConfig] = useState<IReaderConfig>(null);
    const [newConfig, setNewConfig] = useState<IReaderConfig>(DefaultConfig);
    const [configType, setConfigType] = useState<ConfigType>(ConfigType.MANUALLY);
    const [autoStart, setAutoStart] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);

    const dispatch = useDispatch();
    const classes = useStyles();

    const handleSelect = (event: React.ChangeEvent<{ value: ConfigType }>) => {
        const choose = event.target.value;
        setConfigType(choose);
        if (choose === ConfigType.MAX_SPEED) {
            setNewConfig(MaxSpeedConfig);
        }
        if (choose === ConfigType.MAX_DISTANCE) {
            setNewConfig(MaxDistanceConfig);
        }
        if (choose === ConfigType.MANUALLY) {
            setNewConfig(currReaderConfig);
        }
    }

    const handleAutoStart = (event: React.ChangeEvent<HTMLInputElement>) => {
        setAutoStart(event.target.checked);
        setNewConfig({
            ...newConfig,
            autoStart: event.target.checked
        });
    }

    const handleLoading = (newValue: boolean) => {
        setLoading(newValue);
    }

    const getCurrReaderConfig = () => {
        setLoading(true);
        api.reader.getCurrConfig()
            .then((data: IReaderConfig) => {
                data.rfPower = Number(data.rfPower);
                setCurrReaderConfig(data);
                setNewConfig(data);
                setAutoStart(data.autoStart);
            })
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => setLoading(false))
    }

    useEffect(() => {
        getCurrReaderConfig();
    }, []);

    return <>
        {loading && <BackDrop/>}
        <Box mb={2} display='flex' alignItems='center'>
            <FormControl className={classes.select}>
                <InputLabel>Reader Profile</InputLabel>
                <Select value={configType} onChange={handleSelect}>
                    <MenuItem disabled={true} value={ConfigType.MAX_SPEED}>Maximum Speed</MenuItem>
                    <MenuItem disabled={true} value={ConfigType.MAX_DISTANCE}>Maximum Distance</MenuItem>
                    <MenuItem value={ConfigType.MANUALLY}>Manually</MenuItem>
                </Select>
            </FormControl>

            <FormControl className={classes.autoStartControl}>
                <FormControlLabel
                    control={<Switch checked={autoStart} onChange={handleAutoStart}/>}
                    label="Auto Start"
                />
            </FormControl>
        </Box>

        <ReaderConfigForm
            config={newConfig}
            disabled={configType !== ConfigType.MANUALLY}
            onLoading={handleLoading}
        />
    </>
}
