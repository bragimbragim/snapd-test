import React, {useEffect, useState} from 'react';
import {Box, Button, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {openAlert} from '../../../../state/alert';
import {AlertType} from '../../../../state/alert/types/AlertType';
import {updateReaderStatusAction} from '../../../../state/reader';
import {BackDrop} from '../../../../shared/components/BackDrop/component';
import Gallery from 'react-grid-gallery';
import {IGallery} from '../../../../shared/types/IGallery';
import {ReaderStatusCode} from '../../../../../globalTypes/ReaderStatus';
import config from '../../../../config';
import appConfig from '../../../../config';
import {CheckPhotoRes} from '../../../../../globalTypes/CheckPhotosRes';
import {api} from '../../../../application/api';
import {io, Socket} from 'socket.io-client';
import {SocketEvent} from '../../../../../globalTypes/SocketEvent';
import {getReaderStatus} from '../../../../state/reader/selectors';

let socket: Socket;

export default function TimelapseTab() {

    const [loading, setLoading] = useState<boolean>(false);
    const [photos, setPhotos] = useState<IGallery[]>([]);
    const [caption, setCaption] = useState<string>('Waiting For Tags...');
    const reader = useSelector(getReaderStatus);
    const dispatch = useDispatch();

    const connectToSocketIO = () => {
        if (socket?.connected) {
            return;
        }
        socket = io(`${appConfig.API_URL}/timelapse`, {
            transports: ['websocket'],
            reconnection: false
        });

        socket.on(SocketEvent.TIMELAPSE.TAG_DETECTED, () => {
            setCaption('Making Photos...');
            setTimeout(() => {
                socket.emit(SocketEvent.TIMELAPSE.STOP);
            }, 3000);
        });

        socket.on(SocketEvent.TIMELAPSE.SUCCESS, (photos) => {
            const newPhotos: IGallery[] = photos.map(mapPhoto);
            setCaption('Waiting For Tags...');
            setPhotos(newPhotos);
            setLoading(false);
            dispatch(updateReaderStatusAction({code: ReaderStatusCode.INACTIVE}))
        });

        socket.on(SocketEvent.TIMELAPSE.ERROR, (err) => {
            setCaption('Waiting For Tags...');
            dispatch(openAlert(err, AlertType.ERROR));
            setLoading(false);
            dispatch(updateReaderStatusAction({code: ReaderStatusCode.INACTIVE}))
        });
    }

    const disconnectFromSocketIO = () => {
        if (socket?.connected) {
            socket.close();
        }
    }

    const startTimelapse = () => {
        if (reader.code === ReaderStatusCode.GLOBAL_ERROR) {
            return;
        }
        setLoading(true);
        dispatch(updateReaderStatusAction({code: ReaderStatusCode.ACTIVE}));
        socket.emit(SocketEvent.TIMELAPSE.START);
    }

    const mapPhoto = (photo: CheckPhotoRes): IGallery => {
        return {
            src: config.API_URL + photo.path,
            thumbnail: config.API_URL + photo.path,
            tags: [{value: `${photo.time} ms`, title: `${photo.time} ms`}],
            thumbnailWidth: 600,
            thumbnailHeight: 400
        }
    }

    const cancelTimelapse = () => {
        socket.emit(SocketEvent.TIMELAPSE.STOP);
    }

    const clearTestPhotosDir = () => {
        api.gallery.deleteTestPhotos()
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
    }

    useEffect(() => {
        connectToSocketIO();

        return () => {
            clearTestPhotosDir();
            disconnectFromSocketIO();
        }
    }, []);

    return <>
        {
            loading &&
            <BackDrop>
                <Typography variant='h5'>{caption}</Typography>
                <Button
                    variant='contained'
                    color='primary'
                    onClick={cancelTimelapse}
                >Close</Button>
            </BackDrop>
        }
        <Box mt={2} mb={2}>
            <Button
                fullWidth
                variant='outlined'
                color='primary'
                onClick={startTimelapse}
                disabled={reader.code === ReaderStatusCode.GLOBAL_ERROR}
            >Calibrate Camera</Button>
        </Box>

        <Box>
            <Gallery
                images={photos}
                enableImageSelection={false}
                margin={4}
                backdropClosesModal={true}
                preloadNextImage={false}
            />
        </Box>

    </>
}
