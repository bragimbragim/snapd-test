import React, {useEffect} from "react";
import {
    Box,
    Divider,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText
} from '@material-ui/core';
import SignalWifi0BarIcon from '@material-ui/icons/SignalWifi0Bar';
import SignalWifi1BarIcon from '@material-ui/icons/SignalWifi1Bar';
import SignalWifi2BarIcon from '@material-ui/icons/SignalWifi2Bar';
import SignalWifi3BarIcon from '@material-ui/icons/SignalWifi3Bar';
import SignalWifi4BarIcon from '@material-ui/icons/SignalWifi4Bar';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import {useDispatch, useSelector} from 'react-redux';
import {
    getNewNetworksListItems,
    getSavedNetworksListItems,
    getWifiLoading
} from '../../../../../../state/wifi/selectors';
import {IWifiItemResponse} from '../../../../../../../globalTypes/IWifi';
import {WifiConnectDialog} from './components/WifiConnectDialog/WifiConnectDialog';
import {openModal} from '../../../../../../state/modal';
import {connectToSavedNetwork, getListOfNetworks, removeNetwork} from '../../../../../../state/wifi';
import {Spinner} from '../../../../../../shared/components/Spinner/component';

export function WifiControl() {

    const newNetworksList = useSelector(getNewNetworksListItems);
    const savedNetworksList = useSelector(getSavedNetworksListItems);
    const loading = useSelector(getWifiLoading);

    const dispatch = useDispatch();

    const getWifiIconBySignalLevel = (signalLevel: number) => {
        if (signalLevel > -50) {
            return <SignalWifi4BarIcon color='primary'/>
        }
        if (signalLevel > -60 && signalLevel <= -50) {
            return <SignalWifi3BarIcon color='primary'/>
        }
        if (signalLevel > -70 && signalLevel <= -60) {
            return <SignalWifi2BarIcon color='primary'/>
        }
        if (signalLevel > -80 && signalLevel <= -70) {
            return <SignalWifi1BarIcon color='primary'/>
        }
        if (signalLevel <= -80) {
            return <SignalWifi0BarIcon color='primary'/>
        }
    }

    const connectToNew = (item: IWifiItemResponse) => {
        dispatch(openModal(<WifiConnectDialog ssid={item.ssid}/>));
    }

    const connectToSaved = (item: IWifiItemResponse) => {
        dispatch(connectToSavedNetwork(item.ssid));
    }

    const remove = (item: IWifiItemResponse) => {
        dispatch(removeNetwork(item));
    }

    const getCaption = (item: IWifiItemResponse) => {
        let caption = null;
        if (item.active) {
            caption = 'Connected';
        }
        if (item.error) {
            caption += ` with Error: ${item.error}`
        }
        return caption;
    }

    useEffect(() => {
        dispatch(getListOfNetworks());
    }, []);

    return <>
        <Box
            m={2}
            display='flex'
            flexDirection='column'
            style={{maxWidth: 365, position: 'relative'}}
        >
            {loading && <Spinner/>}
            <List component="nav">
                {
                    savedNetworksList
                        .map((item) => (
                            <ListItem
                                button
                                key={item.bssid}
                                selected={item.active}
                                onClick={() => connectToSaved(item)}
                            >
                                <ListItemIcon>
                                    {getWifiIconBySignalLevel(item.signalLevel)}
                                </ListItemIcon>
                                <ListItemText
                                    primary={item.ssid}
                                    secondary={getCaption(item)}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" onClick={() => remove(item)}>
                                        <HighlightOffIcon color='error'/>
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        ))
                }
            </List>
            <Divider/>
            <List component="nav">
                {
                    newNetworksList
                        .map((item) => (
                            <ListItem
                                button
                                key={item.bssid}
                                selected={item.active}
                                onClick={() => connectToNew(item)}
                            >
                                <ListItemIcon>
                                    {getWifiIconBySignalLevel(item.signalLevel)}
                                </ListItemIcon>
                                <ListItemText
                                    primary={item.ssid}
                                    secondary={getCaption(item)}
                                />
                            </ListItem>
                        ))
                }
            </List>
        </Box>
    </>
}
