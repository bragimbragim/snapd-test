import {IWifiItemResponse} from '../../../../../../../../globalTypes/IWifi';

export interface IWifiListProps {
    list: IWifiItemResponse[];
}
