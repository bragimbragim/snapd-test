import React, {useState} from "react";
import {Button, DialogActions, DialogContent, DialogTitle, TextField} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {closeModal} from '../../../../../../../../state/modal';
import {Spinner} from '../../../../../../../../shared/components/Spinner/component';
import {IWifiConnectDialogProps} from './types/IWifiConnectDialogProps';
import {connectToNewNetwork} from '../../../../../../../../state/wifi';
import {getWifiLoading} from '../../../../../../../../state/wifi/selectors';


export function WifiConnectDialog({ssid}: IWifiConnectDialogProps) {

    const loading = useSelector(getWifiLoading);
    const [psk, setPsk] = useState<string>();

    const dispatch = useDispatch();

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPsk(event.target.value);
    }

    const handleConnect = () => {
        (dispatch(connectToNewNetwork({ssid, psk})) as unknown as Promise<any>)
            .then(() => dispatch(closeModal()))
    }

    return <>
        {loading && <Spinner/>}
        <DialogTitle>{ssid}</DialogTitle>
        <DialogContent>
            <TextField
                label='password'
                type='password'
                onChange={handleChange}
            />
        </DialogContent>
        <DialogActions>
            <Button
                color="primary"
                onClick={handleConnect}
                autoFocus
            >Connect</Button>
        </DialogActions>
    </>
}
