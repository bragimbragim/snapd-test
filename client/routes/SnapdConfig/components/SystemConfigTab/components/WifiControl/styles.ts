import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    successStatus: {
        color: theme.palette.success.main
    },
    errorStatus: {
        color: theme.palette.error.dark
    }
}));
