import React, {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {openAlert} from '../../../../../../state/alert';
import {AlertType} from '../../../../../../state/alert/types/AlertType';
import {Box, Button} from '@material-ui/core';
import {KeyboardDateTimePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import {BackDrop} from '../../../../../../shared/components/BackDrop/component';
import {api} from '../../../../../../application/api';
import {SystemDate} from '../../../../../../../globalTypes/SystemDate';
import {LogoutReason} from '../../../../../../shared/types/LogoutReason';
import {ISystemControlProp} from '../../types/ISystemControlProp';

function parseDateForRequest(date: Date): SystemDate {
    const year = String(date.getFullYear());
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    const hour = String(date.getHours());
    const minutes = ('0' + date.getMinutes()).slice(-2);

    return {
        date: year + month + day,
        time: hour + minutes
    };
}

function removeGMTFromDate(date: string): string {
    return date.replace('GMT', '');
}

export default function SystemTimeControl({onLoading}: ISystemControlProp) {

    const [date, setDate] = useState<Date | null>(null);
    const [error, setError] = useState<boolean>(false);

    const dispatch = useDispatch();

    const getSystemTime = () => {

        onLoading(true);
        api.system.getSystemTime()
            .then((data) => {
                const date = removeGMTFromDate(data);
                setDate(new Date(date))
            })
            .catch((err) => {
                dispatch(openAlert(err, AlertType.ERROR));
                setError(true);
            })
            .finally(() => onLoading(false))
    }

    const setSystemTime = () => {
        const newDate = parseDateForRequest(date);
        api.system.setSystemTime(newDate)
            .then((data) => {
                dispatch(openAlert(data, AlertType.SUCCESS));
                api.auth.logout(LogoutReason.UPDATE_SYSTEM_TIME);
            })
            .catch((err) => {
                dispatch(openAlert(err, AlertType.ERROR));
            })
    }

    useEffect(() => {
        getSystemTime();
    }, []);

    return <>
        <Box display='flex' alignItems="flex-end" m={2}>
            <Box style={{width: '200px'}}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDateTimePicker
                        disabled={error}
                        clearable
                        todayLabel='Today'
                        ampm={false}
                        showTodayButton={true}
                        format="yyyy/MM/dd hh:mm a"
                        value={date}
                        onChange={(date: Date) => setDate(date)}
                    />
                </MuiPickersUtilsProvider>
            </Box>

            <Box ml={2} style={{width: '150px'}}>
                <Button
                    disabled={error}
                    variant='contained'
                    color='primary'
                    onClick={setSystemTime}
                    fullWidth
                >Set Time</Button>
            </Box>
        </Box>
    </>
}
