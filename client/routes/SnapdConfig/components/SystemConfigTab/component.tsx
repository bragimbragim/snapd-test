import React, {useState} from "react";
import SystemTimeControl from './components/SystemTimeControl/component';
import {BackDrop} from '../../../../shared/components/BackDrop/component';
import {Box} from '@material-ui/core';
import {WifiControl} from './components/WifiControl/component';


export function SystemConfigTab() {

    const [loading, setLoading] = useState<boolean>(false);

    const handleLoading = (value) => {
        setLoading(value);
    }

    return <>
        {loading && <BackDrop/>}
        <SystemTimeControl onLoading={handleLoading}/>
        <Box mt={5}>
            <WifiControl/>
        </Box>
    </>
}
