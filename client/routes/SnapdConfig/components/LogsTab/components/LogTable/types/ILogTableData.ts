export interface ILogTableData {
    entity: string;
    event: string;
    name: string;
    time: string;
}
