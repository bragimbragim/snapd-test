import {ILogTableData} from './ILogTableData';

export interface ILogTableProps {
    table: ILogTableData[]
}
