import {api} from '../../../../../../application/api';
import React, {useEffect, useState} from 'react';
import {
    Box,
    Button,
    FormControl,
    MenuItem,
    Paper,
    Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography
} from '@material-ui/core';
import useStyles from '../../styles';
import {ILogTableData} from './types/ILogTableData';
import {getDateWithoutTimezone} from '../../../../../../utils/formatDate'
import {useDispatch} from 'react-redux';
import {AlertType} from '../../../../../../state/alert/types/AlertType';
import {openAlert} from '../../../../../../state/alert';
import {CircularButton} from '../../../../../../shared/components/CircularButton/component';

let interval;


export function LogTable() {

    const [log, setLog] = useState<ILogTableData[]>([]);
    const [limitRows, setLimitRows] = useState<number>(20);
    const [loading, setLoading] = useState<boolean>(false);

    const classes = useStyles();
    const dispatch = useDispatch();

    const getLog = () => {
        clearInterval(interval);
        interval = setInterval(() => {
            api.logs.getLog(limitRows)
                .then((log) => setLog(log))
                .catch((err: string) => clearInterval(interval))
        }, 1000)
    }

    const handleChange = (event: React.ChangeEvent<{ value: number }>) => {
        setLimitRows(event.target.value);
    };

    const handleDeleteLog = () => {
        setLoading(true);
        api.logs.deleteLog()
            .then((data) => dispatch(openAlert(data, AlertType.SUCCESS)))
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => setLoading(false))
    }

    const formatDate = (value: string) => {
        const date = getDateWithoutTimezone(value);
        const year = date.getFullYear();
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = ('0' + date.getDate()).slice(-2);
        const hour = ('0' + date.getHours()).slice(-2);
        const minutes = ('0' + date.getMinutes()).slice(-2);
        const seconds = ('0' + date.getSeconds()).slice(-2);
        const milliseconds = ('00' + date.getMilliseconds()).slice(-3);

        return `${hour}:${minutes}:${seconds}.${milliseconds} ${month}-${day}-${year}`;
    }

    useEffect(() => {
        getLog();
    }, [limitRows, useState]);

    useEffect(() => {
        getLog();

        return () => {
            clearInterval(interval);
        }
    }, [useState]);

    return <>
        <Box className={classes.paperHeader}>
            <Typography variant='h5'>Log</Typography>
            <Box ml={2}>
                <FormControl style={{minWidth: 100}}>
                    <Select value={limitRows} onChange={handleChange}>
                        <MenuItem value={20}>Last 20</MenuItem>
                        <MenuItem value={40}>Last 40</MenuItem>
                        <MenuItem value={60}>Last 60</MenuItem>
                    </Select>
                </FormControl>
            </Box>
            <Box ml={2}>
                <CircularButton
                    loading={loading}
                    disabled={loading}
                    variant='contained'
                    onClick={handleDeleteLog}
                >Delete Log</CircularButton>
            </Box>
        </Box>

        <Paper variant='outlined' className={classes.root}>
            <TableContainer>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Entity</TableCell>
                            <TableCell align="center">Event</TableCell>
                            <TableCell align="center">Tag ID</TableCell>
                            <TableCell align="center">Time</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {log.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell align="center">{row.entity}</TableCell>
                                <TableCell align="center">{row.event}</TableCell>
                                <TableCell align="center">{row.name}</TableCell>
                                <TableCell align="center">{formatDate(row.time)}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>

    </>
}
