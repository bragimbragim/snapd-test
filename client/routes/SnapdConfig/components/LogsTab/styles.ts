import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    paper: {
        backgroundColor: 'white'
    },
    root: {
        height: '65vh',
        overflow: 'auto'
    },
    paperHeader: {
        height: 40,
        display: 'flex',
        alignItems: 'center',
        marginBottom: 10
    },
    activeTagsContainer: {
        minWidth: 200
    }
}));
