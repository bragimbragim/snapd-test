import React from "react";
import {Box, Paper, Typography} from '@material-ui/core';
import useStyles from './styles';
import {ActiveTagsPanel} from '../../../../shared/components/ActiveTagsPanel/component';
import {LogTable} from './components/LogTable/component';

export function LogsTab() {

    const classes = useStyles();

    return <>
        <Box display='flex'>

            <Box flexGrow={1}>
                <LogTable/>
            </Box>

            <Box ml={2} className={classes.activeTagsContainer}>
                <Box className={classes.paperHeader}>
                    <Typography variant='h5'>Active tags</Typography>
                </Box>
                <Paper variant='outlined' className={classes.root}>
                    <ActiveTagsPanel/>
                </Paper>
            </Box>

        </Box>
    </>
}
