import {Box} from '@material-ui/core';
import React from 'react';
import {ITabContentProps} from './types/ITabContentProps';

export default function TabContent({children, value, index, disabled}: ITabContentProps) {

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
        >
            {value === index && !disabled && (<Box p={3}>{children}</Box>)}
        </div>
    );
}
