import {IDefaultComponentProps} from '../../../../../shared/types/IDefaultComponentProps';

export interface ITabContentProps extends IDefaultComponentProps {
    value: number,
    index: number;
    disabled?: boolean;
}
