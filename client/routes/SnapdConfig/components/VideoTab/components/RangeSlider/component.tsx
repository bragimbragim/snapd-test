import React, {useEffect, useState} from "react";
import {useDispatch} from 'react-redux';
import {Box, IconButton, Slider} from '@material-ui/core';
import useStyles from './styles';
import {openAlert} from '../../../../../../state/alert';
import {AlertType} from '../../../../../../state/alert/types/AlertType';
import RefreshIcon from '@material-ui/icons/Refresh';
import {api} from '../../../../../../application/api';

let interval;
let isSubscribed;
const GET_SLIDER_DATA_INTERVAL_MS = 500;

function valuetext(value: number) {
    return `${value}°C`;
}

export function VideoRangeSlider() {

    const [maxValue, setMaxValue] = useState(0);
    const [responseData, setResponseData] = useState(0);

    const dispatch = useDispatch();
    const classes = useStyles();

    const resetMaxHandler = () => {
        setMaxValue(0);
    }

    const getSliderData = () => {
        clearInterval(interval);
        interval = setInterval(() => {
            api.video.getShutterData()
                .then((data) => {
                    if (isSubscribed) {
                        setResponseData(Number((data * 1000).toFixed(0)));
                    }
                })
                .catch((err) => {
                    dispatch(openAlert(err, AlertType.ERROR));
                    clearInterval(interval);
                })
        }, GET_SLIDER_DATA_INTERVAL_MS);
    }

    useEffect(() => {
        if (responseData > maxValue) {
            setMaxValue(responseData);
        }
    }, [responseData]);

    useEffect(() => {
        isSubscribed = true;
        getSliderData();
        return () => {
            isSubscribed = false;
            clearInterval(interval);
        }
    }, []);

    return <>
        <Box pb={2} className={classes.root}>
            <Slider
                color='secondary'
                defaultValue={0}
                value={responseData}
                getAriaValueText={valuetext}
                aria-labelledby="discrete-slider-always"
                step={1}
                max={maxValue}
                valueLabelDisplay="on"
            />
            <Box className={classes.sliderMaxLabel}>{maxValue}</Box>
            <Box ml={1}>
                <IconButton onClick={resetMaxHandler} color='secondary'>
                    <RefreshIcon/>
                </IconButton>
            </Box>
        </Box>
    </>
}
