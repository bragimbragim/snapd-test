import React, {ReactElement, useState} from 'react';
import {Box, IconButton} from '@material-ui/core';
import useStyles from '../../styles';
import clsx from 'clsx';
import AspectRatioIcon from '@material-ui/icons/AspectRatio';
import {VideoRangeSlider} from '../RangeSlider/component';
import {IVideoContainerProps} from './types/IVideoContainerProps';

export default function VideoContainer({url}: IVideoContainerProps): ReactElement {

    const [isExpanded, setIsExpanded] = useState<boolean>(false);

    const classes = useStyles();

    const handleExpand = () => {
        setIsExpanded(!isExpanded);
    }

    return <>
        <Box className={clsx({
            [classes.root]: !isExpanded,
            [classes.rootExpanded]: isExpanded
        })}>
            <VideoRangeSlider/>
            <img className={classes.img} src={url} alt=""/>
            <Box className={classes.videoControls}>
                <IconButton onClick={handleExpand} color='primary'>
                    <AspectRatioIcon/>
                </IconButton>
            </Box>
        </Box>
    </>
}
