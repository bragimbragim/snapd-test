import React, {ReactElement, useEffect, useState} from 'react';
import {Alert} from '@material-ui/lab';
import {Box, Button} from '@material-ui/core';
import VideoContainer from './components/VideoContainer/component';
import {BackDrop} from '../../../../shared/components/BackDrop/component';
import {io, Socket} from 'socket.io-client';
import appConfig from '../../../../config';
import {SocketEvent} from '../../../../../globalTypes/SocketEvent';

const STREAM_URL = "/api/video/stream";
let socket: Socket;

export default function VideoTab(): ReactElement {

    const [open, setOpen] = useState<boolean>(false);
    const [streamURL, setStreamURL] = useState<string | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>(null);

    const connectToSocketIO = () => {
        if (socket?.connected) {
            return;
        }
        socket = io(`${appConfig.API_URL}/video`, {
            transports: ['websocket'],
            reconnection: false
        });

        socket.on(SocketEvent.VIDEO.START, () => {
            setOpen(true);
        });

        socket.on(SocketEvent.VIDEO.ERROR, (err) => {
            setError(err);
            setLoading(false);
        });
    }

    const disconnectFromSocketIO = () => {
        if (socket?.connected) {
            socket.close();
        }
    }

    const startVideoServer = () => {
        setLoading(true);
        connectToSocketIO();
    }

    useEffect(() => {
        setTimeout(() => {
            setStreamURL(open ? STREAM_URL : '');
            setLoading(false);
        }, 3000)
    }, [open])

    useEffect(() => {
        return () => {
            setOpen(false);
            disconnectFromSocketIO();
        }
    }, []);

    return <>
        {loading && <BackDrop/>}
        <Box>
            {
                !open &&
                <Button
                    fullWidth
                    onClick={startVideoServer}
                    variant='contained'
                    color='primary'
                >Open Video</Button>
            }
        </Box>
        {error && <Alert severity='warning'>{error}</Alert>}
        {streamURL && <VideoContainer url={streamURL}/>}
    </>
}
