import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    card: {
        width: 320
    },
    cardHeader: {
        backgroundColor: theme.palette.grey[100]
    },
    successStatus: {
        color: theme.palette.success.main
    },
    errorStatus: {
        color: theme.palette.error.dark
    },
    notInitStatus: {
        color: theme.palette.info.light
    },
    warningStatus: {
        color: theme.palette.warning.light
    }
}));
