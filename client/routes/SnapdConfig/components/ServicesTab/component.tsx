import React, {useEffect} from "react";
import {AmazonServiceCard} from './components/AmazonServiceCard/component';
import MemoryCardService from './components/MemoryServiceCard/components';
import {Box} from '@material-ui/core';
import {useDispatch} from 'react-redux';
import {api} from '../../../../application/api';
import {ISnapdServices} from '../../../../../globalTypes/ISnapdServices';
import {updateAmazonServiceStatusAction} from '../../../../state/services/amazon';
import {updateMemoryServiceStatusAction} from '../../../../state/services/memory';
import {openAlert} from '../../../../state/alert';
import {AlertType} from '../../../../state/alert/types/AlertType';

export function ServicesTab() {

    const dispatch = useDispatch();

    const updateAllServices = () => {
        api.system.getServicesStatus()
            .then((data: ISnapdServices) => {
                dispatch(updateAmazonServiceStatusAction(data.amazon));
                dispatch(updateMemoryServiceStatusAction(data.memory));
            })
            .catch((err) => {
                dispatch(openAlert(err, AlertType.ERROR));
            })
    }

    useEffect(() => {
        updateAllServices();
    }, []);

    return <>
        <Box display='flex' justifyContent='start'>

            <Box m={2}>
                <AmazonServiceCard/>
            </Box>

            <Box m={2}>
                <MemoryCardService/>
            </Box>

        </Box>
    </>
}
