import React from "react";
import {useDispatch, useSelector} from 'react-redux';
import {openModal} from '../../../../../../../../state/modal';
import {ConfirmDialog} from '../../../../../../../../shared/components/ConfirmDialog/component';
import {CircularButton} from '../../../../../../../../shared/components/CircularButton/component';
import {getGalleryLoading} from '../../../../../../../../state/gallery/selectors';
import {deleteAllPhotos} from '../../../../../../../../state/gallery';


export function MemoryDeleteButton() {

    const loading = useSelector(getGalleryLoading);

    const dispatch = useDispatch();

    const handleConfirmAnswer = (value) => {
        if (!value) {
            return;
        }

        dispatch(deleteAllPhotos());
    }

    const openConfirm = () => {
        dispatch(
            openModal(
                <ConfirmDialog
                    title='Do you want to delete photos?'
                    note='Please note that photos will be deleted from the device and from the database'
                    confirmBtnCaption='Delete All'
                    onConfirm={handleConfirmAnswer}
                />
            )
        );
    }

    return <>
        <CircularButton
            loading={loading}
            disabled={loading}
            variant='outlined'
            color='primary'
            onClick={openConfirm}
        >Delete All Photos From Unit</CircularButton>
    </>
}
