import React, {useCallback, useEffect} from "react";
import {Box, CardActions, Typography} from '@material-ui/core';
import {getMemoryData} from '../../../../../../../../state/services/memory/selectors';
import {useDispatch, useSelector} from 'react-redux';
import {IServiceCardBodyProps} from '../../../ServiceCard/types/IServiceCardBodyProps';
import {MemoryDeleteButton} from '../DeleteButton/component';
import {MemoryServiceReloadButton} from '../ReloadButton/component';
import {ServiceStatusCode} from '../../../../../../../../../globalTypes/ServiceStatus';
import {fetchMemoryServiceStatus} from '../../../../../../../../state/services/memory';

function readableKBytes(kBytes) {
    const i = Math.floor(Math.log(kBytes) / Math.log(1024));
    const prefixes = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const size = Number((kBytes / Math.pow(1024, i)).toFixed(2));
    return size + ' ' + prefixes[i];
}

export function MemoryServiceCardBody({status}: IServiceCardBodyProps) {

    const memory = useSelector(getMemoryData);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchMemoryServiceStatus());
    }, [memory.warning]);

    const getMemoryControl = useCallback(() => {
        if (status.code !== ServiceStatusCode.ERROR) {
            return <Box mb={1}><MemoryDeleteButton/></Box>
        }
        if (status.code === ServiceStatusCode.ERROR) {
            return <MemoryServiceReloadButton/>;
        }
    }, [status.code]);

    return <>
        <Typography variant="h5" component="h2">
            Available: {memory.available ? readableKBytes(memory.available) : 0}
        </Typography>
        <Typography color="textSecondary">
            All Size: {memory.size ? readableKBytes(memory.size) : 0}
        </Typography>
        <CardActions>
            <Box display='flex' flexDirection='column' width='100%'>
                {getMemoryControl()}
            </Box>
        </CardActions>
    </>
}
