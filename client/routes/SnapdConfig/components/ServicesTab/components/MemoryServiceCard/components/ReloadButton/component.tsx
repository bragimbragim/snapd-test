import React from "react";
import {CircularButton} from '../../../../../../../../shared/components/CircularButton/component';
import {useDispatch, useSelector} from 'react-redux';
import {getMemoryServiceLoading} from '../../../../../../../../state/services/memory/selectors';
import {reloadMemoryService} from '../../../../../../../../state/services/memory';

export function MemoryServiceReloadButton() {

    const loading = useSelector(getMemoryServiceLoading);

    const dispatch = useDispatch();

    const handleReload = () => {
        dispatch(reloadMemoryService());
    }

    return <>
        <CircularButton
            loading={loading}
            disabled={loading}
            fullWidth
            variant='outlined'
            color='secondary'
            onClick={handleReload}
        >Reload</CircularButton>
    </>
}
