import React, {useCallback} from "react";
import {useSelector} from 'react-redux';
import {ServiceCard} from '../ServiceCard/component';
import {ServiceCardHeader} from '../ServiceCardHeader/component';
import {MemoryServiceCardBody} from './components/CardBody/component';
import {ServiceStatusCode} from '../../../../../../../globalTypes/ServiceStatus';
import {getMemoryServiceStatus} from '../../../../../../state/services/memory/selectors';

export default function MemoryCardService() {

    const memoryServiceStatus = useSelector(getMemoryServiceStatus);

    const getBodyComponent = useCallback(() => {
        if (memoryServiceStatus.code !== ServiceStatusCode.NOT_INITIALIZED) {
            return <MemoryServiceCardBody/>;
        }
    }, [memoryServiceStatus.code]);

    return <>
        <ServiceCard
            status={memoryServiceStatus}
            header={<ServiceCardHeader title='Memory Service'/>}
            body={getBodyComponent()}
        />
    </>
}
