import {ReactElement} from 'react';
import {IServiceStatus} from '../../../../../../../../globalTypes/ServiceStatus';

export interface IServiceCardProps {
    status: IServiceStatus;
    header: ReactElement;
    body: ReactElement;
}
