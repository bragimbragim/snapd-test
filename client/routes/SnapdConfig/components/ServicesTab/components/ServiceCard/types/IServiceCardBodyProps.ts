import {IServiceStatus} from '../../../../../../../../globalTypes/ServiceStatus';

export interface IServiceCardBodyProps {
    status?: IServiceStatus;
}
