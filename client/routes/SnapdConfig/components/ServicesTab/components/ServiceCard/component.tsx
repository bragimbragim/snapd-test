import React from "react";
import {Box, Card, CardContent, CircularProgress} from '@material-ui/core';
import useStyles from '../../styles';
import {IServiceCardProps} from './types/IServiceCardProps';

export function ServiceCard({status, header, body}: IServiceCardProps) {

    const classes = useStyles();

    const headerComponent = () => {
        return React.cloneElement(header, {status});
    }

    const bodyComponent = () => {
        return React.cloneElement(body, {status});
    }
    return <>
        <Card className={classes.card}>
            {
                status === null
                ? <Box
                    p={5}
                    display='flex'
                    justifyContent='center'
                    alignItems='center'
                ><CircularProgress/></Box>
                : <Box>
                    {headerComponent()}
                    {
                        body && <CardContent>{bodyComponent()}</CardContent>
                    }
                </Box>
            }

        </Card>

    </>
}
