import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    circular: {
        position: 'absolute',
        width: '90%',
        display: 'flex',
        justifyContent: 'center',
        zIndex: 999,
        backgroundColor: '#ffffff7a'
    }
}));
