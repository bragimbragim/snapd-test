import {AmazonStatusCode} from '../../../../../../../../../../../src/types/AmazonStatus';

export interface IQueueListItem {
    photoName: string;
    photoDate: string;
    code: AmazonStatusCode;
    caption?: string;
}
