import React, {ChangeEvent, useEffect, useState} from "react";
import {
    Box, CircularProgress,
    FormControlLabel,
    Switch
} from '@material-ui/core';
import {Controller, useForm} from 'react-hook-form';
import {DefaultConfig} from '../../../../../../../../../src/configs/AmazonConfig';
import {api} from '../../../../../../../../application/api';
import {useDispatch} from 'react-redux';
import {openAlert} from '../../../../../../../../state/alert';
import {AlertType} from '../../../../../../../../state/alert/types/AlertType';
import {IAmazonConfig} from '../../../../../../../../../globalTypes/IAmazonConfig';
import useStyles from './styles';
import {ConfirmDialog} from '../../../../../../../../shared/components/ConfirmDialog/component';
import {reloadAmazonService} from '../../../../../../../../state/services/amazon';
import {openModal} from '../../../../../../../../state/modal';

export function AmazonServiceConfigForm() {

    const {
        reset,
        handleSubmit,
        control,
        watch
    } = useForm<IAmazonConfig>({
        mode: 'onBlur',
        defaultValues: DefaultConfig
    });

    const [loading, setLoading] = useState<boolean>(false);

    const dispatch = useDispatch();
    const classes = useStyles();
    const loadPhotosToAmazon = watch('loadPhotosToAmazon');

    const getAmazonOptions = () => {
        setLoading(true);
        api.amazon.getOptions()
            .then((options: IAmazonConfig) => reset(options))
            .catch((err: string) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => setLoading(false))
    }

    const onSubmit = (data: IAmazonConfig) => {
        setLoading(true);
        api.amazon.setOptions(data)
            .then((data: string) => dispatch(openAlert(data, AlertType.SUCCESS)))
            .catch((err: string) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => setLoading(false))
    }

    const openConfirmModal = (): Promise<boolean> => {
        return new Promise((resolve, reject) => {
            dispatch(openModal(
                <ConfirmDialog
                    title='Do you really want to change the Amazon Location'
                    confirmBtnCaption='Reload Amazon Service'
                    onConfirm={(value) => {
                        if (!value) {
                            return reject('Not Confirmed');
                        }
                        return resolve(true);
                    }}
                />
            ));
        });
    }

    useEffect(() => {
        getAmazonOptions();
    }, []);

    return <>
        <div>
            <form>
                {
                    loading &&
                    <Box className={classes.circular}><CircularProgress/></Box>
                }
                <FormControlLabel
                    control={
                        <Controller
                            name="loadPhotosToAmazon"
                            control={control}
                            render={({value, onChange}) => (
                                <Switch
                                    checked={value}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                        onChange(e.target.checked);
                                        handleSubmit(onSubmit)()
                                    }}
                                />
                            )}
                        />
                    }
                    label="Load photos to Amazon"
                />

                <FormControlLabel
                    control={
                        <Controller
                            name="notSavePhotosInUnit"
                            control={control}
                            defaultValue={false}
                            render={({value, onChange}) => (
                                <Switch
                                    checked={value}
                                    disabled={false}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                        const checked = e.target.checked;
                                        openConfirmModal()
                                            .then(() => {
                                                onChange(checked);
                                                handleSubmit(onSubmit)()
                                                    .then(() => dispatch(reloadAmazonService()));
                                            });
                                    }}
                                />
                            )}
                        />
                    }
                    label="Remove after uploading"
                />

                <FormControlLabel
                    control={
                        <Controller
                            name="debugLocation"
                            control={control}
                            defaultValue={false}
                            render={({value, onChange}) => (
                                <Switch
                                    checked={value}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                        const checked = e.target.checked;
                                        openConfirmModal()
                                            .then(() => {
                                                onChange(checked);
                                                handleSubmit(onSubmit)()
                                                    .then(() => dispatch(reloadAmazonService()));
                                            });
                                    }}
                                />
                            )}
                        />
                    }
                    label="Debug mode"
                />
            </form>
        </div>
    </>
}
