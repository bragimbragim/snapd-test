import React, {useState} from "react";
import {Badge, Box, CircularProgress, IconButton, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {getAmazonQueueErrors, getAmazonQueueCount} from '../../../../../../../../state/services/amazon/selectors';
import {api} from '../../../../../../../../application/api';
import {openAlert} from '../../../../../../../../state/alert';
import {AlertType} from '../../../../../../../../state/alert/types/AlertType';
import CancelIcon from '@material-ui/icons/Cancel';
import ListAltIcon from '@material-ui/icons/ListAlt';
import useStyles from './styles';
import {QueueList} from './components/QueueList/component';
import {openModal} from '../../../../../../../../state/modal';


export function AmazonServiceQueue() {

    const queueCount = useSelector(getAmazonQueueCount);
    const queueErrors = useSelector(getAmazonQueueErrors);

    const [loading, setLoading] = useState<boolean>(false);

    const dispatch = useDispatch();
    const classes = useStyles();

    const openQueueList = () => {
        dispatch(openModal(<QueueList/>));
    }

    const clearQueue = () => {
        setLoading(true);
        api.amazon.clearQueue()
            .then((data: string) => dispatch(openAlert(data, AlertType.SUCCESS)))
            .catch((err: string) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => setLoading(false))
    }

    return <>
        <Box display='flex' alignItems='center'>
            <Box mr={1}>
                <Typography>Loading Queue: {queueCount} photos</Typography>
            </Box>

            <IconButton onClick={openQueueList}>
                <Badge badgeContent={queueErrors} classes={{badge: classes.badge}}>
                    <ListAltIcon color='primary'/>
                </Badge>
            </IconButton>
            <IconButton onClick={clearQueue} disabled={loading}>
                <CancelIcon color='secondary'/>
                {loading && <CircularProgress size={24} className={classes.circular}/>}
            </IconButton>
        </Box>
    </>
}
