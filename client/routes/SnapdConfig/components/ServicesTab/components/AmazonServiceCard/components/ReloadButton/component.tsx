import React from "react";
import {CircularButton} from '../../../../../../../../shared/components/CircularButton/component';
import {useDispatch, useSelector} from 'react-redux';
import {getAmazonServiceLoading} from '../../../../../../../../state/services/amazon/selectors';
import {reloadAmazonService} from '../../../../../../../../state/services/amazon';

export function AmazonServiceReloadButton() {

    const loading = useSelector(getAmazonServiceLoading);

    const dispatch = useDispatch();

    const handleReload = () => {
        dispatch(reloadAmazonService());
    }

    return <>
        <CircularButton
            loading={loading}
            disabled={loading}
            fullWidth
            variant='outlined'
            color='secondary'
            onClick={handleReload}
        >Reload</CircularButton>
    </>
}
