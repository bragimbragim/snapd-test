import React from "react";
import {Box, List, ListSubheader} from '@material-ui/core';
import useStyles from '../styles';
import {QueueListItem} from '../QueueListItem/component';
import {useSelector} from 'react-redux';
import {getAmazonQueueList} from '../../../../../../../../../../state/services/amazon/selectors';

export function QueueList() {

    const queueList = useSelector(getAmazonQueueList);

    const classes = useStyles();

    const getSubheader = () => {
        return <ListSubheader color='primary'>Amazon Queue</ListSubheader>
    }

    const getListItems = () => {
        const queue = [...queueList];

        return queue
            .reverse()
            .map((item, index) => {
                return <QueueListItem
                    key={index}
                    photoName={item.photoName}
                    photoDate={item.photoDate}
                    code={item.code}
                    caption={item.caption}
                />
            })
    }

    return <>
        <Box p={2}>
            <List
                className={classes.listContainer}
                subheader={getSubheader()}
            >
                {getListItems()}
            </List>
        </Box>
    </>
}
