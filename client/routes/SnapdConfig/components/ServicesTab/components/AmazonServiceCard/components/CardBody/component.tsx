import React from "react";
import {Box, CardActions, Divider} from '@material-ui/core';
import {IServiceCardBodyProps} from '../../../ServiceCard/types/IServiceCardBodyProps';
import {AmazonServiceReloadButton} from '../ReloadButton/component';
import {AmazonServiceConfigForm} from '../Form/component';
import {AmazonServiceQueue} from '../Queue/component';
import {ServiceStatusCode} from '../../../../../../../../../globalTypes/ServiceStatus';


export function AmazonServiceCardBody({status}: IServiceCardBodyProps) {

    return <>
        {
            status.code !== ServiceStatusCode.NOT_INITIALIZED &&
            <Box>
                <AmazonServiceQueue/>
                <Box mt={2} mb={2}><Divider/></Box>
            </Box>
        }
        <AmazonServiceConfigForm/>
        <CardActions>
            {status.code === ServiceStatusCode.ERROR && <AmazonServiceReloadButton/>}
        </CardActions>
    </>
}
