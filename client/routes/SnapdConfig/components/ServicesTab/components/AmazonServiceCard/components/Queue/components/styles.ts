import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    listContainer: {
        minWidth: 400,
        backgroundColor: theme.palette.background.paper
    },
    successIcon: {
        color: theme.palette.success.dark
    },
    errorIcon: {
        color: theme.palette.error.dark
    },
    publishIcon: {
        color: theme.palette.info.dark
    },
    waitingIcon: {
        color: '#b0b0b0'
    }
}));
