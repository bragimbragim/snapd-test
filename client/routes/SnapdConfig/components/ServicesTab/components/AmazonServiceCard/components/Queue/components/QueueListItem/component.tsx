import React from "react";
import {ListItem, ListItemIcon, ListItemText, Box} from '@material-ui/core';
import ErrorIcon from '@material-ui/icons/Error';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import PublishIcon from '@material-ui/icons/Publish';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import useStyles from '../styles'
import {IQueueListItem} from '../types/IQueueListItem';
import {AmazonStatusCode} from '../../../../../../../../../../../src/types/AmazonStatus';

export function QueueListItem({photoName, photoDate, caption, code}: IQueueListItem) {

    const classes = useStyles();

    const getIcon = () => {
        switch (code) {
            case AmazonStatusCode.ERROR:
                return <ErrorIcon className={classes.errorIcon}/>
            case AmazonStatusCode.SUCCESS:
                return <CheckCircleIcon className={classes.successIcon}/>
            case AmazonStatusCode.UPLOADING:
                return <PublishIcon className={classes.publishIcon}/>
            case AmazonStatusCode.WAITING_FOR_UPLOAD:
                return <HourglassEmptyIcon className={classes.waitingIcon}/>
        }
    }

    return <>
        <ListItem>
            <ListItemIcon>{getIcon()}</ListItemIcon>
            <ListItemText
                primary={photoName}
                secondary={
                    <React.Fragment>
                        {photoDate}
                        <br/>
                        {caption}
                    </React.Fragment>
                }
            />
        </ListItem>
    </>
}
