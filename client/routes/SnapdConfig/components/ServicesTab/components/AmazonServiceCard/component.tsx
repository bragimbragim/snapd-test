import React, {useEffect} from "react";
import {ServiceCardHeader} from '../ServiceCardHeader/component';
import {useDispatch, useSelector} from 'react-redux';
import {ServiceCard} from '../ServiceCard/component';
import {AmazonServiceCardBody} from './components/CardBody/component';
import {
    updateAmazonQueueCountAction,
    updateAmazonQueueListAction, updateAmazonServiceStatusAction
} from '../../../../../../state/services/amazon';
import {api} from '../../../../../../application/api';
import {IAmazonServiceData} from '../../../../../../../globalTypes/IAmazonServiceData';
import {openAlert} from '../../../../../../state/alert';
import {AlertType} from '../../../../../../state/alert/types/AlertType';
import {getAmazonServiceStatus} from '../../../../../../state/services/amazon/selectors';

let interval;

export function AmazonServiceCard() {

    const amazonServiceStatus = useSelector(getAmazonServiceStatus);

    const dispatch = useDispatch();

    const getData = () => {
        clearInterval(interval);
        interval = setInterval(() => {
            api.amazon.getData()
                .then((data: IAmazonServiceData) => {
                    dispatch(updateAmazonQueueCountAction(data.queueCount));
                    dispatch(updateAmazonQueueListAction(data.queueList));
                    dispatch(updateAmazonServiceStatusAction(data.serviceStatus));
                })
                .catch((err: string) => {
                    dispatch(openAlert(err, AlertType.ERROR));
                    clearInterval(interval);
                })
        }, 1000);
    }

    useEffect(() => {
        getData();

        return () => {
            clearInterval(interval);
        }
    }, []);

    return <>
        {
            <ServiceCard
                status={amazonServiceStatus}
                header={<ServiceCardHeader title='Amazon Service'/>}
                body={<AmazonServiceCardBody/>}
            />
        }
    </>
}
