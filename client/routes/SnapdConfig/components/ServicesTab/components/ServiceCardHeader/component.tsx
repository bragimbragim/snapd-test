import React from "react";
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import clsx from 'clsx';
import {CardHeader} from '@material-ui/core';
import useStyles from '../../styles';
import {IServiceCardHeaderProps} from './types/IServiceCardHeaderProps';
import {ServiceStatusCode} from '../../../../../../../globalTypes/ServiceStatus';

export function ServiceCardHeader({title, status}: IServiceCardHeaderProps) {

    const classes = useStyles();

    return <>
        <CardHeader
            className={classes.cardHeader}
            title={title}
            subheader={status.caption || ''}
            avatar={
                <FiberManualRecordIcon
                    className={clsx({
                        [classes.successStatus]: status.code === ServiceStatusCode.SUCCESS,
                        [classes.errorStatus]: status.code === ServiceStatusCode.ERROR,
                        [classes.notInitStatus]: status.code === ServiceStatusCode.NOT_INITIALIZED,
                        [classes.warningStatus]: status.code === ServiceStatusCode.WARNING
                    })}
                />
            }
        />
    </>
}
