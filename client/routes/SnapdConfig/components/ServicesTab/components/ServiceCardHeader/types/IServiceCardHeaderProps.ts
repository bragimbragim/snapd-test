import {IServiceStatus} from '../../../../../../../../globalTypes/ServiceStatus';

export interface IServiceCardHeaderProps {
    title: string;
    status?: IServiceStatus;
}
