import React, {useEffect, useState} from "react";
import {Box, Button} from '@material-ui/core';
import {IGallery} from '../../../../../../shared/types/IGallery';
import {useDispatch} from 'react-redux';
import {openAlert} from '../../../../../../state/alert';
import {AlertType} from '../../../../../../state/alert/types/AlertType';
import Gallery from 'react-grid-gallery'
import config from '../../../../../../config';
import {IPhotoConfigChildProps} from '../../types/IPhotoConfigChildProps';
import {api} from '../../../../../../application/api';

export default function CheckPhotoGallery({changeLoading}: IPhotoConfigChildProps) {

    const [photos, setPhotos] = useState<IGallery[]>([]);

    const dispatch = useDispatch();

    const takeImage = () => {
        changeLoading(true);
        api.camera.checkCamera()
            .then((data) => {
                setPhotos([
                    ...photos,
                    {
                        src: config.API_URL + data.path,
                        thumbnail: config.API_URL + data.path,
                        thumbnailWidth: 600,
                        thumbnailHeight: 400
                    }
                ]);
                dispatch(
                    openAlert(
                        `Photo is taken in ${data.time}`,
                        AlertType.SUCCESS
                    )
                );
            })
            .catch((err: string) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => changeLoading(false))
    }

    const clearTestPhotosDir = () => {
        api.gallery.deleteTestPhotos()
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
    }

    useEffect(() => {
        return clearTestPhotosDir;
    }, []);

    return <>
        <Box mt={2} mb={2}>
            <Button
                variant='contained'
                color='secondary'
                onClick={takeImage}
                fullWidth
            >Take Photo</Button>
        </Box>
        <Gallery
            images={photos}
            enableImageSelection={false}
            margin={4}
            backdropClosesModal={true}
            preloadNextImage={false}
        />
    </>
}
