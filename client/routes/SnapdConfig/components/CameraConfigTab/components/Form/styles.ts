import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    formControl: {
        width: '30%',
        margin: '0 10px 10px 10px',
        position: 'relative'
    },
    select: {
        width: '100%'
    },
    selectWithIcon: {
        marginRight: '30px'
    },
    infoIcon: {
        cursor: 'pointer',
        position: 'absolute',
        right: 0,
        bottom: 0
    }
}));
