import React, {ChangeEvent, useEffect} from "react";
import {
    Box,
    Button, Checkbox,
    FormControlLabel,
    MenuItem,
    Switch,
    TextField
} from '@material-ui/core';
import {Controller, useForm} from 'react-hook-form';
import useStyles from './styles';
import {useDispatch} from 'react-redux';
import {DefaultConfig} from '../../../../../../../src/configs/CameraConfig';
import {ICameraConfig} from '../../../../../../../globalTypes/ICameraConfig';
import {AlertType} from '../../../../../../state/alert/types/AlertType';
import {openAlert} from '../../../../../../state/alert';
import {isDefined} from 'class-validator';
import ReactHookFormSelect from '../../../../../../shared/components/ReactHookFormSelect/component';
import {ExposureModeArray} from '../../../../../../../globalTypes/ExposureMode';
import CustomTooltip from '../../../../../../shared/components/Tooltip/component';
import {SensorModeArray} from '../../../../../../../globalTypes/SensorMode';
import InfoIcon from '@material-ui/icons/Info';
import {CAMERA as CameraOptionsDefinition} from '../../../../../../application/OptionsDefinitions';
import {ICameraConfigForm} from './types/ICameraConfigForm';
import {IPhotoConfigChildProps} from '../../types/IPhotoConfigChildProps';
import {FlickerMode} from '../../../../../../../globalTypes/FlickerMode';
import {api} from '../../../../../../application/api';

const sensorModeCaption = [
    'automatic selection',
    '2028x1080, 169:90',
    '2028x1520, 4:3',
    '4056x3040, 4:3',
    '1012x760, 4:3'
];

export default function CameraConfigForm({changeLoading}: IPhotoConfigChildProps) {

    const {
        register,
        reset,
        handleSubmit,
        errors,
        watch,
        control
    } = useForm<ICameraConfigForm>({
        mode: 'onBlur',
        defaultValues: DefaultConfig
    });

    const classes = useStyles();
    const dispatch = useDispatch();
    const watchLimitCameraMode = watch('limitCameraMode');

    const getCameraOptions = () => {
        changeLoading(true);
        api.camera.getCameraOptions()
            .then((data: ICameraConfig) => reset(data))
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => changeLoading(false))
    }

    const onSubmit = (data: ICameraConfigForm) => {
        changeLoading(true);
        data = mapRequest(data);
        api.camera.setCameraOptions(data)
            .then((data: string) => dispatch(openAlert(data, AlertType.SUCCESS)))
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
            .finally(() => changeLoading(false))
    }

    const mapRequest = (data: ICameraConfigForm): ICameraConfigForm => {
        data.shutter = (!data.shutter) ? null : data.shutter;
        data.ISO = (!data.ISO) ? null : data.ISO;
        data.flicker = (data.flicker === 'default') ? null : data.flicker;
        data.exposure = (data.exposure === 'default') ? null : data.exposure;
        data.mode = null;

        console.log(data);

        return data;
    }

    useEffect(() => {
        getCameraOptions();
    }, [reset]);

    return <>
        <form onSubmit={handleSubmit(onSubmit)}>

            <Box display='flex' flexDirection='row' flexWrap='wrap'>

                <Box display='flex' flexDirection='row' className={classes.formControl}>
                    <ReactHookFormSelect
                        className={classes.select}
                        id="exposure"
                        name="exposure"
                        label="Exposure"
                        control={control}
                        defaultValue='default'
                    >
                        {
                            ExposureModeArray.map((mode) => {
                                return <MenuItem key={mode} value={mode}>{mode}</MenuItem>
                            })
                        }
                    </ReactHookFormSelect>
                </Box>

                <Box display='flex' flexDirection='row' className={classes.formControl}>
                    <ReactHookFormSelect
                        disabled={true}
                        className={[classes.select, classes.selectWithIcon].join(' ')}
                        id="mode"
                        name="mode"
                        label="SensorMode"
                        control={control}
                        defaultValue='3'
                    >
                        {
                            SensorModeArray.map((mode) => {
                                return <MenuItem key={mode} value={mode}>{sensorModeCaption[mode]}</MenuItem>
                            })
                        }
                    </ReactHookFormSelect>
                    <CustomTooltip definition={CameraOptionsDefinition.MODE}>
                        <InfoIcon className={classes.infoIcon}/>
                    </CustomTooltip>
                </Box>

                <Box display='flex' flexDirection='row' className={classes.formControl}>
                    <ReactHookFormSelect
                        disabled={!watchLimitCameraMode}
                        className={classes.select}
                        id="photoCount"
                        name="photoCount"
                        label="Count of photos for tag"
                        control={control}
                        defaultValue='1'
                    >
                        {
                            Array.from({length: 10}).map((item, index) => {
                                return <MenuItem key={index + 1} value={index + 1}>{index + 1}</MenuItem>
                            })
                        }
                    </ReactHookFormSelect>
                </Box>

                <CustomTooltip definition={CameraOptionsDefinition.ISO}>
                    <TextField
                        InputLabelProps={{shrink: true}}
                        className={classes.formControl}
                        error={isDefined(errors.ISO)}
                        name='ISO'
                        type='number'
                        inputRef={register({
                            min: {value: 100, message: 'Should be >= 100 and <= 800'},
                            max: {value: 800, message: 'Should be >= 100 and <= 800'}
                        })}
                        label='ISO'
                        helperText={errors.ISO && errors.ISO.message}
                    />
                </CustomTooltip>

                <Box display='flex' flexDirection='row' className={classes.formControl}>
                    <ReactHookFormSelect
                        className={classes.select}
                        id="flicker"
                        name="flicker"
                        label="Flicker"
                        control={control}
                        defaultValue='default'
                    >
                        {
                            FlickerMode.map((mode) => {
                                return <MenuItem key={mode} value={mode}>{mode}</MenuItem>
                            })
                        }
                    </ReactHookFormSelect>
                </Box>

                <CustomTooltip definition={CameraOptionsDefinition.SHUTTER}>
                    <TextField
                        InputLabelProps={{shrink: true}}
                        className={classes.formControl}
                        error={isDefined(errors.shutter)}
                        name='firstPhotoDelayMs'
                        type='number'
                        inputRef={register({
                            min: {value: 0, message: 'Should be >= 0 and <= 10000'},
                            max: {value: 10000, message: 'Should be >= 0 and <= 10000'}
                        })}
                        label='First photo delay ms'
                        helperText={errors.firstPhotoDelayMs && errors.firstPhotoDelayMs.message}
                    />
                </CustomTooltip>

                <CustomTooltip definition={CameraOptionsDefinition.SHUTTER}>
                    <TextField
                        InputLabelProps={{shrink: true}}
                        className={classes.formControl}
                        error={isDefined(errors.shutter)}
                        name='shutter'
                        type='number'
                        inputRef={register({
                            min: {value: 600, message: 'Should be >= 600 and <= 200000000'},
                            max: {value: 200000000, message: 'Should be >= 600 and <= 200000000'}
                        })}
                        label='Shutter'
                        helperText={errors.shutter && errors.shutter.message}
                    />
                </CustomTooltip>

                <Box display='flex' flexDirection='row' className={classes.formControl}>
                    <FormControlLabel
                        label="Don't save photos"
                        control={
                            <Controller
                                name="debugMode"
                                control={control}
                                render={({value, onChange}) => (
                                    <Switch
                                        checked={value}
                                        onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                            onChange(e.target.checked)
                                        }}
                                    />
                                )}
                            />
                        }
                    />
                </Box>

                <Box display='flex' flexDirection='row' className={classes.formControl}>
                    <FormControlLabel
                        label="Limit Photoshooting"
                        control={
                            <Controller
                                name='limitCameraMode'
                                control={control}
                                render={({value, onChange}) => (
                                    <Checkbox
                                        checked={value}
                                        onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                            onChange(e.target.checked)
                                        }}
                                    />
                                )}
                            />
                        }
                    />
                </Box>

            </Box>

            <Box mt={2}>
                <Button
                    type='submit'
                    variant='contained'
                    color='primary'
                >Save config</Button>
            </Box>

        </form>
    </>
}
