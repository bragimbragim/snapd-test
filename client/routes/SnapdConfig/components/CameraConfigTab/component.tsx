import React, {useState} from "react";
import {Box, Divider} from '@material-ui/core';
import CameraConfigForm from './components/Form/component';
import CheckPhotoGallery from './components/CheckPhotoGallery/component';
import {BackDrop} from '../../../../shared/components/BackDrop/component';

export default function CameraConfigTab() {

    const [loading, setLoading] = useState<boolean>(false);

    const handleChange = (newValue: boolean) => {
        setLoading(newValue);
    }

    return <>
        {loading ? <BackDrop/> : null}
        <CameraConfigForm changeLoading={handleChange}/>
        <Box mt={3} mb={2}><Divider/></Box>
        <CheckPhotoGallery changeLoading={handleChange}/>
    </>
}
