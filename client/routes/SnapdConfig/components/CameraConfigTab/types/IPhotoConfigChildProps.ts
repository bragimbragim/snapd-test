import {IDefaultComponentProps} from '../../../../../shared/types/IDefaultComponentProps';

export interface IPhotoConfigChildProps extends IDefaultComponentProps {
    changeLoading: (newValue: boolean) => void;
}
