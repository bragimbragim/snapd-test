import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
        width: '100%'
    },
    appBar: {
        backgroundColor: theme.palette.background.paper
    }
}));
