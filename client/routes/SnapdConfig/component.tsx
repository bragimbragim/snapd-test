import React, {ChangeEvent, ReactElement, useEffect, useMemo, useState} from 'react';
import {AppBar, Tab, Tabs} from '@material-ui/core';
import useStyles from './styles';
import TabContent from './components/TabContent/component';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import RssFeedIcon from '@material-ui/icons/RssFeed';
import CameraConfigTab from './components/CameraConfigTab/component';
import ReaderConfigTab from './components/ReaderConfigTab/component';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideoTab from './components/VideoTab/component';
import BuildIcon from '@material-ui/icons/Build';
import SubjectIcon from '@material-ui/icons/Subject';
import {LogsTab} from './components/LogsTab/component';
import {useSelector} from 'react-redux';
import {getReaderStatus} from '../../state/reader/selectors';
import {ReaderStatusCode} from '../../../globalTypes/ReaderStatus';
import TimelapseIcon from '@material-ui/icons/Timelapse';
import TimelapseTab from './components/TimelapseTab/component';
import {ServicesTab} from './components/ServicesTab/component';
import BallotIcon from '@material-ui/icons/Ballot';
import {SystemConfigTab} from './components/SystemConfigTab/component';

export default function SnapdConfig(): ReactElement {

    const classes = useStyles();
    const [value, setValue] = useState<number>(null);
    const readerStatus = useSelector(getReaderStatus);

    const handleChange = (event: ChangeEvent<{}>, newValue: number) => {
        localStorage.setItem('activeConfigTab', newValue.toString());
        setValue(newValue);
    };

    const readerIsActive = useMemo(() => {
        return [ReaderStatusCode.ACTIVE].includes(readerStatus.code)
    }, [readerStatus]);

    const readerNotInitialized = useMemo(() => {
        return readerStatus.code === ReaderStatusCode.NOT_INITIALIZED;
    }, [readerStatus]);

    useEffect(() => {
        const activeConfigTab = localStorage.getItem('activeConfigTab');
        setValue(Number(activeConfigTab || 0));
    }, []);

    return <>
        {
            value !== null && <div className={classes.root}>
                <AppBar position="static" color="default" elevation={1} className={classes.appBar}>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="scrollable"
                        scrollButtons="on"
                        indicatorColor="primary"
                        textColor="primary"
                    >
                        <Tab label="Camera" icon={<PhotoCameraIcon/>} disabled={readerIsActive}/>
                        <Tab label='Video' icon={<VideocamIcon/>} disabled={readerIsActive}/>
                        <Tab label="Reader" icon={<RssFeedIcon/>} disabled={readerIsActive}/>
                        <Tab label="System" icon={<BuildIcon/>} disabled={readerIsActive}/>
                        <Tab label="Services Tab" icon={<BallotIcon/>}/>
                        <Tab label="Calibration" icon={<TimelapseIcon/>} disabled={readerIsActive || readerNotInitialized}/>
                        <Tab label="Log" icon={<SubjectIcon/>}/>
                    </Tabs>
                </AppBar>

                <TabContent value={value} index={0} disabled={readerIsActive}>
                    <CameraConfigTab/>
                </TabContent>

                <TabContent value={value} index={1} disabled={readerIsActive}>
                    <VideoTab/>
                </TabContent>

                <TabContent value={value} index={2} disabled={readerIsActive}>
                    <ReaderConfigTab/>
                </TabContent>

                <TabContent value={value} index={3} disabled={readerIsActive}>
                    <SystemConfigTab/>
                </TabContent>

                <TabContent value={value} index={4}>
                    <ServicesTab/>
                </TabContent>

                <TabContent value={value} index={5}>
                    <TimelapseTab/>
                </TabContent>

                <TabContent value={value} index={6}>
                    <LogsTab/>
                </TabContent>

            </div>
        }

    </>
}
