import React, {ReactElement} from 'react';
import {Typography} from '@material-ui/core';

export default function ManageTags(): ReactElement {

    return <>
        <Typography align='center' variant='h1'>MANAGE TAGS</Typography>
    </>
}
