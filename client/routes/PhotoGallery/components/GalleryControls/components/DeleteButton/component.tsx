import React from "react";
import {useDispatch, useSelector} from 'react-redux';
import {getGallery, getSelectedPhotos} from '../../../../../../state/gallery/selectors';
import {deleteAllPhotos, deleteSelectedPhotos} from '../../../../../../state/gallery';
import {Box, Button} from '@material-ui/core';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import {IGalleryControlsBtnProps} from '../../types/IGalleryControlsBtnProps';
import {openModal} from '../../../../../../state/modal';
import {ConfirmDialog} from '../../../../../../shared/components/ConfirmDialog/component';

export function GalleryControllerDeleteBtn({...rest}: IGalleryControlsBtnProps) {

    const gallery = useSelector(getGallery);
    const selectedPhotos = useSelector(getSelectedPhotos);
    const dispatch = useDispatch();

    const deletePhotos = () => {
        if (gallery.isSelectedAll) {
            dispatch(deleteAllPhotos());
            return;
        }
        dispatch(deleteSelectedPhotos(selectedPhotos));
    }

    const handleConfirmAnswer = (value: boolean) => {
        if (!value) {
            return;
        }

        deletePhotos();
    }

    const openConfirm = () => {
        dispatch(
            openModal(
                <ConfirmDialog
                    title='Do you want to delete photos?'
                    note='Please note that photos will be deleted from the device and from the database'
                    confirmBtnCaption='Delete All'
                    onConfirm={handleConfirmAnswer}
                />)
        );
    }

    return <>
        <Button onClick={openConfirm} {...rest}>
            <Box display='flex' mr={1}><DeleteForeverIcon/></Box>
            Delete
        </Button>
    </>
}
