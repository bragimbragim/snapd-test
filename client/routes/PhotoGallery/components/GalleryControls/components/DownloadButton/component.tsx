import React from "react";
import {Box, Button} from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import {downloadPhotos} from '../../../../../../state/gallery';
import {useDispatch, useSelector} from 'react-redux';
import {IGalleryControlsBtnProps} from '../../types/IGalleryControlsBtnProps';
import {getSelectedPhotos} from '../../../../../../state/gallery/selectors';

function saveAs(file, name) {
    const url = window.URL.createObjectURL(new Blob([file], {type: 'application/zip'}));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', name);
    document.body.appendChild(link);
    link.click();
    link.remove();
}

export function GalleryControllerDownloadBtn({...rest}: IGalleryControlsBtnProps) {

    const selectedPhotos = useSelector(getSelectedPhotos);
    const dispatch = useDispatch();

    const download = () => {
        const promise = (dispatch(downloadPhotos(selectedPhotos)) as unknown) as Promise<string>;
        promise.then((file) => saveAs(file, 'photos.zip'))
    }

    return <>
        <Button
            onClick={download}
            {...rest}
        >
            <Box display='flex' mr={1}><GetAppIcon/></Box>
            Download
        </Button>
    </>
}
