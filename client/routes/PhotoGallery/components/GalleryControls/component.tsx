import React, {useMemo} from "react";
import {Box, ButtonGroup, Checkbox, FormControlLabel} from '@material-ui/core';
import {UserRole} from '../../../../shared/types/UserRole';
import {IGalleryControlsProps} from './types/IGalleryControlsProps';
import {userStorage} from '../../../../application/UserStorage';
import {selectAllPhotos} from '../../../../state/gallery';
import {useDispatch, useSelector} from 'react-redux';
import {getGallery, getSelectedPhotos} from '../../../../state/gallery/selectors';
import {GalleryControllerDeleteBtn as DeletePhotoBtn} from './components/DeleteButton/component';
import {GalleryControllerDownloadBtn as DownloadPhotoBtn} from './components/DownloadButton/component';
import useStyles from '../../styles';


export function GalleryControls({}: IGalleryControlsProps) {

    const gallery = useSelector(getGallery);
    const selectedPhotos = useSelector(getSelectedPhotos);
    const dispatch = useDispatch();
    const classes = useStyles();

    const onSelectAll = () => {
        dispatch(selectAllPhotos(!gallery.isSelectedAll));
    }

    const hasSelectedPhotos = useMemo(() => {
        return selectedPhotos.length > 0
    }, [selectedPhotos]);

    return <>
        <Box className={classes.galleryControlsContainer}>
            <Box ml={2}>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={gallery.isSelectedAll}
                            onChange={onSelectAll}
                            color="primary"
                        />
                    }
                    label="Select All"
                />
            </Box>

            <ButtonGroup color="primary">
                <DownloadPhotoBtn
                    className={classes.controlBtn}
                    color='primary'
                    variant='contained'
                    disabled={!hasSelectedPhotos}
                />
                {
                    userStorage.getRole() === UserRole.ADMIN &&
                    <DeletePhotoBtn
                        className={classes.controlBtn}
                        color='secondary'
                        variant='contained'
                        disabled={!hasSelectedPhotos}
                    />
                }
            </ButtonGroup>
        </Box>
    </>
}
