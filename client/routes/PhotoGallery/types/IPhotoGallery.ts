import {IGallery} from '../../../shared/types/IGallery';
import {IPhoto} from '../../../shared/types/IPhoto';

export interface IPhotoGallery extends IGallery, IPhoto {

}
