import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around',
            overflow: 'hidden',
            backgroundColor: theme.palette.background.paper
        },
        galleryControlsContainer: {
            backgroundColor: theme.palette.grey[200],
            margin: '20px 20px 10px 20px',
            padding: 10,
            display: 'flex',
            alignItems: 'center'
        },
        controlBtn: {
          width: 200
        },
        icon: {
            color: 'rgba(255, 255, 255, 0.54)'
        },
        image: {
            top: '50%',
            width: '100%',
            position: 'relative',
            transform: 'translateY(-50%)',
            [theme.breakpoints.down('xs')]: {
                top: 0,
                left: '50%',
                width: 'auto',
                height: '100%',
                transform: 'translateX(-50%)'
            }
        }
    })
);
