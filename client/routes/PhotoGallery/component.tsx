import React, {useEffect} from 'react';
import {BackDrop} from '../../shared/components/BackDrop/component';
import Gallery from 'react-grid-gallery'
import {Box} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {userStorage} from '../../application/UserStorage';
import {getGallery} from '../../state/gallery/selectors';
import {getPhotos, selectPhoto} from '../../state/gallery';
import {GalleryControls} from './components/GalleryControls/component';


function PhotoGallery() {

    const gallery = useSelector(getGallery);
    const dispatch = useDispatch();

    const onSelectImage = (index) => {
        dispatch(selectPhoto(index));
    }

    useEffect(() => {
        dispatch(getPhotos(userStorage.getRole()));
    }, []);

    return <>
        {gallery.loading && <BackDrop/>}

        <GalleryControls/>

        <Box m={5}>
            <Gallery
                images={gallery.photos}
                onSelectImage={onSelectImage}
                margin={4}
                backdropClosesModal={true}
                preloadNextImage={false}
            />
        </Box>
    </>
}

export default PhotoGallery;
