import React, {ReactElement} from 'react';
import {Router, Route, Switch, Redirect} from 'react-router-dom';
import LoginPage from './LoginPage/component';
import {LoginForm} from './LoginPage/components/loginForm/component';
import {RegistrationForm} from './LoginPage/components/registrationForm/component';
import PrivateRoute from '../shared/components/PrivateRoute/component';
import SideBar from './SideBar/component';
import ManageTags from './ManageTags/component';
import AccountInfo from './AccountInfo/AccountInfo';
import AccountList from './AccountList/component';
import SnapdConfig from './SnapdConfig/component';
import PhotoGallery from './PhotoGallery/component';
import {RouterLinks} from '../shared/types/RouterLinks';
import {UserRole} from '../shared/types/UserRole';
import history from '../utils/history';

export default function Routes(): ReactElement {

    return (
        <Router history={history}>
            <Switch>

                <Route exact path='/'>
                    <Redirect to={RouterLinks.GALLERY}/>
                </Route>

                {/*<LoginPage>*/}
                <Route path={RouterLinks.LOGIN}>
                    <LoginPage><LoginForm/></LoginPage>
                </Route>
                <Route path={RouterLinks.REGISTRATION}>
                    <LoginPage><RegistrationForm/></LoginPage>
                </Route>

                {/*<Photo Gallery Page>*/}
                <PrivateRoute
                    role={[UserRole.USER, UserRole.ADMIN]}
                    path={RouterLinks.GALLERY}
                >
                    <SideBar><PhotoGallery/></SideBar>
                </PrivateRoute>

                {/*<Manage Tags Page>*/}
                <PrivateRoute
                    role={[UserRole.USER, UserRole.ADMIN]}
                    path={RouterLinks.MANAGE_TAGS}
                >
                    <SideBar><ManageTags/></SideBar>
                </PrivateRoute>

                {/*<Account Info Page>*/}
                <PrivateRoute
                    role={[UserRole.USER, UserRole.ADMIN]}
                    path={RouterLinks.ACCOUNT_INFO}
                >
                    <SideBar><AccountInfo/></SideBar>
                </PrivateRoute>

                {/*<ADMIN ONLY>*/}
                {/*<Account List Page>*/}
                <PrivateRoute
                    role={[UserRole.ADMIN]}
                    path={RouterLinks.ACCOUNT_LIST}
                >
                    <SideBar><AccountList/></SideBar>
                </PrivateRoute>

                {/*<Config Page>*/}
                <PrivateRoute
                    exact
                    role={[UserRole.ADMIN]}
                    path={RouterLinks.CONFIG}
                >
                    <SideBar><SnapdConfig/></SideBar>
                </PrivateRoute>

            </Switch>

        </Router>
    )
}
