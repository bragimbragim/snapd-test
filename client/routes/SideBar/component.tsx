import React, {useEffect, useState} from 'react';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItems from './components/ListItems/ListItems';
import useStyles from './styles';
import {snapd} from '../../application/snapd';
import {ISideBarListItem} from '../../shared/types/ISideBarListItem';
import {ListItem} from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {ISideBarProps} from './types/ISideBarProps';
import ReaderControl from '../../shared/components/ReaderControl/component';
import AppControls from './components/AppControls/component';
import {ReaderMode} from '../../../globalTypes/ReaderMode';
import {api} from '../../application/api';
import {MemoryChecker} from '../../shared/components/MemoryChecker/component';
import {UnitNameControl} from './components/UnitNameSelect/component';
import {ModalContainer} from '../../shared/components/ModalContainer/component';

export default function SideBar({children}: ISideBarProps) {

    const classes = useStyles();

    const [open, setOpen] = React.useState(false);
    const [menu, setMenu] = useState<ISideBarListItem[]>([]);

    const toggleDrawer = () => setOpen(!open);

    const onLogout = () => {
        api.auth.logout();
    }

    useEffect(() => {
        const menuItems = snapd.getMenuItems();
        setMenu(menuItems);
    }, []);

    return (
        <div className={classes.root}>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open
                    })
                }}
            >

                <div className={classes.toolbar}>
                    <IconButton
                        color="inherit"
                        onClick={toggleDrawer}
                    >
                        {!open ? <MenuIcon/> : <ChevronLeftIcon/>}
                    </IconButton>
                </div>

                <Divider/>

                <ListItem button className={classes.avatar}>
                    <UnitNameControl/>
                </ListItem>

                <Divider/>

                <List className={classes.list}>
                    {menu.length > 0 ? <ListItems list={menu}/> : null}
                </List>

                <Divider/>

                <ListItem button onClick={onLogout} className={classes.logout}>
                    <ListItemIcon><ExitToAppIcon/></ListItemIcon>
                    <ListItemText primary='Logout'/>
                </ListItem>

                <AppControls open={open}/>

            </Drawer>
            <main className={
                clsx(classes.content, {
                    [classes.contentOpen]: open,
                    [classes.contentClose]: !open
                })}>
                <ReaderControl mode={ReaderMode.MAKE_PHOTO}/>
                {children}
            </main>

            <MemoryChecker/>
            <ModalContainer/>

        </div>
    );
}
