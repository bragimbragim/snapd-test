import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

const drawerWidth = 240;

export default makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex'
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
            whiteSpace: 'nowrap'
        },
        list: {
          padding: 0
        },
        listItem: {
            backgroundColor: 'black'
        },
        drawerOpen: {
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            })
        },
        drawerClose: {
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen
            }),
            overflowX: 'hidden',
            width: theme.spacing(9) + 1
        },
        toolbar: {
            display: 'flex',
            alignItems: 'center',
            marginLeft: 5,
            padding: theme.spacing(0, 1),
            ...theme.mixins.toolbar,
            [theme.breakpoints.up('xs')]: {
                minHeight: 72
            }
        },
        content: {
            padding: 0
        },
        contentOpen: {
          width: `calc(100% - ${drawerWidth}px)`
        },
        contentClose: {
            width: `calc(100% - ${theme.spacing(9) + 1}px)`
        },
        avatar: {
            display: 'flex',
            alignItems: 'center',
            padding: 16
        },
        avatarName: {
            marginLeft: 25,
            minWidth: 0,
            overflow: 'hidden'
        },
        logout: {
            paddingLeft: 25
        },
        appControl: {
            position: 'absolute',
            bottom: 20,
            left: 0,
            right: 0,
            textAlign: 'center'
        }
    })
);
