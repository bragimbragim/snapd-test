import React, {ReactElement} from 'react';
import {IListItemsProps} from './types/IListItemsProps';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import useStyles from './styles';
import {Icon} from '@material-ui/core';
import {Link} from 'react-router-dom';

export default function ListItems({list}: IListItemsProps): ReactElement {

    const classes = useStyles();

    return <>
        {list.map((text) => (
            <ListItem
                component={Link}
                to={text.link}
                button key={text.link}
                className={classes.listItem}
            >
                <ListItemIcon>
                    <Icon
                        component={text.icon}
                        color='secondary'
                    />
                </ListItemIcon>
                <ListItemText primary={text.caption}/>
            </ListItem>
        ))}
    </>

}
