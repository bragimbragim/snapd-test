import {createStyles, makeStyles} from '@material-ui/core/styles';

export default makeStyles(() =>
    createStyles({
        listItem: {
            paddingLeft: 25
        }
    })
);
