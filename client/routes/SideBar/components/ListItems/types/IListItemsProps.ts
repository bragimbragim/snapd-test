import {ISideBarListItem} from '../../../../../shared/types/ISideBarListItem';

export interface IListItemsProps {
    list: ISideBarListItem[];
}
