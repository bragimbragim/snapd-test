import React, {useEffect, useState} from "react";
import {Avatar, Box, FormControl, MenuItem, Select, Typography} from '@material-ui/core';
import useStyles from '../../styles';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import IconButton from '@material-ui/core/IconButton';
import {api} from '../../../../application/api';
import {useDispatch} from 'react-redux';
import {openAlert} from '../../../../state/alert';
import {AlertType} from '../../../../state/alert/types/AlertType';

const unitNamesArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

export function UnitNameControl() {

    const [unitName, setUnitName] = useState<string>('A');
    const [editable, setEditable] = useState<boolean>(false);

    const classes = useStyles();
    const dispatch = useDispatch();

    const getUnitName = () => {
        api.system.getUnitName()
            .then((name) => setUnitName(name))
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
    }

    const edit = () => {
        setEditable(true);
    }

    const save = () => {
        api.system.setUnitName(unitName)
            .then((data) => dispatch(openAlert(data, AlertType.SUCCESS)))
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
        
        setEditable(false);
    }

    const handleChange = (event) => {
        setUnitName(event.target.value);
    }

    useEffect(() => {
        getUnitName();
    }, []);

    return <>
        <Avatar>{unitName}</Avatar>
        {
            !editable &&
            <Typography variant='subtitle1' className={classes.avatarName}>Unit Name: {unitName}</Typography>
        }
        {
            editable &&
            <Box ml={2}>
                <FormControl>
                    <Select value={unitName} onChange={handleChange}>
                        {
                            unitNamesArray.map((name, index) => {
                                return <MenuItem key={index} value={name}>{name}</MenuItem>
                            })
                        }
                    </Select>
                </FormControl>
            </Box>
        }
        <Box ml={2}>
            <IconButton color="inherit" onClick={editable ? save : edit}>
                {editable ? <SaveIcon/> : <EditIcon/>}
            </IconButton>
        </Box>

    </>
}
