import {IDefaultComponentProps} from '../../../../../shared/types/IDefaultComponentProps';

export interface IAppControlsProps extends IDefaultComponentProps {
    open: boolean;
}
