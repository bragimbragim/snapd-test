import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) =>
    createStyles({
        drawerOpen: {
            display: 'inline-block'
        },
        drawerClose: {
            display: 'flex',
            flexDirection: 'column-reverse',
            width: '70%',
            margin: '0 auto'
        },
        appControl: {
            position: 'absolute',
            bottom: 20,
            left: 0,
            right: 0,
            textAlign: 'center'
        }
    })
);
