import React from "react";
import CustomTooltip from '../../../../shared/components/Tooltip/component';
import IconButton from '@material-ui/core/IconButton';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import RefreshIcon from '@material-ui/icons/Refresh';
import useStyles from './styles';
import {IAppControlsProps} from './types/IAppControlsProps';
import clsx from 'clsx';
import {snapd} from '../../../../application/snapd';
import {useDispatch} from 'react-redux';
import {openAlert} from '../../../../state/alert';
import {AlertType} from '../../../../state/alert/types/AlertType';
import {api} from '../../../../application/api';


export default function AppControls({open}: IAppControlsProps) {

    const classes = useStyles();
    const dispatch = useDispatch();

    const reboot = () => {
        api.system.rebootRaspberrypi()
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
    }

    const powerOff = () => {
        api.system.powerOffRaspberrypi()
            .catch((err) => dispatch(openAlert(err, AlertType.ERROR)))
    }

    return <>
        <div className={classes.appControl}>
            <div className={clsx({
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open
            })}>
                <CustomTooltip definition={{title: 'Power Off Raspberrypi'}}>
                    <IconButton
                        aria-label="powerOff"
                        color="secondary"
                        onClick={powerOff}
                    >
                        <PowerSettingsNewIcon/>
                    </IconButton>
                </CustomTooltip>

                <CustomTooltip definition={{title: 'Reboot Raspberrypi'}}>
                    <IconButton
                        aria-label="powerOff"
                        color="primary"
                        onClick={reboot}
                    >
                        <RefreshIcon/>
                    </IconButton>
                </CustomTooltip>
            </div>
        </div>
    </>
}
