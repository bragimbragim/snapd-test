import React, {useMemo} from "react";
import {LogoutReason} from '../../../../../../shared/types/LogoutReason';
import {Alert} from '@material-ui/lab';
import {ILogoutReasonAlertProps} from './types/ILogoutReasonAlertProps';

export function LogoutReasonAlert({reason}: ILogoutReasonAlertProps) {

    const getReasonCaption = useMemo(() => {
        if (reason === LogoutReason.SESSION_EXPIRED) {
            return 'Session expired'
        }
        if (reason == LogoutReason.UPDATE_SYSTEM_TIME) {
            return 'Please relogin after system time is updated.'
        }
    }, [reason]);

    return <>
        {
            reason !== LogoutReason.NO_REASON
            ? <Alert severity="info">{getReasonCaption}</Alert>
            : null
        }
    </>
}
