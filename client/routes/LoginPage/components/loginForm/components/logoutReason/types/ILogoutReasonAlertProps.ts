import {IDefaultComponentProps} from '../../../../../../../shared/types/IDefaultComponentProps';
import {LogoutReason} from '../../../../../../../shared/types/LogoutReason';

export interface ILogoutReasonAlertProps extends IDefaultComponentProps {
    reason: LogoutReason
}
