import React, {useEffect, useState} from 'react';
import {useForm} from 'react-hook-form';
import {ILoginFormInputs} from './types/ILoginFormInputs';
import {
    Box,
    Button,
    CircularProgress,
    Divider,
    TextField
} from '@material-ui/core';
import {isDefined} from '../../../../utils/isDefined';
import {useHistory} from 'react-router-dom';
import {ISignInReq} from '../../../../shared/types/ISignInReq';
import {useDispatch} from 'react-redux';
import {openAlert} from '../../../../state/alert';
import {AlertType} from '../../../../state/alert/types/AlertType';
import {LogoutReasonAlert} from './components/logoutReason/component';
import {useQuery} from '../../../../shared/hooks/useQuery';
import {LogoutReason} from '../../../../shared/types/LogoutReason';
import {api} from '../../../../application/api';

export function LoginForm() {

    const {register, handleSubmit, errors} = useForm<ILoginFormInputs>();
    const [loading, setLoading] = useState(false);
    const [logoutReason, setLogoutReason] = useState<LogoutReason>(null);
    const [backUrl, setBackUrl] = useState('/gallery');

    const dispatch = useDispatch();
    const history = useHistory();
    const query = useQuery();

    const onSubmit = (data: ISignInReq) => {
        setLoading(true);
        api.auth.signIn(data)
            .then(() => {
                setLoading(false);
                dispatch(openAlert('Logged In', AlertType.SUCCESS))
                history.push(backUrl);
            })
            .catch((error) => {
                setLoading(false);
                dispatch(openAlert(error, AlertType.ERROR))
            })
    }

    const onCreateNewAccountClick = () => {
        history.push('/registration');
    }

    useEffect(() => {
        const backUrl = query.get('backUrl');
        const reason = query.get('reason') as unknown as LogoutReason;
        if (backUrl) {
            setBackUrl(backUrl);
        }
        if (reason !== LogoutReason.NO_REASON) {
            setLogoutReason(Number(reason));
        }
    }, []);

    return <>
        {loading ? <CircularProgress/> : null}
        <LogoutReasonAlert reason={logoutReason}/>
        <Box>
            <h1>Snapd</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Box display='flex' flexDirection='column'>
                    <TextField
                        error={isDefined(errors.login)}
                        name='login'
                        inputRef={
                            register({
                                required: 'Field is Required'
                            })
                        }
                        label='Tags ID or Email'
                        helperText={errors.login && errors.login.message}
                    />

                    <TextField
                        error={isDefined(errors.password)}
                        name='password'
                        inputRef={register({
                            required: 'Field is required'
                        })}
                        label='Password'
                        helperText={errors.password && errors.password.message}
                        type='password'
                    />

                    <Box mt={2}>
                        <Button
                            type='submit'
                            variant='contained'
                            color='primary'
                            disabled={loading}
                        >Login</Button>
                    </Box>

                    <Box mt={2} mb={2}>
                        <Divider/>
                    </Box>

                    <Box>
                        <Button
                            variant='contained'
                            color='secondary'
                            fullWidth
                            onClick={onCreateNewAccountClick}
                            disabled={loading}
                        >Create New Account</Button>
                    </Box>

                </Box>
            </form>
        </Box>
    </>
}
