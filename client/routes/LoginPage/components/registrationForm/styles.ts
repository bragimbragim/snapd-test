import {makeStyles, Theme} from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    detectTags: {
        height: 250,
        overflowY: 'auto'
    }
}));
