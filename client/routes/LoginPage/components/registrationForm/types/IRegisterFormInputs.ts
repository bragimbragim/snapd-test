export interface IRegisterFormInputs {
    tagId: string;
    email: string;
    password: string;
    confirmPassword: string;
}
