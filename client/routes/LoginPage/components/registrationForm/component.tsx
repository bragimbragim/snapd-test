import React, {useState} from 'react';
import {Box, Button, CircularProgress, FormControl, TextField, Typography} from '@material-ui/core';
import {isDefined} from '../../../../utils/isDefined';
import {useForm} from 'react-hook-form';
import {IRegisterFormInputs} from './types/IRegisterFormInputs';
import {ISignUpReq} from '../../../../shared/types/ISignUpReq';
import {useDispatch} from 'react-redux';
import {openAlert} from '../../../../state/alert';
import {useHistory} from 'react-router-dom';
import {AlertType} from '../../../../state/alert/types/AlertType';
import {ActiveTagsPanel} from '../../../../shared/components/ActiveTagsPanel/component';
import {ReaderMode} from '../../../../../globalTypes/ReaderMode';
import ReaderControl from '../../../../shared/components/ReaderControl/component';
import useStyles from './styles';
import {api} from '../../../../application/api';

export function RegistrationForm() {

    const {register, handleSubmit, errors, watch, setValue} = useForm<IRegisterFormInputs>();

    const [loading, setLoading] = useState(false);

    const history = useHistory();
    const dispatch = useDispatch();
    const classes = useStyles();

    const onSubmit = (data: ISignUpReq) => {
        setLoading(true);
        api.auth.signUp(data)
            .then(() => {
                setLoading(false);
                dispatch(openAlert('You have been successfully registered', AlertType.SUCCESS));
                history.push('/login');
            })
            .catch((error) => {
                setLoading(false);
                dispatch(openAlert(error, AlertType.ERROR))
            })
    }

    const handleSelectTag = (newValue: string) => {
        if (!Number.isNaN(+newValue)) {
            setValue('tagId', String(+newValue), {shouldValidate: true})
            return;
        }
        setValue('tagId', newValue, {shouldValidate: true});
    }

    return <>
        {loading && <CircularProgress/>}
        <Box>
            <Box mb={2}>
                <Typography variant='h3'>Sign Up</Typography>
                <Typography>It's quick and easy</Typography>
            </Box>
            <Box display='flex'>
                <form onSubmit={handleSubmit(onSubmit)}>

                    <FormControl fullWidth component="fieldset">

                        <Box pt={1}>
                            <TextField
                                InputLabelProps={{shrink: true}}
                                error={isDefined(errors.tagId)}
                                name='tagId'
                                inputRef={register({
                                    required: 'Field is Required'
                                })}
                                label='Tag ID'
                                helperText={errors.tagId && errors.tagId.message}
                                fullWidth
                            />
                        </Box>

                        <Box pt={1}>
                            <TextField
                                error={isDefined(errors.email)}
                                name='email'
                                inputRef={register({
                                    required: 'Field is required',
                                    pattern: {
                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                        message: "Invalid email address"
                                    }
                                })}
                                label='Email'
                                helperText={errors.email && errors.email.message}
                                fullWidth
                            />
                        </Box>

                        <Box pt={1}>
                            <TextField
                                error={isDefined(errors.password)}
                                name='password'
                                inputRef={register({
                                    required: 'Please, input a password with at least 8 symbols',
                                    pattern: {
                                        value: /^.{8,}$/,
                                        message: 'Please, input a password with at least 8 symbols'
                                    }
                                })}
                                label='Password'
                                helperText={errors.password && errors.password.message}
                                placeholder='At least 8 symbols'
                                type='password'
                                fullWidth
                            />
                        </Box>

                        <Box pt={1}>
                            <TextField
                                error={isDefined(errors.confirmPassword)}
                                name='confirmPassword'
                                inputRef={register({
                                    required: 'Please, input a password with at least 8 symbols',
                                    validate: (value: string) => value === watch('password') || 'Passwords don\'t match.'
                                })}
                                label='Confirm Password'
                                helperText={errors.confirmPassword && errors.confirmPassword.message}
                                type='password'
                                fullWidth
                            />
                        </Box>

                        <Box mt={2}>
                            <Button
                                type='submit'
                                variant='contained'
                                color='primary'
                            >Sign Up</Button>
                        </Box>

                    </FormControl>

                </form>
                <Box ml={2}>
                    <ReaderControl mode={ReaderMode.DETECT_ONLY}/>
                    <Box className={classes.detectTags}>
                        <ActiveTagsPanel onSelect={handleSelectTag}/>
                    </Box>
                </Box>
            </Box>
        </Box>
    </>
}
