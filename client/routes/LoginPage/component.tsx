import {ILoginPageProps} from './types/ILoginPageProps';
import React from 'react';
import {Box, Card, Grid} from '@material-ui/core';

export default function LoginPage({children}: ILoginPageProps) {

    return <>
        <Grid
            container
            style={{height: 'calc(100vh - 16px)'}}
            direction="row"
            justify="center"
            alignItems="center">

            <Grid item>
                <Box m={1}>
                    <Card>
                        <Box p={3}>
                            {children}
                        </Box>
                    </Card>
                </Box>
            </Grid>
        </Grid>
    </>

}
