import bootstrapAxios from './axios';
import bootstrapInterceptors from './interceptors';
import memoryChecker from './memoryChecker';

export default function bootstrap() {
    bootstrapAxios();
    bootstrapInterceptors();
    memoryChecker();
}
