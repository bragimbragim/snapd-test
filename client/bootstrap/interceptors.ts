import axios, {AxiosError} from 'axios';
import {LogoutReason} from '../shared/types/LogoutReason';
import {api} from '../application/api';

export default function bootstrap() {
    axios.interceptors.response.use(
        (response) => response,
        (error: AxiosError): Promise<string> => {

            if (!error.response) {
                return Promise.reject(error.message);
            }

            if ([401, 403].includes(error.response?.status)) {
                api.auth.logout(LogoutReason.SESSION_EXPIRED);
                return Promise.reject('Not authorized');
            }

            return Promise.reject(error.response.data);
        }
    )
}
