import store from '../state/store';
import {io} from 'socket.io-client';
import appConfig from '../config';
import {SocketEvent} from '../../globalTypes/SocketEvent';
import {MemoryData} from '../../globalTypes/MemoryData';
import {updateMemoryDataAction} from '../state/services/memory';

let socket;

export default function MemoryChecker() {
    if (socket?.connected) {
        return;
    }
    socket = io(`${appConfig.API_URL}/memory`, {
        transports: ['websocket'],
        reconnection: false
    });

    socket.on(SocketEvent.MEMORY.DATA, (status: MemoryData) => {
        store.dispatch(updateMemoryDataAction(status));
    });
}
