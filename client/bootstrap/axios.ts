import axios from 'axios';
import config from '../config';

export default function bootstrap() {
    axios.defaults.withCredentials = true;
    axios.defaults.baseURL = config.API_URL + '/api/';
}
