import {createMuiTheme} from '@material-ui/core';

export default createMuiTheme({
    overrides: {
        MuiCard: {
            root: {
                position: 'relative'
            }
        }
    }
});
