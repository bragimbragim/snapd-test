export interface ILog {
    entity: string;
    event: string;
    name?: string;
    time: string;
}
