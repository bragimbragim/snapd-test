export interface ISnapdConfig {
    unitName?: string;
    WifiSsid?: string;
    WifiPsk?: string;
}
