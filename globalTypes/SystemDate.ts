export interface SystemDate {
    date: string;
    time: string;
}
