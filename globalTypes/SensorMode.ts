export const SensorModeArray = [0, 1, 2, 3, 4];

export type SensorModeType = typeof SensorModeArray[number];
