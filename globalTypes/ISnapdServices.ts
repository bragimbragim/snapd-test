import {ReaderStatus} from './ReaderStatus';
import {IServiceStatus} from './ServiceStatus';

export interface ISnapdServices {
    camera: IServiceStatus,
    mqtt: IServiceStatus,
    memory: IServiceStatus,
    amazon: IServiceStatus,
    reader: ReaderStatus,
}

type ArrayProperties<T, Condition> = {
    [K in keyof T]: T[K] extends Condition ? K : never
}[keyof T]

export type ISnapdServicesNames = ArrayProperties<ISnapdServices, IServiceStatus>;
