export enum CameraTimelapseCommand {
    START_CAPTURE,
    STOP_CAPTURE
}
