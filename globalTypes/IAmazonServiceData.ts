import {IAmazonQueueItemStatus} from './AmazonQueueItemStatus';
import {IServiceStatus} from './ServiceStatus';

export interface IAmazonServiceData {
    queueCount: string;
    queueList: IAmazonQueueItemStatus[];
    serviceStatus: IServiceStatus;
}
