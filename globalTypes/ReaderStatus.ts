import {ReaderMode} from './ReaderMode';

export enum ReaderStatusCode {
    NO_ANSWER = '0',
    GLOBAL_SUCCESS = '1',
    GLOBAL_ERROR = '2',
    ACTIVE = '3',
    INACTIVE = '4',
    NOT_INITIALIZED = '5',
    INITIALIZED = '6'
}

export interface ReaderStatus {
    mode?: ReaderMode;
    code?: ReaderStatusCode;
    error?: string;
}

export const initReaderStatus: ReaderStatus = {
    mode: ReaderMode.MAKE_PHOTO,
    code: ReaderStatusCode.NOT_INITIALIZED
};
