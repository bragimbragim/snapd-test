export const ExposureModeArray = [
    'default',
    'auto',
    'night',
    'nightpreview',
    'backlight',
    'spotlight',
    'sports',
    'snow',
    'beach',
    'verylong',
    'fixedfps',
    'antishake',
    'fireworks'
] as const;

export type ExposureMode = typeof ExposureModeArray[number];
