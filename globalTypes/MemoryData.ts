export interface MemoryData {
    size: number;
    available: number;
    warning?: string;
}
