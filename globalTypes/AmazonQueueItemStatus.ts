import {AmazonStatus} from '../src/types/AmazonStatus';

export interface IAmazonQueueItemStatus extends AmazonStatus {
    photoName: string;
    photoDate: string;
}
