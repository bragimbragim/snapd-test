import {ExposureMode} from './ExposureMode';
import {SensorModeType} from './SensorMode';

export interface ICamera {
    disabled?: boolean;
    debugMode?: boolean;
    firstPhotoDelayMs?: number;
    photoCount?: number;
    limitCameraMode?: boolean;
}

export interface ICameraConfig extends ICamera {
    ISO?: number | string;
    mode?: SensorModeType;
    shutter?: number | string;
    exposure?: ExposureMode;
    flicker?: string;
    profile?: 'baseline';
    ev?: number;
    sharpness?: number;
    contrast?: number;
    brightness?: number;
    saturation?: number;
    width?: number;
    height?: number;
    framerate?: number;
}
