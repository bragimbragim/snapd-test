export enum ReaderControlCommand {
    STOP_READ = '0',
    START_READ = '1',
    CHECK_ALIVE = '2'
}
