export interface ILogEvent {
    type: 'photo' | 'reader',
    timestamp: number;
    caption: string;
}
