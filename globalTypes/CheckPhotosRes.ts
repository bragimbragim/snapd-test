export interface CheckPhotoRes {
    path: string,
    time: number
}
