export interface IAmazonConfig {
    loadPhotosToAmazon: boolean;
    debugLocation: boolean;
    notSavePhotosInUnit: boolean;
}
