const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');

function srcPaths(src) {
    return path.join(__dirname, src);
}

module.exports = (env) => {
    return {
        entry: './client/index.tsx',
        mode: 'development',
        devtool: 'source-map',
        output: {
            path: path.resolve(__dirname, './dist'),
            filename: 'bundle.js',
            publicPath: '/',
        },
        resolve: {
            alias: {
                "@material-ui/styles": path.resolve(__dirname, "node_modules", "@material-ui/styles"),
                '@public': path.resolve(srcPaths('./public'))
            },
            extensions: ['.js', '.json', '.ts', '.tsx']
        },
        module: {
            rules: [
                {
                    test: /\.(ts|tsx)$/,
                    exclude: /node_modules/,
                    loader: 'ts-loader'
                },
                {
                    test: /\.(scss|css)$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.(jpg|png|svg|ico|icns)$/,
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]'
                    }
                }
            ]
        },
        devServer: {
            contentBase: path.join(__dirname, './dist'),
            historyApiFallback: true,
            // compress: true,

            port: 4200
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, './public/index.html')
            }),
            new CopyPlugin({
                patterns: [
                    {
                        from: './public/assets/*',
                        to: '[path][name].[ext]'
                    }
                ]
            }),
            new DefinePlugin({
                'process.env.API_URL': JSON.stringify(env.API_URL)
            })
        ]
    }
};
