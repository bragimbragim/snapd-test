import {ICamera, ICameraConfig} from '../../globalTypes/ICameraConfig';

const cameraOptions: ICamera = {
    // Global disable camera
    disabled: false,
    // Flag for testing autonomous work (not save photo to amazon and unit)
    debugMode: false,
    firstPhotoDelayMs: 0,
    // count of photos for tag
    photoCount: 1,
    // camera mode
    limitCameraMode: false
}

export const DefaultConfig: ICameraConfig = {
    ...cameraOptions
} as const;

