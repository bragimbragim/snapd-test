import {IAmazonConfig} from '../../globalTypes/IAmazonConfig';

export const DefaultConfig: IAmazonConfig = {
    loadPhotosToAmazon: true,
    debugLocation: false,
    notSavePhotosInUnit: false
} as const;
