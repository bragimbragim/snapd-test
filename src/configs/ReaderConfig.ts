import {IReaderConfig} from '../../globalTypes/IReaderConfig';

export const DefaultConfig: IReaderConfig = {
    autoStart: false,
    rfPower: 10,
    BLF: 'LINK250KHZ',
    tari: 'TARI25US',
    tagEncoding: 'FM0',
    session: 'S0',
    target: 'A',
    Q: 'DynamicQ',
    staticQ: 0
} as const;

export const MaxSpeedConfig: IReaderConfig = {
    autoStart: false,
    rfPower: 20,
    BLF: 'LINK250KHZ',
    tari: 'TARI25US',
    tagEncoding: 'FM0',
    session: 'S0',
    target: 'A',
    Q: 'DynamicQ',
    staticQ: 0
} as const;

export const MaxDistanceConfig: IReaderConfig = {
    autoStart: false,
    rfPower: 30,
    BLF: 'LINK250KHZ',
    tari: 'TARI25US',
    tagEncoding: 'FM0',
    session: 'S0',
    target: 'A',
    Q: 'DynamicQ',
    staticQ: 0
} as const;

export const MinDistanceConfig: IReaderConfig = {
    autoStart: false,
    rfPower: 5,
    BLF: 'LINK250KHZ',
    tari: 'TARI25US',
    tagEncoding: 'FM0',
    session: 'S0',
    target: 'A',
    Q: 'DynamicQ',
    staticQ: 0
} as const;
