import {ISnapdConfig} from '../../globalTypes/ISnapdConfig';

export const DefaultConfig: ISnapdConfig = {
    unitName: 'A',
    WifiSsid: 'ITSM-WIFI',
    WifiPsk: '123'
} as const;
