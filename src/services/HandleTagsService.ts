import {Time} from '../entity/Time';
import {Tag} from '../entity/Tag';
import {EventEmitter} from 'events';
import {EmitterEvents} from '../types/EmitterEvents';
import {errorLogger} from '../utils/logger';

type Counter = {
    [key: string]: number;
}

export class HandleTagsService extends EventEmitter {

    public readonly CATCHER_TIMEOUT = 100;
    public tempArray = [];
    private isEmptyActiveTags = false;
    private busy = false;
    private interval;
    private counter: Counter = {};

    constructor() {
        super();
    }

    public catch() {
        if (this.busy) {
            return;
        }

        if (this.isEmptyActiveTags) {
            return;
        }

        this.busy = true;

        this.handleTags()
            .catch((err) => {
                errorLogger.error(err.message, {event: 'HandleTagsService: handleTags().catch'})
                this.emit(EmitterEvents.CATCHER.FATAL_ERROR);
            })
            .finally(() => {
                this.busy = false;
            });
    }

    private async handleTags() {

        const activeTags = await Time.find({timeLoose: null});

        this.isEmptyActiveTags = !this.tempArray.length && !activeTags.length;

        if (this.isEmptyActiveTags) {
            this.emit(EmitterEvents.TAG.All_DISAPPEAR);
        }

        //activate tag
        for (let i = 0; i < this.tempArray.length; i++) {

            const msg = this.tempArray[i];

            this.counter[msg] = 0;

            const found = activeTags
                .find(obj => obj.tag?.name === msg)

            if (!found) {
                let tag = await Tag.findOne({name: msg});
                if (!tag) {
                    tag = new Tag();
                    tag.name = msg;
                    await tag.save();
                }

                const time = new Time();
                time.timeDetect = new Date().valueOf();
                time.tag = tag;
                await time.save();

                this.emit(EmitterEvents.TAG.APPEAR, tag.name);
            }

        }

        //deactivate
        for (let i = 0; i < activeTags.length; i++) {

            const time = activeTags[i];

            if (!this.tempArray.includes(time.tag.name)) {

                this.counter[time.tag.name] = this.counter[time.tag.name]
                                              ? this.counter[time.tag.name] + 1
                                              : 1;

                if (this.counter[time.tag.name] > 10) {
                    time.timeLoose = new Date().valueOf();
                    await time.save();

                    this.emit(EmitterEvents.TAG.DISAPPEAR, time.tag.name);
                    delete this.counter[time.tag.name];
                }
            }
        }

        this.tempArray = [];
    }

    public startCatcher() {
        clearInterval(this.interval);
        this.interval = setInterval(() => {
            this.catch();
        }, this.CATCHER_TIMEOUT);
    }

    public stopCatcher() {
        clearInterval(this.interval);
        this.busy = false;
        // this.tempArray = [];
    }

    public pushNewTag(tag: string) {
        if (!this.tempArray.includes(tag)) {
            this.isEmptyActiveTags = false;
            this.tempArray.push(tag);
        }
    }

}
