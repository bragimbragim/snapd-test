import {Service} from 'typedi';
import {DefaultConfig as DefaultCameraConfig} from '../configs/CameraConfig';
import {DefaultConfig as DefaultReaderConfig} from '../configs/ReaderConfig';
import {DefaultConfig as DefaultSnapdConfig} from '../configs/SnapdConfig';
import {DefaultConfig as DefaultAmazonConfig} from '../configs/AmazonConfig';
import {HardwareConfigPropsType} from '../types/HardwareConfigProps';
import * as path from 'path';
import {ICameraConfig} from '../../globalTypes/ICameraConfig';
import {IReaderConfig} from '../../globalTypes/IReaderConfig';
import {EventEmitter} from 'events';
import {EmitterEvents} from '../types/EmitterEvents';
import {ISnapdConfig} from '../../globalTypes/ISnapdConfig';
import {IAmazonConfig} from '../../globalTypes/IAmazonConfig';
import {defaultsDeep} from 'lodash';

const fs = require('fs');
const ini = require('ini');
const colors = require('colors/safe');

export interface ICustomConfig {
    camera: ICameraConfig,
    reader: IReaderConfig,
    snapd: ISnapdConfig;
    amazon: IAmazonConfig;
}

const CONFIG_PATH = '../configs/CurrHardwareConfig.ini';

@Service()
export class ConfigService extends EventEmitter {

    private initError: Error = null;

    private defaultCameraConfig = DefaultCameraConfig;
    private defaultReaderConfig = DefaultReaderConfig;
    private defaultSnapdConfig = DefaultSnapdConfig;
    private defaultAmazonConfig = DefaultAmazonConfig;

    constructor() {
        super();
        this.init();
    }

    public init() {

        const error = this.createFileIfNotExist();

        if (error) {
            return;
        }

        const defaultGlobalConfig: ICustomConfig = {
            reader: DefaultReaderConfig,
            camera: DefaultCameraConfig,
            amazon: DefaultAmazonConfig,
            snapd: DefaultSnapdConfig
        }
        const currConfig = this.parseConfig();
        const updatedConfig = defaultsDeep(currConfig, defaultGlobalConfig);
        this.saveConfig(updatedConfig);
    }

    private getDefaultConfig(configType: HardwareConfigPropsType) {
        let defaultConfig;
        switch (configType) {
            case 'camera':
                defaultConfig = this.defaultCameraConfig;
                break;
            case 'reader':
                defaultConfig = this.defaultReaderConfig;
                break;
            case 'snapd':
                defaultConfig = this.defaultSnapdConfig;
                break;
            case 'amazon':
                defaultConfig = this.defaultAmazonConfig;
        }

        return defaultConfig;
    }

    public getCurrConfig<T>(configType: HardwareConfigPropsType): T {
        let defaultConfig = this.getDefaultConfig(configType);
        return this.parseConfig<T>(configType) ?? defaultConfig;
    }

    public updateCustomConfig<T>(configType: HardwareConfigPropsType, config: T) {
        let defaultConfig = this.getDefaultConfig(configType);

        const allConfig = this.parseConfig<ICustomConfig>();

        if (!allConfig.hasOwnProperty(configType)) {
            const newConfig = {
                ...allConfig,
                [configType]: {...defaultConfig, ...config}
            };
            this.saveConfig(newConfig);
            return;
        }

        const updatedConfig = {
            ...allConfig,
            [configType]: {...allConfig[configType], ...config}
        }
        this.saveConfig(updatedConfig);
    }

    protected parseConfig<T>(configType?: HardwareConfigPropsType): T | null {
        const config = ini.parse(
            fs.readFileSync(path.resolve(__dirname, CONFIG_PATH), 'utf-8')
        );

        if (!configType) {
            return config;
        }

        return config.hasOwnProperty(configType) ? config[configType] : null;
    }

    protected saveConfig(config): void {
        fs.writeFileSync(
            path.resolve(__dirname, CONFIG_PATH),
            ini.stringify(config)
        );
        this.emit(EmitterEvents.CONFIG.UPDATE, this.parseConfig());
    }

    private createFileIfNotExist() {
        try {
            const exist = fs.existsSync(path.resolve(__dirname, CONFIG_PATH));
            if (!exist) {
                fs.openSync(path.resolve(__dirname, CONFIG_PATH), 'w')
            }
        } catch (err) {
            this.initError = new Error(`Config init Error: try to set ${CONFIG_PATH} file manually`);
            return false;
        }
    }

    public getInitError(): Error | null {
        return this.initError;
    }

}
