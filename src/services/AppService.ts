import {Service} from 'typedi';
import {MqttService} from './MqttService';
import {ConfigService} from './ConfigService';
import {TimeService} from './TimeService';
import {Camera} from '../lib/RaspiCam';
import {ReaderService} from './ReaderService';
import {AmazonService} from './AmazonService';
import {MemoryService} from './MemoryService';
import {WifiService} from './WifiService';
import {LogService} from './LogService';
import {errorLogger} from '../utils/logger';
import {ISnapdServices} from '../../globalTypes/ISnapdServices';
import {ServiceStatusCode} from '../../globalTypes/ServiceStatus';
import {AutoUpdaterService} from './AutoUpdaterService';

const colors = require('colors/safe');

@Service()
export class AppService {

    constructor(
        private mqttService: MqttService,
        private configService: ConfigService,
        private timeService: TimeService,
        private camera: Camera,
        private readerService: ReaderService,
        private amazonService: AmazonService,
        private memoryService: MemoryService,
        private autoUpdaterService: AutoUpdaterService,
        private logService: LogService // Do not remove
    ) {
    }

    async init() {

        const error = this.checkConfigFileError();
        if (error) {
            throw new Error('Snapd global error');
        }

        const initialized = await this.initDependentReaderModules();

        if (initialized) {
            await this.readerService.init()
                .catch((err) => {
                    console.log(colors.red(err.message));
                    errorLogger.error(err.message, {event: 'readerService: init()'});
                })
        }

        await this.amazonService.init()
            .catch((err) => {
                console.log(colors.red(err.message));
                errorLogger.error(err.message, {event: 'amazonService: init()'});
            })

        // await this.autoUpdaterService.update()
        //     .then((data: string) => {
        //         console.log(data);
        //     })
        //     .catch((err) => {
        //         console.log(colors.red(err.message));
        //         errorLogger.error(err.message, {event: 'autoUpdaterService: update()'});
        //     })
    }

    checkConfigFileError() {
        const configInitError = this.configService.getInitError()
        if (configInitError instanceof Error) {
            console.log(colors.red(configInitError.message));
            errorLogger.error(configInitError.message, {event: 'configService: init()'});
            return true;
        }

        return false;
    }

    async initDependentReaderModules(): Promise<boolean> {

        await this.mqttService.init()
            .catch((err) => {
                console.log(colors.red(err.message));
                errorLogger.error(err.message, {event: 'mqttService: init()'});

            })
        await this.camera.init()
            .catch((err) => {
                console.log(colors.red(err.message));
                errorLogger.error(err.message, {event: 'camera: init()'});
            })
        await this.memoryService.init()
            .catch((err) => {
                console.log(colors.red(err.message));
                errorLogger.error(err.message, {event: 'memoryService: init()'});
            })

        return this.dependentModulesIsInit();
    }

    dependentModulesIsInit(): boolean {
        const dependentModules = [
            this.mqttService.serviceStatus.get(),
            this.camera.serviceStatus.get(),
            this.memoryService.serviceStatus.get()
        ];

        const hasError = dependentModules
            .some((module) => module.code === ServiceStatusCode.ERROR);

        return !hasError;
    }

    getServicesStatus(): ISnapdServices {
        return {
            camera: this.camera.serviceStatus.get(),
            mqtt: this.mqttService.serviceStatus.get(),
            reader: this.readerService.getStatus(),
            memory: this.memoryService.serviceStatus.get(),
            amazon: this.amazonService.serviceStatus.get()
        }
    }

}
