import mqtt, {MqttClient} from 'mqtt';
import {MqttTopic} from '../types/MqttTopics';
import {Service} from 'typedi';
import {EventEmitter} from "events";
import {ServiceStatus, StatusDecorator} from '../utils/ServiceStatusDecorator';
import {EmitterEvents} from '../types/EmitterEvents';
import {IServiceStatus, ServiceStatusCode} from '../../globalTypes/ServiceStatus';

@Service()
export class MqttService extends EventEmitter {
    
    public client: MqttClient;

    constructor(@StatusDecorator('Mqtt') public serviceStatus: ServiceStatus) {
        super();
        this.setMaxListeners(0);
        // this.spammer();
    }

    public init(): Promise<IServiceStatus | Error> {
        if (this.client) {
            this.client.end();
            this.client.removeAllListeners('message');
            this.client.removeAllListeners('error');
        }

        return new Promise((resolve, reject) => {
            this.client = mqtt.connect('mqtt://localhost:1883');

            this.client.once('error', (err) => {
                this.client.end();
                this.serviceStatus.set({code: ServiceStatusCode.ERROR, caption: err.message});
                return reject(new Error('Mqtt: ' + err.message));
            });

            this.client.once('connect', () => {

                this.subscribeAllTopics();

                this.client.on('message', (topic: MqttTopic, message: Buffer) => {
                    this.emit(topic, message.toString());
                });

                this.client.on('error', (err) => {
                    this.serviceStatus.set({code: ServiceStatusCode.ERROR, caption: err.message});
                    this.client.end();
                    this.emit(EmitterEvents.MQTT.ERROR);
                });

                this.serviceStatus.set({code: ServiceStatusCode.SUCCESS, caption: 'Initialized'});

                return resolve(this.serviceStatus.get());
            });
        });
    }

    // spammer() {
    //     this.client.subscribe(MqttTopic.SPAMMER, async (err) => {
    //         if (!err) {
    //             const tags = []
    //             for (let i = 0; i <= 10; i++) {
    //                 const name = uuidv4();
    //                 tags.push(name);
    //             }
    //
    //             setInterval(() => {
    //
    //                 //stable case
    //                 tags.forEach((tag) => {
    //                     this.client.publish(MqttTopic.SPAMMER, tag)
    //                 });
    //
    //             }, 50);
    //
    //             //every 10 sec remove one tag
    //             setInterval(() => {
    //                 tags.shift();
    //                 // console.log(tags.length);
    //             }, 5000)
    //
    //             // setInterval(async () => {
    //             //     const t = await Tag.find();
    //             //     // @ts-ignore
    //             //     if (!t.includes(t[0].name)) {
    //             //         // @ts-ignore
    //             //         tags.push(t[0].name);
    //             //     }
    //             // }, 5000)
    //         }
    //     })
    // }

    public subscribeAllTopics() {
        this.client.subscribe(MqttTopic.READER_STATUS);
        this.client.subscribe(MqttTopic.READER_DATA);
        this.client.subscribe(MqttTopic.AMAZON_STATUS);
        this.client.subscribe(MqttTopic.AMAZON_QUEUE);
        this.client.subscribe(MqttTopic.AMAZON_RESPONSE);
        this.client.subscribe(MqttTopic.CAMERA_STATUS);
    }

    public sendMessage(topic: MqttTopic, message: Buffer | string) {
        this.client.publish(topic, message);
    }
}
