import {Service} from 'typedi';
import {exec} from "child_process";
import {readFileSync, writeFileSync} from 'fs';
import path from 'path';
import {
    IWifiCurrentNetworkResponse,
    IWifiNetworkResponse,
    IWifiScanResponse,
    IWifiItemResponse
} from '../../globalTypes/IWifi';

const util = require('util');
const execPromisify = util.promisify(exec);
const Wifi = require('../lib/rpi-wifi/wifi-connection');
const wifi = new Wifi('wlan1');

const sleep = util.promisify(setTimeout)


@Service()
export class WifiService {

    private hostapdFilePath = '../../build/hostapd.conf';
    private wpaFilePath = '../../build/wpa_supplicant.conf';

    private hostapdRegexp: RegExp = new RegExp(/ssid=(.*)/);
    private wpaPskRegexp: RegExp = new RegExp(/psk=(.*)/);
    private wpaPassphraseRegexp: RegExp = /\b[A-Fa-f0-9]{64}\b/;

    constructor() {
    }

    public async connectToNewNetwork(ssid: string, psk: string): Promise<string | Error> {
        return await wifi
            .connect({ssid, psk, timeout: 10000})
            .then(async () => {
                await this.assignIP();
                return `Successfully connected to network: ${ssid}`;
            })
    }

    public async connectToSavedNetwork(ssid: string): Promise<string | Error> {
        try {
            const networks = await this.getNetworks();

            if (networks instanceof Error) {
                return Promise.reject(networks);
            }

            const networkID = networks.find((item) => item.ssid === ssid).id;
            await execPromisify(`wpa_cli -i wlan1 select_network ${networkID}`);
            console.log(`wpa_cli -i wlan1 select_network ${networkID}`);
            await this.assignIP();
            return `Successfully connected to network`;
        } catch (err) {
            return Promise.reject(err);
        }
    }

    public async removeNetwork(ssid: string): Promise<string | Error> {
        const networks = await this.getNetworks();

        if (networks instanceof Error) {
            return Promise.reject(networks);
        }
        const networkID = networks.find((network) => network.ssid == ssid).id;
        return await wifi.removeNetwork(networkID);
    }

    public async getListOfNetworks(): Promise<IWifiItemResponse[] | Error> {
        const allNetworks = await this.scan();
        const savedNetworks = await this.getNetworks();
        await sleep(1500);
        const currentNetwork = await this.getCurrentNetwork();
        console.log('currentNetwork', currentNetwork);

        if (allNetworks instanceof Error) {
            return Promise.reject(allNetworks)
        }
        if (savedNetworks instanceof Error) {
            return Promise.reject(savedNetworks)
        }
        if (currentNetwork instanceof Error) {
            return Promise.reject(currentNetwork);
        }

        return allNetworks
            .reduce<IWifiScanResponse[]>((acc, curr) => {
                const index = acc.findIndex((net) => net.ssid === curr.ssid);
                if (index === -1) {
                    return [...acc, curr];
                }
                if (curr.signalLevel > acc[index].signalLevel) {
                    acc[index] = curr;
                }
                return acc;
            }, [])
            .map<IWifiItemResponse>((network) => {
                const saved = savedNetworks.some((item) => item.ssid === network.ssid);
                const active = currentNetwork?.ssid === network.ssid;
                const error = (active && !currentNetwork?.ip_address) ? 'No ip address was assigned' : null;
                return {...network, saved, active, error};
            })
            .filter((network) => network.ssid)
    }

    public async scan(): Promise<IWifiScanResponse[] | Error> {
        return await wifi.scan().then((data) => {
            return data;
        })
    }

    public async getNetworks(): Promise<IWifiNetworkResponse[] | Error> {
        return await wifi.getNetworks().then((data) => {
            return data;
        })
    }

    public async getCurrentNetwork(): Promise<IWifiCurrentNetworkResponse | Error> {
        return await wifi.getStatus();
    }

    public async getConnectionStatus(): Promise<boolean | Error> {
        return await wifi.getState();
    }

    public async updateHostapdFile(unitName: string) {
        const hostapdContent = this.hostapdFile.replace(this.hostapdRegexp, `ssid=Snapd_${unitName}`);
        writeFileSync(path.resolve(__dirname, this.hostapdFilePath), hostapdContent);
        await execPromisify('sudo systemctl restart hostapd');
    }

    // public updateWpaFile(body: IWifiRequest): Promise<string> {
    //
    //     return new Promise((resolve, reject) => {
    //
    //         exec(`wpa_passphrase ${body.ssid} ${body.pass}`, async (error: ExecException, stdout: string) => {
    //             if (error) {
    //                 reject(error.message);
    //             }
    //             try {
    //
    //                 const wpaPassphrase: string = String(stdout.match(this.wpaPassphraseRegexp)[0]);
    //
    //                 let supplicantContent = readFileSync(path.resolve(__dirname, this.wpaFilePath), 'utf-8')
    //                     .replace(this.wpaSsidRegexp, `ssid="${body.ssid}"`)
    //                     .replace(this.wpaPskRegexp, `psk=${wpaPassphrase}`)
    //
    //                 writeFileSync(path.resolve(__dirname, '../../build/wpa_supplicant.conf'), supplicantContent, {encoding: 'utf-8'});
    //
    //                 resolve('Wifi settings have been successfully changed');
    //             } catch (err) {
    //                 reject(err.message);
    //             }
    //         });
    //     });
    //
    // }

    private async assignIP(): Promise<void | Error> {
        try {
            const date = new Date().valueOf();
            await execPromisify('sudo dhclient -r wlan1');
            await execPromisify('sudo dhclient wlan1');
            const newDate = new Date().valueOf();
            console.log(`assign IP: ${newDate - date} ms`);

            await this.getCurrentNetwork()
                .then((data: IWifiCurrentNetworkResponse) => {
                    console.log(data);
                    if (!data.hasOwnProperty('ssid') && !data.hasOwnProperty('ip_address')) {
                        return Promise.reject(new Error('Unable to connect this network'));
                    }
                    if (!data.hasOwnProperty('ip_address')) {
                        return Promise.reject(new Error('No ip address was assigned'));
                    }
                })

            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    }

    private get hostapdFile(): string {
        return readFileSync(path.resolve(__dirname, this.hostapdFilePath), "utf-8");
    }

}
