import {Service} from 'typedi';
import {ReaderService} from './ReaderService';
import {Camera} from '../lib/RaspiCam';
import {EmitterEvents} from '../types/EmitterEvents';
import {Log} from '../entity/Log';
import {infoLogger} from '../utils/logger';

@Service()
export class LogService {

    constructor(
        private readerService: ReaderService,
        public camera: Camera
    ) {
        this.readerService.on(EmitterEvents.TAG.APPEAR, this.appearTagHandle.bind(this));
        this.readerService.on(EmitterEvents.TAG.DISAPPEAR, this.disappearTagHandle.bind(this));
        this.camera.raspistill.on(EmitterEvents.PHOTO.CREATED, this.cameraHandle.bind(this));
    }

    private async appearTagHandle(tag: string) {
        // infoLogger.info(tag, {event: 'Tag Appear'});
        const log = new Log();
        log.entity = 'Tag';
        log.event = 'appear';
        log.name = tag;
        log.time = new Date();
        await log.save();
    }

    private async disappearTagHandle(tag: string) {
        // infoLogger.info(tag, {event: 'Tag Disappear'});
        const log = new Log();
        log.entity = 'Tag';
        log.event = 'disappear';
        log.name = tag;
        log.time = new Date();
        await log.save();
    }

    private async cameraHandle() {
        // infoLogger.info('', {event: 'Photo Created'});
        const log = new Log();
        log.entity = 'Photo';
        log.event = 'created';
        log.name = '';
        log.time = new Date();
        await log.save();
    }

}
