import {Service} from 'typedi';
import {ReaderService} from './ReaderService';
import {EmitterEvents} from '../types/EmitterEvents';
import {Camera} from '../lib/RaspiCam';
import {ConfigService, ICustomConfig} from './ConfigService';
import {CalibratePhotoModeService} from './photoModes/CalibratePhotoModeService';
import {StandardPhotoModeService} from './photoModes/StandardPhotoModeService';
import {ICameraConfig} from '../../globalTypes/ICameraConfig';
import {LimitPhotoModeService} from './photoModes/LimitPhotoModeService';

type CameraMode = 'standard' | 'calibrate' | 'limit';

@Service()
export class PhotoService {

    public cameraMode: CameraMode = 'standard';
    private disabled: boolean = false;

    constructor(
        private readerService: ReaderService,
        private configService: ConfigService,
        private calibratePhotoModeService: CalibratePhotoModeService,
        private standardPhotoModeService: StandardPhotoModeService,
        private limitPhotoModeService: LimitPhotoModeService,
        public camera: Camera
    ) {
        this.readerService.on(EmitterEvents.TAG.APPEAR, this.handleAppearTag.bind(this));
        this.readerService.on(EmitterEvents.TAG.DISAPPEAR, this.handleDisappearTag.bind(this));

        const cameraConfig = this.configService.getCurrConfig<ICameraConfig>('camera');
        this.cameraMode = cameraConfig?.limitCameraMode ? 'limit' : this.cameraMode;
        this.disabled = cameraConfig?.disabled ?? this.disabled;

        this.configService.on(EmitterEvents.CONFIG.UPDATE, this.handleConfigUpdate.bind(this));
    }

    public disable() {
        this.disabled = true;
    }

    public enable() {
        this.disabled = false;
    }

    private handleAppearTag(tag) {
        if (this.disabled) {
            return;
        }
        switch (this.cameraMode) {

            case 'limit':
                this.limitPhotoModeService.setActiveTags(tag);
                if (this.limitPhotoModeService.active) {
                    break;
                }
                this.limitPhotoModeService.active = true;
                setTimeout(() => {
                    this.limitPhotoModeService.makePhoto();
                }, this.getFirstPhotoDelay())
                break;

            case 'standard':
                this.standardPhotoModeService.setActiveTags(tag);
                if (this.standardPhotoModeService.active) {
                    break;
                }
                this.standardPhotoModeService.active = true;
                setTimeout(() => {
                    this.standardPhotoModeService.makePhoto();
                }, this.getFirstPhotoDelay())
                break;

            case 'calibrate':
                if (this.calibratePhotoModeService.active) {
                    break;
                }
                this.calibratePhotoModeService.active = true;
                this.calibratePhotoModeService.makePhoto();
                break;

        }
    }

    private handleDisappearTag(tag) {
        if (this.disabled) {
            return;
        }
        switch (this.cameraMode) {
            case 'standard':
                this.standardPhotoModeService.setDisappearedTags(tag);
        }
    }

    public updateMode(mode: CameraMode) {
        this.cameraMode = mode;
    }

    private handleConfigUpdate(config: ICustomConfig) {
        if (config.camera?.limitCameraMode) {
            this.updateMode('limit');
            return;
        }
        this.updateMode('standard');
    }

    public getFirstPhotoDelay() {
        return Number(this.camera.raspistill.options.firstPhotoDelayMs);
    }

}
