import {Service} from 'typedi';
import {Camera} from '../../lib/RaspiCam';
import {AbstractPhotoService} from '../../abstract/AbstractPhotoService';
import {ReaderService} from '../ReaderService';
import {errorLogger} from '../../utils/logger';
import {AmazonService} from '../AmazonService';
import {StandardPhotoResponse} from '../../types/PhotoResponse';

@Service()
export class StandardPhotoModeService extends AbstractPhotoService {

    public active: boolean = false;
    private activeTags: string[] = [];
    private disappearedTags: string[] = [];

    constructor(
        public camera: Camera,
        public readerService: ReaderService,
        private amazonService: AmazonService
    ) {
        super(readerService);
    }

    public makePhoto() {
        console.log(`make photo(): active = ${this.active}`);

        if (this.readerStopped || !this.activeTags.length) {
            this.active = false;
            return;
        }

        this.camera.raspistill.takeStandardPhoto()
            .then(async (response: StandardPhotoResponse) => {
                const {photoName, currTime, unitName, relativePath} = response;
                console.log('active tags before send to amazon: ', this.activeTags);
                if (!this.camera.getConfig()?.debugMode) {
                    this.amazonService.sendPhotoToAmazon(this.activeTags, currTime, photoName, unitName);
                    console.time('PHOTO SAVING: photo was saved with');
                    await this.savePhotoToDB({activeTags: this.activeTags, currTime, photoName, relativePath});
                    console.timeEnd('PHOTO SAVING: photo was saved with');
                }
                this.updateActiveTags();
                console.log('call new makePhoto() fn');
                this.makePhoto();
            })
            .catch((err) => {
                console.log(err.message);
                this.stopWithError();
                errorLogger.error(err.message, {event: 'StandardCameraService: makePhoto().catch'})
            })
    }

    stopWithError() {
        super.stopWithError();
        this.active = false;
    }

    public setActiveTags(tag: string) {
        this.activeTags = Array.from(new Set([...this.activeTags, tag]));
        this.disappearedTags = this.disappearedTags.filter((tag) => tag !== tag);
    }

    public setDisappearedTags(tag: string) {
        this.disappearedTags = Array.from(new Set([...this.disappearedTags, tag]));
    }

    private updateActiveTags() {
        this.activeTags = this.activeTags.filter((tag) => !this.disappearedTags.includes(tag));
        this.disappearedTags = [];
    }

}
