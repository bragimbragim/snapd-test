import {Service} from 'typedi';
import {Camera} from '../../lib/RaspiCam';
import {ReaderService} from '../ReaderService';
import {errorLogger} from '../../utils/logger';
import {StandardPhotoResponse} from '../../types/PhotoResponse';
import {ConfigService} from '../ConfigService';
import {AmazonService} from '../AmazonService';
import {AbstractPhotoService} from '../../abstract/AbstractPhotoService';

type TagsCounter = {
    [tag: string]: number
}

@Service()
export class LimitPhotoModeService extends AbstractPhotoService {

    public active: boolean = false;
    private maxPhotos: number = 5;
    private activeTags: string[] = [];
    private tagsCounter: TagsCounter = {};

    constructor(
        public camera: Camera,
        public readerService: ReaderService,
        private configService: ConfigService,
        private amazonService: AmazonService
    ) {
        super(readerService);
    }

    public makePhoto() {
        this.updateActiveTags();

        if (this.readerStopped || !this.activeTags.length) {
            console.log('STOP RECURSION');
            this.stop();
            return;
        }

        this.camera.raspistill.takeStandardPhoto()
            .then(async (response: StandardPhotoResponse) => {
                const {photoName, currTime, unitName, relativePath} = response;
                // TODO: Update tags. If tagCounter[key] === 0 then +=1
                console.log('active tags before send to amazon: ', this.activeTags);
                if (!this.camera.getConfig().debugMode) {
                    this.amazonService.sendPhotoToAmazon(this.activeTags, currTime, photoName, unitName);
                    console.time('PHOTO SAVING: photo was saved with');
                    await this.savePhotoToDB({activeTags: this.activeTags, currTime, photoName, relativePath});
                    console.timeEnd('PHOTO SAVING: photo was saved with');
                }
                console.log('call new makePhoto() fn');
                this.makePhoto();
            })
            .catch((err) => {
                console.log(err.message);
                this.stopWithError();
                errorLogger.error(err.message, {event: 'StandardCameraService: makePhoto().catch'})
            })
    }

    stop() {
        this.active = false;
        this.activeTags = [];
        this.tagsCounter = {};
    }

    stopWithError() {
        super.stopWithError();
        this.stop();
    }

    public setActiveTags(tag: string) {
        if (!this.tagsCounter.hasOwnProperty(tag)) {
            this.tagsCounter[tag] = 0;
        }
        this.activeTags = Array.from(new Set([...this.activeTags, tag]));
    }

    private updateActiveTags() {
        this.maxPhotos = this.camera.getConfig()?.photoCount ?? this.maxPhotos;
        Object.keys(this.tagsCounter).forEach((key) => {
            this.tagsCounter[key] += 1;
            if (this.tagsCounter[key] > this.maxPhotos) {
                this.activeTags = this.activeTags.filter((tag) => tag !== key)
                delete this.tagsCounter[key];
            }
        })
    }

}
