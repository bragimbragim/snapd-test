import {Service} from 'typedi';
import {ReaderService} from '../ReaderService';
import {Camera} from '../../lib/RaspiCam';
import fs from "fs";
import {v4 as uuidv4} from 'uuid';
import {CheckPhotoRes} from '../../../globalTypes/CheckPhotosRes';
import {EmitterEvents} from '../../types/EmitterEvents';
import {errorLogger} from '../../utils/logger';

@Service()
export class CalibratePhotoModeService {

    public photos: CheckPhotoRes[] = [];
    public startTime: number = null;
    private busy: boolean = false;
    public active: boolean = false;
    private readonly listener: () => void;

    constructor(public camera: Camera, public readerService: ReaderService) {
        this.listener = this.stopEventHandler.bind(this);
        this.readerService.on(EmitterEvents.READER.STOP, this.listener);
    }

    public makePhoto() {
        if (!this.active) return;

        if (this.busy) return;
        this.busy = true;

        if (this.startTime === null) {
            this.startTime = new Date().getTime();
        }

        this.camera.takeFastImage()
            .then((buffer) => {
                const endTime = new Date().getTime();
                const photoName = uuidv4();
                const path = `photos/check/fast-${photoName}.jpg`;
                fs.writeFileSync(path, buffer);
                this.photos.push({
                    path,
                    time: (endTime - this.startTime) / 1000
                });
            })
            .catch((err) => {
                errorLogger.error(err.message, {event: 'FastCameraService: makePhoto().catch'})
            })
            .finally(() => {
                this.busy = false;
                this.makePhoto();
            });
    }

    public clearTempPhotos() {
        this.photos = [];
        this.startTime = null;
    }

    public stopEventHandler() {
        this.active = false;
    }
}
