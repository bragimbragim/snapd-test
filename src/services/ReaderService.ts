import {MqttService} from './MqttService';
import {Service} from 'typedi';
import {MqttTopic} from '../types/MqttTopics';
import {ReaderControlCommand} from '../../globalTypes/ReaderControlCommand';
import {initReaderStatus, ReaderStatus, ReaderStatusCode} from '../../globalTypes/ReaderStatus';
import {ChildProcess, spawn} from "child_process";
import {IReaderConfig} from '../../globalTypes/IReaderConfig';
import {ReaderMode} from '../../globalTypes/ReaderMode';
import {HandleTagsService} from './HandleTagsService';
import {EmitterEvents} from '../types/EmitterEvents'
import {ConfigService} from './ConfigService';
import {errorLogger} from '../utils/logger';
import {Camera} from '../lib/RaspiCam';
import {killAllProcesses} from '../utils/killAllProcesses';

const colors = require('colors/safe');

@Service()
export class ReaderService extends HandleTagsService {

    private readonly INIT_READER_TIMEOUT_MS = 20000;
    private readonly PING_INTERVAL_MS = 4000;
    private readonly PING_MAX_ATTEMPT = 5;
    private pingAttemptCount = 0;
    private snapdrfid: ChildProcess;
    private pingInterval;
    private currStatus: ReaderStatus = initReaderStatus;

    constructor(
        private mqttService: MqttService,
        private configService: ConfigService,
        private camera: Camera
    ) {
        super();
        this.registerReaderDataListener();
        this.on(EmitterEvents.CATCHER.FATAL_ERROR, () => {
            this.stopReading({
                code: ReaderStatusCode.GLOBAL_ERROR,
                error: 'Catcher Error'
            });
            this.stopCatcher();
        });
        this.mqttService.on(EmitterEvents.MQTT.ERROR, () => {
            this.destroyReader();
        })
    }

    /**
     * Initialization for C snapdrfid App.
     *
     * snapdrfid - reader C app process instance
     *
     * Destroy snapdrfid if process exist
     * Listen MQTT reader status topic and EventEmitter status topic
     * Spawn command for start C snapdrfid App.
     * If MQTT reader status topic got value, then check value:
     * 1) value = GLOBAL_ERROR. Sets Error and break;
     * 2) value = GLOBAL_SUCCESS. Start catcher, autostart from config
     *
     * Sets spawn error handler (the error can appear when the server is not running on the raspberry pi)
     *  1) If has Error, then emit GLOBAL_ERROR and reject with error
     *
     * Sets SetTimeout with @INIT_READER_TIMEOUT
     * After Timeout End, emit NO_ANSWER event and reject with Error 'RFID App Timeout End'.
     */
    public async init(): Promise<ReaderStatus | Error> {
        console.log(colors.underline.green('\nReader: Init'));
        const currConfig = this.configService.getCurrConfig<IReaderConfig>('reader');

        await killAllProcesses('/home/pi/projects/snapdrfid/snapdrfid')
            .catch((err) => {
                this.updateStatus({code: ReaderStatusCode.GLOBAL_ERROR, error: err.message});
                return Promise.reject(err);
            })

        try {
            return await new Promise((resolve, reject) => {
                this.destroyReader();

                this.mqttService.once(MqttTopic.READER_STATUS, (status: ReaderStatusCode) => {
                    this.removeAllListeners(MqttTopic.READER_STATUS);
                    switch (status) {
                        case ReaderStatusCode.GLOBAL_ERROR:
                            this.updateStatus(
                                {
                                    code: ReaderStatusCode.GLOBAL_ERROR,
                                    error: 'Reader internal error'
                                }
                            );
                            errorLogger.error('Reader internal error', {event: 'ReaderService: init()'})
                            return reject(new Error(this.currStatus.error));

                        case ReaderStatusCode.GLOBAL_SUCCESS:
                            this.startCatcher();
                            this.updateStatus({code: ReaderStatusCode.INITIALIZED});
                            if (currConfig.autoStart) {
                                this.startReading();
                            }
                            return resolve(this.currStatus);
                    }
                });

                this.once(MqttTopic.READER_STATUS, (code: ReaderStatusCode) => {
                    this.mqttService.removeAllListeners(MqttTopic.READER_STATUS);
                    switch (code) {
                        case ReaderStatusCode.NO_ANSWER:
                            this.updateStatus({
                                code: ReaderStatusCode.GLOBAL_ERROR,
                                error: 'RFID App Timeout End'
                            });
                            return reject(new Error('RFID App Timeout End'));

                        case ReaderStatusCode.GLOBAL_ERROR:
                            errorLogger.error(this.currStatus.error, {event: 'ReaderService: init()'})
                            return reject(new Error(this.currStatus.error));
                    }
                });

                this.snapdrfid = spawn(
                    '/home/pi/projects/snapdrfid/snapdrfid',
                    {stdio: 'inherit'}
                );

                this.snapdrfid.on('error', (err: any) => {
                    let error = (err.code === 'ENOENT')
                                ? 'Reader App not found'
                                : 'Reader internal Error';

                    this.updateStatus({code: ReaderStatusCode.GLOBAL_ERROR, error});

                    this.emit(MqttTopic.READER_STATUS, ReaderStatusCode.GLOBAL_ERROR);
                });

                setTimeout(() => {
                    this.emit(MqttTopic.READER_STATUS, ReaderStatusCode.NO_ANSWER);
                }, this.INIT_READER_TIMEOUT_MS);
            });
        } catch (err) {
            errorLogger.error(err.message, {event: 'ReaderService: init()'})
            throw new Error(err.message);
        }
    }

    public async startReading(cameraMode = 'standard') {

        if (cameraMode === 'calibrate' || this.currStatus.mode === ReaderMode.DETECT_ONLY) {
            this.emit(EmitterEvents.READER.START);
            this.mqttService.sendMessage(MqttTopic.READER_CONTROL, ReaderControlCommand.START_READ);
            this.updateStatus({code: ReaderStatusCode.ACTIVE});
            this.startPing();
            return;
        }

        await this.camera.open()
            .then(() => {
                this.emit(EmitterEvents.READER.START);
                this.mqttService.sendMessage(MqttTopic.READER_CONTROL, ReaderControlCommand.START_READ);
                this.updateStatus({code: ReaderStatusCode.ACTIVE});
                this.startPing();
                return Promise.resolve();
            })
            .catch((err) => Promise.reject(err))
    }

    public stopReading(
        status: ReaderStatus = {
            code: ReaderStatusCode.INACTIVE,
            error: null
        }
    ) {
        clearInterval(this.pingInterval);
        this.emit(EmitterEvents.READER.STOP);
        this.camera.close();
        this.mqttService.sendMessage(MqttTopic.READER_CONTROL, ReaderControlCommand.STOP_READ);
        this.updateStatus(status);
    }

    private startPing() {
        clearInterval(this.pingInterval);
        this.pingAttemptCount = 0;
        this.pingInterval = setInterval(() => {
            this.ping();
        }, this.PING_INTERVAL_MS);
    }

    private ping() {
        let timeout;
        this.mqttService.once(MqttTopic.READER_STATUS, (code: ReaderStatusCode) => {
            clearTimeout(timeout);
            this.removeAllListeners(MqttTopic.READER_STATUS);
            switch (code) {
                case ReaderStatusCode.INACTIVE:
                    this.updateStatus({code});
                    this.emit(EmitterEvents.READER.STOP);
                    clearInterval(this.pingInterval);
                    break;
                case ReaderStatusCode.ACTIVE:
                    this.pingAttemptCount = 0;
            }
        });

        this.once(MqttTopic.READER_STATUS, (code: ReaderStatusCode) => {
            this.mqttService.removeAllListeners(MqttTopic.READER_STATUS);
            switch (code) {
                case ReaderStatusCode.NO_ANSWER:
                    this.pingAttemptCount += 1;
                    if (this.pingAttemptCount < this.PING_MAX_ATTEMPT) {
                        return;
                    }
                    this.stopReading({
                        code: ReaderStatusCode.GLOBAL_ERROR,
                        error: 'Ping: No answer'
                    });
                    clearInterval(this.pingInterval);
                    break;
            }
        })

        this.mqttService.sendMessage(MqttTopic.READER_CONTROL, ReaderControlCommand.CHECK_ALIVE);

        timeout = setTimeout(() => {
            this.emit(MqttTopic.READER_STATUS, ReaderStatusCode.NO_ANSWER);
        }, 3000);
    }

    public setOptions(options: IReaderConfig) {
        const optionsForReader = {
            ...options,
            rfPower: options.rfPower * 100,
            Q: {
                type: options.Q,
                value: Number(options.staticQ) + 1
            }
        };
        delete optionsForReader.autoStart;
        delete optionsForReader.staticQ;
        this.mqttService.sendMessage(MqttTopic.READER_OPTIONS, JSON.stringify(optionsForReader))
    }

    public registerReaderDataListener() {
        this.mqttService.on(MqttTopic.READER_DATA, (data) => {
            this.pushNewTag(data);
        });
    }

    public getStatus(): ReaderStatus {
        return this.currStatus;
    }

    public updateStatus(status: ReaderStatus) {
        this.currStatus = {
            ...this.currStatus,
            ...status,
            error: status?.error ?? null
        }
    }

    public destroyReader() {
        if (!this.snapdrfid) {
            return;
        }
        this.snapdrfid.kill();
        this.updateStatus({code: ReaderStatusCode.NOT_INITIALIZED});
        clearInterval(this.pingInterval);
        this.stopCatcher();
        this.emit(EmitterEvents.READER.STOP);
    }
}
