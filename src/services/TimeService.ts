import {Service} from 'typedi';
import {exec} from "child_process";
import {SystemDate} from '../../globalTypes/SystemDate';

const util = require('util');
const execPromisify = util.promisify(exec);

@Service()
export class TimeService {

    constructor() {
    }

    async updateDate({date, time}: SystemDate) {
        await execPromisify(`sudo date +%Y%m%d -s '${date}'`);
        await execPromisify(`sudo date +%R -s '${time}'`);
        await execPromisify('sudo hwclock -w');
    }

    async getTime(): Promise<string> {
        const {stdout} = await execPromisify('sudo date');
        return stdout;
    }

}
