import {Container, Service} from 'typedi';
import {exec} from "child_process";
import {MemoryData} from '../../globalTypes/MemoryData';
import {PhotoService} from './PhotoService';
import {ReaderService} from './ReaderService';
import {ReaderStatusCode} from '../../globalTypes/ReaderStatus';
import {EventEmitter} from 'events';
import {EmitterEvents} from '../types/EmitterEvents';
import {IServiceStatus, ServiceStatusCode} from '../../globalTypes/ServiceStatus';
import {ServiceStatus, StatusDecorator} from '../utils/ServiceStatusDecorator';
import {AppService} from './AppService';

const util = require('util');
const execPromisify = util.promisify(exec);

@Service()
export class MemoryService extends EventEmitter {

    private CHECK_MEMORY_TIMEOUT_MS = 5000;
    private MIN_MEMORY_SIZE = 100000;

    private memoryData: MemoryData;
    private interval;
    private command: string = 'df /home';
    private regexp: RegExp = /\d+/g;

    constructor(
        @StatusDecorator('memory') public serviceStatus: ServiceStatus,
        private photoService: PhotoService,
        private readerService: ReaderService
    ) {
        super();
    }

    public async init(): Promise<IServiceStatus | Error> {
        return new Promise(async (resolve, reject) => {
            try {
                await this.updateStatus();
                const warning = this.getWarning();
                this.startCheckingMemory();
                if (warning) {
                    this.serviceStatus.set({code: ServiceStatusCode.WARNING, caption: warning});
                    return reject(new Error(warning));
                }
                this.serviceStatus.set({code: ServiceStatusCode.SUCCESS, caption: 'Initialized'});
                return resolve(this.serviceStatus.get());
            } catch (err) {
                this.serviceStatus.set({code: ServiceStatusCode.ERROR, caption: err.message});
                return reject(new Error(err.message));
            }
        });
    }

    public async getStatus(): Promise<MemoryData> {
        await this.updateStatus();
        return this.memoryData;
    }

    public getWarning() {
        return this.memoryData?.warning;
    }

    private handleMemory() {

        if (this.memoryData.available < this.MIN_MEMORY_SIZE && this.memoryData?.warning) {
            this.emit(EmitterEvents.MEMORY.DATA, this.memoryData);
            return;
        }

        if (this.memoryData.available < this.MIN_MEMORY_SIZE) {
            this.readerService.stopReading({code: ReaderStatusCode.NOT_INITIALIZED});
            this.memoryData = {...this.memoryData, warning: 'Memory is over'};
            this.serviceStatus.set({code: ServiceStatusCode.WARNING, caption: 'Memory is over'});
            this.emit(EmitterEvents.MEMORY.DATA, this.memoryData);
            return;
        }

        if (this.memoryData.available >= this.MIN_MEMORY_SIZE && this.memoryData?.warning) {
            const dependentModulesIsInit = Container.get(AppService).dependentModulesIsInit();
            console.log('dependentModulesFlag: ', dependentModulesIsInit);
            if (dependentModulesIsInit) {
                this.readerService.updateStatus({code: ReaderStatusCode.INITIALIZED});
            }
            this.serviceStatus.set({code: ServiceStatusCode.SUCCESS, caption: 'Initialized'});
        }

        this.memoryData = {...this.memoryData, warning: null};
        this.emit(EmitterEvents.MEMORY.DATA, this.memoryData);
        return;
    }

    private async updateStatus() {
        const {stdout} = await execPromisify(this.command);
        if (stdout) {
            const result = stdout.match(this.regexp);
            this.memoryData = {
                ...this.memoryData,
                size: Number(result[1]),
                available: Number(result[3])
            }
            this.handleMemory();
        }
    }

    private startCheckingMemory() {
        clearInterval(this.interval);

        this.interval = setInterval(async () => {
            await this.updateStatus();
        }, this.CHECK_MEMORY_TIMEOUT_MS);
    }

}
