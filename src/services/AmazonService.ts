import {Service} from 'typedi';
import {MqttTopic} from '../types/MqttTopics';
import {ChildProcess, spawn} from "child_process";
import {ServiceStatus, StatusDecorator} from '../utils/ServiceStatusDecorator';
import {MqttService} from './MqttService';
import {EventEmitter} from 'events';
import {AmazonStatus, AmazonStatusCode} from '../types/AmazonStatus';
import {AmazonRequestData} from '../types/AmazonRequestData';
import {AmazonControlCommand} from '../../globalTypes/AmazonControlCommand';
import {IAmazonQueueItemStatus} from '../../globalTypes/AmazonQueueItemStatus';
import {ConfigService} from './ConfigService';
import {IAmazonConfig} from '../../globalTypes/IAmazonConfig';
import {ICustomConfig} from './ConfigService';
import {EmitterEvents} from '../types/EmitterEvents';
import {IServiceStatus, ServiceStatusCode} from '../../globalTypes/ServiceStatus';
import {killAllProcesses} from '../utils/killAllProcesses';

const colors = require('colors/safe');

@Service()
export class AmazonService extends EventEmitter {

    private childProcess: ChildProcess;
    private currQueueCount: string = '0';
    private queueList: IAmazonQueueItemStatus[] = [];
    private timeout;
    private config: IAmazonConfig;

    constructor(
        @StatusDecorator('amazon') public serviceStatus: ServiceStatus,
        private configService: ConfigService,
        private mqttService: MqttService
    ) {
        super();
        this.serviceStatus.set({caption: 'Amazon service hasn\'t started yet'});
        this.config = configService.getCurrConfig<IAmazonConfig>('amazon');

        configService.on(EmitterEvents.CONFIG.UPDATE, (config: ICustomConfig) => {
            this.config = config.amazon;
        });
    }

    public async init(): Promise<IServiceStatus | Error> {
        console.log(colors.underline.green('\nAmazon: Init'));
        this.destroyService();

        await killAllProcesses('/home/pi/projects/snapd-amazon/snapd-amazon')
            .catch((err) => {
                this.serviceStatus.set({code: ServiceStatusCode.ERROR, caption: err.message})
                return Promise.reject(err);
            })

        return await new Promise((resolve, reject) => {

            this.mqttService.once(MqttTopic.AMAZON_STATUS, (data: string) => {
                clearTimeout(this.timeout);
                this.removeAllListeners(MqttTopic.AMAZON_STATUS);
                const status: AmazonStatus = JSON.parse(data);

                switch (status.code) {

                    case AmazonStatusCode.SUCCESS:
                        this.serviceStatus.set({
                            code: ServiceStatusCode.SUCCESS,
                            caption: status.caption
                        });
                        this.listenAmazonModule();
                        return resolve(this.serviceStatus.get());

                    case AmazonStatusCode.ERROR:
                        this.serviceStatus.set({
                            code: ServiceStatusCode.ERROR,
                            caption: status.caption
                        })
                        return reject(new Error(status.caption));

                }

            });

            this.once(MqttTopic.AMAZON_STATUS, (status: AmazonStatus) => {
                this.mqttService.removeAllListeners(MqttTopic.AMAZON_STATUS);

                switch (status.code) {

                    // case AmazonStatusCode.NO_ANSWER:
                    //     this.serviceStatus.set({
                    //         code: ServiceStatusCode.ERROR,
                    //         caption: 'Amazon C module no answer'
                    //     })
                    //     return reject(new Error('C module no answer'));

                    case AmazonStatusCode.ERROR:
                        this.serviceStatus.set({
                            code: ServiceStatusCode.ERROR,
                            caption: status.caption
                        });
                        return reject(new Error(status.caption))

                }

            });

            const childProcessArgs: string[] = [
                ...(this.config.debugLocation ? ['--debug'] : []),
                ...(this.config.notSavePhotosInUnit ? ['--delete'] : [])
            ];

            this.childProcess = spawn(
                `/home/pi/projects/snapd-amazon/snapd-amazon`,
                childProcessArgs,
                {stdio: 'inherit'}
            );

            this.childProcess.on('error', (err: any) => {
                let error = (err.code === 'ENOENT') ? 'C App not found' : 'C App internal Error';

                this.emit(MqttTopic.AMAZON_STATUS, {
                    code: AmazonStatusCode.ERROR,
                    caption: error
                });
            });

            // this.timeout = setTimeout(() => {
            //     this.emit(MqttTopic.AMAZON_STATUS, {code: AmazonStatusCode.NO_ANSWER});
            // }, this.INIT_AMAZON_TIMEOUT_MS);
        });
    }

    public getQueueCount(): string {
        return this.currQueueCount;
    }

    public getQueueList(): IAmazonQueueItemStatus[] {
        return this.queueList;
    }

    public clearQueue(): Promise<string | Error> {

        return new Promise((resolve, reject) => {

            this.mqttService.once(MqttTopic.AMAZON_RESPONSE, (answer: string) => {
                this.queueList = this.queueList
                    .filter((item) => item.code === AmazonStatusCode.UPLOADING);
                return resolve('Queue have been successfully clear');
            });

            this.once(MqttTopic.AMAZON_RESPONSE, () => {
                return reject(new Error('Amazon module no answer'));
            });

            this.mqttService.sendMessage(MqttTopic.AMAZON_CONTROL, AmazonControlCommand.CLEAR_QUEUE);

            this.timeout = setTimeout(() => {
                this.emit(MqttTopic.AMAZON_RESPONSE);
            }, 5000);

        });
    }

    public getConfig(): IAmazonConfig {
        return this.config;
    }

    public sendPhotoToAmazon(tags: string[], date: Date, photoName, unitName) {

        if (!this.config.loadPhotosToAmazon) {
            return;
        }

        const formattedDate = this.getFormattedDate(date);
        const requestData: AmazonRequestData = {photoName, tags, unitName, formattedDate}
        this.mqttService.sendMessage(MqttTopic.AMAZON_DATA, JSON.stringify(requestData));
    }

    private listenAmazonModule() {
        this.mqttService.on(MqttTopic.AMAZON_QUEUE, (queue: string) => {
            this.currQueueCount = queue;
        });
        this.mqttService.on(MqttTopic.AMAZON_STATUS, (status: string) => {
            const parsedItem: IAmazonQueueItemStatus = JSON.parse(status);

            if (parsedItem?.photoName) {
                this.addToQueueList(parsedItem);
            }

            switch (parsedItem.code) {

                case AmazonStatusCode.SUCCESS:
                    this.serviceStatus.set({
                        code: ServiceStatusCode.SUCCESS,
                        caption: parsedItem.caption
                    });
                    break;

                case AmazonStatusCode.ERROR:
                    this.serviceStatus.set({
                        code: ServiceStatusCode.ERROR,
                        caption: parsedItem.caption
                    })
                    break;

                case AmazonStatusCode.UPLOADING:
                    this.serviceStatus.set({
                        caption: parsedItem.caption
                    });
            }
        });
    }

    private addToQueueList(newItem: IAmazonQueueItemStatus) {
        const foundItem = this.queueList
            .find((item) => item.photoName === newItem.photoName);

        if (foundItem) {
            foundItem.caption = newItem.caption;
            foundItem.code = newItem.code;
            return;
        }

        this.queueList.push(newItem);
    }

    private destroyService() {
        this.queueList = [];
        this.mqttService.removeAllListeners(MqttTopic.AMAZON_QUEUE);
        this.mqttService.removeAllListeners(MqttTopic.AMAZON_STATUS);
        if (this.childProcess) {
            this.childProcess.kill();
        }
    }

    private getFormattedDate(date: Date): string {
        const year = String(date.getFullYear());
        const month = String(('0' + (date.getMonth() + 1)).slice(-2));
        const day = String(('0' + date.getDate()).slice(-2));
        const hour = String(date.getHours());
        const minutes = String(('0' + date.getMinutes()).slice(-2));
        const seconds = String(('0' + date.getSeconds()).slice(-2));

        return `${year}-${month}-${day} ${hour}:${minutes}:${seconds}`;
    }
}
