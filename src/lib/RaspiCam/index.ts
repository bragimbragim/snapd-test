import {RaspistillModule} from './RaspistillModule';
import {Service} from 'typedi';
import {FastCamera} from './FastCamera';
import {ICameraConfig} from '../../../globalTypes/ICameraConfig';
import {ConfigService, ICustomConfig} from '../../services/ConfigService';
import {ServiceStatusCode} from '../../../globalTypes/ServiceStatus';
import {ServiceStatus, StatusDecorator} from '../../utils/ServiceStatusDecorator';
import {killAllProcesses} from '../../utils/killAllProcesses';
import {EmitterEvents} from '../../types/EmitterEvents';

const colors = require('colors/safe');

@Service()
export class Camera {

    public busy = false;
    public raspistill: RaspistillModule;
    public fastCamera: FastCamera;
    private config: ICameraConfig;

    constructor(
        @StatusDecorator('camera') public serviceStatus: ServiceStatus,
        private configService: ConfigService
    ) {
        this.raspistill = new RaspistillModule();
        this.fastCamera = new FastCamera();

        this.config = this.configService.getCurrConfig<ICameraConfig>('camera');

        this.configService.on(EmitterEvents.CONFIG.UPDATE, (config: ICustomConfig) => {
            this.config = config.camera;
        });
    }

    public async init() {
        console.log(colors.underline.green('\nCamera: Init'));

        return new Promise(async (resolve, reject) => {
            this.close();

            await killAllProcesses('raspistill')
                .catch((err) => {
                    this.serviceStatus.set({code: ServiceStatusCode.ERROR, caption: err.message});
                    return reject(new Error(err.message));
                })

            this.raspistill.updateOptions();
            this.busy = this.config.disabled;

            await this.raspistill.openCamera()
                .then(() => {
                    this.raspistill.stopCamera();
                    this.serviceStatus.set({code: ServiceStatusCode.SUCCESS, caption: 'Initialized'});

                    return resolve(this.serviceStatus.get());
                })
                .catch((err) => {
                    this.serviceStatus.set({code: ServiceStatusCode.ERROR, caption: err.message})
                    return reject(new Error(err.message));
                })
        });

    }

    public open() {
        if (this.busy) {
            return Promise.reject(new Error('Camera busy now'));
        }
        this.busy = true;
        return this.raspistill.openCamera()
            .catch(() => this.busy = false)
    }

    public close() {
        this.raspistill.stopCamera();
        this.busy = false;
    }

    /**
     * STREAM CAMERA
     */
    public takeFastImage() {
        return this.fastCamera.takeImage();
    }

    public async startCalibrateCapture(): Promise<void> {
        if (this.busy) {
            return Promise.reject(new Error('Camera busy now'));
        }
        this.busy = true;
        await this.fastCamera.startCapture()
            .catch(() => this.busy = false)
    }

    public async stopCalibrateCapture() {
        await this.fastCamera.stopCapture();
        this.busy = false;
    }

    public getConfig(): ICameraConfig {
        return this.config;
    }
}
