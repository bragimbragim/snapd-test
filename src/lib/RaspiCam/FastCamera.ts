import StreamCamera, {Codec, SensorMode, StreamOptions} from 'pi-camera-connect/dist/lib/stream-camera';
import {EventEmitter} from 'events';

export class FastCamera extends EventEmitter {

    private streamCamera: StreamCamera;
    private defaultOptions: StreamOptions = {
        codec: Codec.MJPEG,
        width: 1920,
        height: 1080,
        fps: 20,
        sensorMode: SensorMode.Mode1
    }

    constructor() {
        super();
        this.streamCamera = new StreamCamera(this.defaultOptions);
    }

    public async takeImage(): Promise<Buffer> {
        return await this.streamCamera.takeImage();
    }

    public async startCapture() {
        await this.streamCamera.createStream();
        await this.streamCamera.startCapture();
    }

    public async stopCapture() {
        await this.streamCamera.stopCapture();
    }

}
