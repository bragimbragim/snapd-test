import Raspi from './Raspi';
import {ICameraConfig} from '../../../globalTypes/ICameraConfig';
import {v4 as uuidv4} from 'uuid';
import {ConfigService, ICustomConfig} from '../../services/ConfigService';
import {Container} from 'typedi';
import {EmitterEvents} from '../../types/EmitterEvents';
import {ILogEvent} from '../../../globalTypes/ILogEvent';
import {ChildProcess} from 'child_process'
import {ISnapdConfig} from '../../../globalTypes/ISnapdConfig';
import {MqttService} from '../../services/MqttService';
import {MqttTopic} from '../../types/MqttTopics';
import {PhotoResponse, StandardPhotoResponse} from '../../types/PhotoResponse';
import {CameraStatusCode} from '../../types/CameraStatusCode';

const child = require('child_process');

export class RaspistillModule extends Raspi {

    public options: ICameraConfig;
    private mqttService: MqttService;
    private unitName: string;
    private cameraProcess: ChildProcess;
    private ABSOLUTE_PATH = '/home/pi/projects/snapd';
    private saving: boolean = false;

    constructor() {
        super();

        this.mqttService = Container.get(MqttService);
        const configService = Container.get(ConfigService);

        this.unitName = configService.getCurrConfig<ISnapdConfig>('snapd').unitName;

        configService.on(EmitterEvents.CONFIG.UPDATE, (config: ICustomConfig) => {
            this.unitName = config.snapd.unitName;
        });
    }

    public updateOptions() {
        this.options = Container.get(ConfigService).getCurrConfig<ICameraConfig>('camera');
        this.setOptions(this.options);
    }

    // private setAnnotateForPhoto() {
    //     this.spawnArgs.push(
    //         '--annotateex', '120,0x00,0x808000',
    //         '--annotate', this.getOptionsForAnnotate()
    //     )
    // }
    //
    // private getOptionsForAnnotate(): string {
    //     let annotate = '';
    //     const optionsForAnnotate = {...this.options};
    //     delete optionsForAnnotate.disabled;
    //     delete optionsForAnnotate.firstPhotoDelayMs;
    //     delete optionsForAnnotate.noSave;
    //
    //     Object.keys(optionsForAnnotate).forEach((key) => {
    //         annotate += `${key}: ${this.options[key]} `
    //     });
    //     return annotate;
    // }

    public openCamera(): Promise<string | Error> {
        let interval;
        return new Promise((resolve, reject) => {

            this.defaultSpawnArgs = ['-n', '-k', '-bm', '-o', 'test.jpg', '-q', '100', '-t', '0'];
            this.setOptions(this.options);

            this.mqttService.once(MqttTopic.CAMERA_STATUS, (status: CameraStatusCode) => {
                clearInterval(interval);
                this.removeAllListeners(MqttTopic.CAMERA_STATUS);
                switch (status) {
                    case CameraStatusCode.INITIALIZED:
                        return resolve('Camera opened');
                    case CameraStatusCode.ERROR:
                        return reject(new Error('Camera not opened'))
                }

            });

            this.once(MqttTopic.CAMERA_STATUS, () => {
                this.mqttService.removeAllListeners(MqttTopic.CAMERA_STATUS);
                reject(new Error('Camera not opened'))
            });

            this.cameraProcess = child.spawn(
                '/home/pi/projects/snapd-camera/raspistill',
                this.spawnArgs,
                {stdio: 'inherit'}
            );

            this.cameraProcess.once('error', () => {
                reject(new Error('Camera internal error'))
            })

            interval = setTimeout(() => {
                this.emit(MqttTopic.CAMERA_STATUS);
            }, 20000);

        })
    }

    public stopCamera() {
        if (!this.cameraProcess) {
            return;
        }

        if (!this.saving) {
            this.cameraProcess.kill();
            return;
        }

        this.once(EmitterEvents.PHOTO.SAVED, () => {
            this.cameraProcess.kill();
        });
    }

    private async makePhoto(folder): Promise<PhotoResponse | Error> {
        let interval;
        this.saving = true;
        return new Promise(async (resolve, reject) => {

            let currTime: Date;
            const photoTime = new Date();
            const photoName = this.getPhotoName(photoTime);
            const absolutePath = `${this.ABSOLUTE_PATH}/${folder}/${photoName}.jpg`;
            // const absolutePath = `/mnt/${photoName}.jpg`;
            const relativePath = `${folder}/${photoName}.jpg`;
            const data = JSON.stringify({command: 3, filename: absolutePath});

            this.mqttService.once(MqttTopic.CAMERA_STATUS, async (status: CameraStatusCode) => {
                console.log('response from mqtt: ', status);
                this.emit(EmitterEvents.PHOTO.SAVED);
                this.saving = false;
                clearInterval(interval);
                this.removeAllListeners(MqttTopic.CAMERA_STATUS);
                switch (status) {
                    case CameraStatusCode.SUCCESS:
                        return resolve({currTime, photoName, absolutePath, relativePath});
                    case CameraStatusCode.ERROR:
                        return reject(new Error('Camera error'))
                }

            });

            this.once(MqttTopic.CAMERA_STATUS, () => {
                this.emit(EmitterEvents.PHOTO.SAVED);
                this.saving = false;
                this.mqttService.removeAllListeners(MqttTopic.CAMERA_STATUS);
                return reject(new Error('Camera timeout end'));
            })

            console.log(this.spawnArgs.join(','));
            this.mqttService.sendMessage(MqttTopic.CAMERA_CONTROL, data);

            currTime = new Date();

            interval = setTimeout(() => {
                this.emit(MqttTopic.CAMERA_STATUS);
            }, 20000);

        })
    }

    public async takeStandardPhoto(): Promise<StandardPhotoResponse | Error> {
        return await this.makePhoto('photos')
            .then((data: PhotoResponse) => {
                this.throwPhotoEvent(data.currTime);
                return Promise.resolve({...data, unitName: this.unitName});
            })
            .catch((err) => Promise.reject(err))
    }

    public async takeDebugPhoto() {
        return await this.makePhoto('photos/check')
            .then((path: PhotoResponse) => path.relativePath)
            .catch((err) => Promise.reject(err));
    }

    private getPhotoName(currTime: Date): string {
        const timestamp = String(currTime.valueOf());
        const randomPrefix = uuidv4().slice(0, 5);
        return this.unitName + '_' + timestamp + '_' + randomPrefix;
    }

    private throwPhotoEvent(currTime: Date) {
        const data: ILogEvent = {
            type: 'photo',
            timestamp: currTime.valueOf(),
            caption: 'Photo is taken'
        }
        this.emit(EmitterEvents.PHOTO.CREATED, data);
    }
}
