import {EventEmitter} from "events";
import {ICameraConfig} from '../../../globalTypes/ICameraConfig';

export default class Raspi extends EventEmitter {

    public defaultSpawnArgs = [];

    public spawnArgs = [];

    constructor() {
        super();
    }

    setOptions(options: ICameraConfig) {
        this.spawnArgs = [

            // default options for both raspistill and raspivid
            ...this.defaultSpawnArgs,

            ...(options?.width ? ['--width', options.width.toString()] : []),
            ...(options?.height ? ['--height', options.height.toString()] : []),
            ...(options?.exposure ? ['--exposure', options.exposure.toString()] : []),
            ...(options?.shutter ? ['--shutter', options.shutter.toString()] : []),
            ...(options?.mode ? ['--mode', options.mode.toString()] : []),
            ...(options?.ISO ? ['--ISO', options.ISO.toString()] : []),
            ...(options?.brightness ? ['--brightness', options.brightness.toString()] : []),
            ...(options?.saturation ? ['--saturation', options.saturation.toString()] : []),
            ...(options?.sharpness ? ['--sharpness', options.framerate.toString()] : []),
            ...(options?.contrast ? ['--contrast', options.contrast.toString()] : []),
            ...(options?.flicker ? ['--flicker', options.flicker.toString()] : []),
            ...(options?.ev ? ['--ev', options.contrast.toString()] : []),
        ];
    }

}
