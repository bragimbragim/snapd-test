import {EntityRepository, Repository, getRepository} from 'typeorm';
import {Photo} from '../entity/Photo';
import {Tag} from '../entity/Tag';
import {IPhoto} from '../../client/shared/types/IPhoto';

@EntityRepository(Photo)
export class PhotoRepository extends Repository<Photo> {

    public async getAllPhotosForUser(userId: number) {

        const photos = await getRepository(Tag)
            .createQueryBuilder('tag')
            .leftJoinAndSelect('tag.user', 'user')
            .leftJoinAndSelect('tag.photo', 'photo')
            .where('photo.photoName is not null')
            .andWhere('user.id = :id', {id: userId})
            .select(['photo.id', 'photo.time', 'photo.path', 'tag.name', 'user.email'])
            .orderBy('photo.time', 'DESC')
            .getRawMany();

        return photos
            .map((photo) => {
                return {
                    ...this.getDefaultFields(photo),
                    tagName: photo.tag_name,
                    userEmail: photo.user_email
                }
            }) || [];
    }

    public async getAllPhotosForAdmin(): Promise<IPhoto[]> {
        const allPhotos = await getRepository(Tag)
            .createQueryBuilder('tag')
            .leftJoinAndSelect('tag.photo', 'photo')
            .where('photo.photoName is not null')
            .select(['photo.id', 'photo.time', 'photo.path', 'tag.name'])
            .orderBy('photo.time', 'DESC')
            .getRawMany();

        return allPhotos
            .reduce((acc, curr) => {
                const foundPhoto = acc.find((item) => item.photo_path === curr.photo_path);
                if (!foundPhoto) {
                    acc.push(curr);
                    return acc;
                }
                foundPhoto.tag_name += '; ' + curr.tag_name;
                return acc;
            }, [])
            .map((photo) => {
                return {
                    ...this.getDefaultFields(photo),
                    tagName: photo.tag_name
                }
            }) || [];
    }

    private getDefaultFields(photo: any) {
        return {
            id: photo.photo_id,
            date: photo.photo_time,
            path: photo.photo_path
        }
    }
}
