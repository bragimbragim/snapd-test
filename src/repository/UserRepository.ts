import {EntityRepository, Repository} from 'typeorm';
import {User} from '../entity/User';
import concatTag from '../utils/concatTag';

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    public findByMailOrTag(data: string) {
        return this
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.tags', 'tags')
            .where(
                'user.email = :login OR tags.name = :tag',
                {login: data, tag: concatTag(data)}
            )
            .getOne();
    }

    public findByMail(email: string) {
        return this.findOne({where: {email}});
    }
}
