import {UserRoles} from './UserRoles';

export interface IUser {
    id: number;
    email: string;
    role: UserRoles;
}
