export interface ISignUpRequestBody {
    password: string,
    email: string,
    tagId: string
}
