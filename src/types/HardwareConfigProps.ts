export const HardwareConfigProps = {
    CAMERA: 'camera',
    READER: 'reader',
    SNAPD: 'snapd',
    AMAZON: 'amazon'
} as const;

type Keys = keyof typeof HardwareConfigProps;
export type HardwareConfigPropsType = typeof HardwareConfigProps[Keys];
