export interface AmazonRequestData {
    photoName: string;
    tags: string[];
    formattedDate: string;
    unitName: string;
}
