export interface ISignInRequestBody {
    password: string,
    login: string
}
