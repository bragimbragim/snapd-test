export interface TakeImageOptions {
    check: boolean;
    saveCheck: boolean;
    annotate?: boolean;
    path?: string;
}
