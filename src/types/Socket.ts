import {Socket} from 'socket.io-client';
import {MemoryData} from '../../globalTypes/MemoryData';

export interface ReaderSocket extends Socket {
    listener: (socket: Socket, data: any) => void;
}

export interface LogSocket extends Socket {
    appearTagListener: (socket: Socket, data: any) => void;
    looseTagListener: (socket: Socket, data: any) => void;
    photoCreated: (socket: Socket) => void;
}

export interface MemorySocket extends Socket {
    memoryDataListener: (socket: Socket, data: MemoryData) => void;
}
