export interface PhotoResponse {
    currTime: Date;
    photoName: string;
    absolutePath: string;
    relativePath: string;
}

export interface StandardPhotoResponse extends PhotoResponse {
    unitName: string;
}
