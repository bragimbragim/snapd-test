export enum CameraStatusCode {
    INITIALIZED = '1',
    SUCCESS = '2',
    ERROR = '3'
}
