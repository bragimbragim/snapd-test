export enum MqttTopic {
    READER_DATA = '/reader/data',
    READER_CONTROL = '/reader/control',
    READER_OPTIONS = '/reader/options',
    READER_STATUS = '/reader/status',
    AMAZON_DATA = '/amazon/data',
    AMAZON_STATUS = '/amazon/status',
    AMAZON_QUEUE = '/amazon/queue',
    AMAZON_CONTROL = '/amazon/control',
    AMAZON_RESPONSE = '/amazon/response',
    CAMERA_CONTROL = '/camera/control',
    CAMERA_STATUS = '/camera/status',
    SPAMMER = 'tags'
}
