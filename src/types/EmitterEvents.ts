export const EmitterEvents = {
    TAG: {
        APPEAR: '/tag/appear',
        DISAPPEAR: 'tag/disappear',
        All_DISAPPEAR: '/tag/disappear/all'
    },
    READER: {
        START: '/reader/start',
        STOP: '/reader/stop'
    },
    PHOTO: {
        CREATED: '/photo/created',
        SAVED: '/photo/saved'
    },
    CATCHER: {
        FATAL_ERROR: '/tags_catcher/error/fatal'
    },
    MQTT: {
        ERROR: '/mqtt/error'
    },
    CONFIG: {
        UPDATE: '/config/update'
    },
    MEMORY: {
        DATA: '/memory/data'
    }
}
