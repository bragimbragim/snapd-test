export enum AmazonStatusCode {
    SUCCESS = '0',
    ERROR = '1',
    UPLOADING = '2',
    WAITING_FOR_UPLOAD = '3'
}

export interface AmazonStatus {
    code: AmazonStatusCode;
    caption?: string;
}
