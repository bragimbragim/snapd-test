import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Index} from "typeorm";

@Entity()
export class Log extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    entity: 'Tag' | 'Photo';

    @Column()
    event: 'appear' | 'disappear' | 'created';

    @Column()
    name: string;

    @Index('time-idx')
    @Column({type: 'datetime'})
    time: Date;

}
