import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Index} from "typeorm";
import {Tag} from './Tag';

@Entity()
export class Time extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Index('timeDetect-idx')
    @Column({nullable: false, unique: false, type: 'datetime'})
    timeDetect: number;

    @Index('timeLoose-idx')
    @Column({nullable: true, unique: false, type: 'datetime'})
    timeLoose: number;

    @ManyToOne(() => Tag, tag => tag.photo, {eager: true})
    tag: Tag;

}
