import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne} from "typeorm";
import {Tag} from './Tag';

@Entity()
export class Photo extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    photoName: string;

    @Column()
    path: string;

    @Column({type: 'datetime'})
    time: number;

    @ManyToOne(() => Tag, tag => tag.photo, {eager: true})
    tag: Tag;
}
