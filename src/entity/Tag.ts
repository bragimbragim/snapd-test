import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, OneToMany, Index} from "typeorm";
import {User} from './User';
import {Time} from './Time';
import {Photo} from './Photo';

@Entity()
export class Tag extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Index('name-idx')
    @Column({nullable: false, unique: true})
    name: string;

    @OneToMany(() => Time, time => time.tag)
    time: Time;

    @OneToMany(() => Photo, photo => photo.tag)
    photo: Photo;

    @ManyToOne(() => User, user => user.tags)
    user: User;

}
