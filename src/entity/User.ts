import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, OneToMany} from "typeorm";
import {IUser} from '../types/IUser';
import {Tag} from './Tag';
import {UserRoles} from '../types/UserRoles';

@Entity()
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    email: string;

    @Column({nullable: false})
    password: string;

    @Column({nullable: false})
    role: UserRoles;

    @OneToMany(() => Tag, tag => tag.user, {cascade: ['remove', 'update']})
    tags: Tag[];

    public get loginData(): IUser {
        return {
            id: this.id,
            email: this.email,
            role: this.role
        }
    }

}
