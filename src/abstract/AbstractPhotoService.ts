import {EmitterEvents} from '../types/EmitterEvents';
import {ReaderService} from '../services/ReaderService';
import {ReaderStatusCode} from '../../globalTypes/ReaderStatus';
import {Photo} from '../entity/Photo';
import {Tag} from '../entity/Tag';
import {createQueryBuilder} from 'typeorm';

interface saveData {
    activeTags: string[];
    relativePath: string;
    photoName: string;
    currTime: Date;
}

export abstract class AbstractPhotoService {

    public readerStopped: boolean = false;

    protected constructor(protected readerService: ReaderService) {

        this.readerService.on(EmitterEvents.READER.START, () => {
            this.readerStopped = false;
        });

        this.readerService.on(EmitterEvents.READER.STOP, () => {
            this.readerStopped = true;
        });

    }

    stopWithError() {
        this.readerService.stopReading({
            code: ReaderStatusCode.GLOBAL_ERROR,
            error: 'Camera Error'
        });
    }

    public async savePhotoToDB(props: saveData) {
        const {activeTags, currTime, photoName, relativePath} = props;
        const timestamp = currTime.valueOf();

        const promises = activeTags.map(async (tag) => {

            let foundTag = await Tag.findOne({name: tag})
            const photo = new Photo();

            photo.path = relativePath;
            photo.photoName = photoName;
            photo.time = timestamp;
            photo.tag = foundTag;

            return photo;
        });

        return await Promise.all(promises)
            .then((entities) => {
                return createQueryBuilder()
                    .insert().into(Photo)
                    .values(entities)
                    .execute()
            })
            .catch((err) => {
                console.log('mapping err: ', err.message);
            })
    }
}
