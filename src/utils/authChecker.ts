import {Action} from 'routing-controllers';
import {UserRoles} from '../types/UserRoles';

export default async function (action: Action, roles: UserRoles[]) {
    const session = action.request.session;

    if (!session?.userId) return false;
    if (!roles.includes(session?.role)) return false;

    return true;
}
