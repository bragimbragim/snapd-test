const maxNumbers = 24;

export default function (tag: string): string {
    const tagLength = tag.length;
    let result = '';

    for (let i = 0; i < maxNumbers - tagLength; i++) {
        result += '0';
    }

    return result + tag;
}
