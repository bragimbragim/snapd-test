import winston, {format} from 'winston';
import WinstonDailyRotate from 'winston-daily-rotate-file';

const {combine, timestamp, printf, metadata} = format;

const loggerFormat = printf((
    {
        level,
        timestamp,
        message,
        metadata
    }
) => {
    return `[${level}] ${timestamp} [${metadata.event}]: ${message}`;
});

const infoLogger = winston.createLogger({
    format: combine(
        metadata(),
        timestamp(),
        loggerFormat
    ),
    transports: [
        new WinstonDailyRotate({
            dirname: './src/logs/events',
            filename: 'events-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            maxSize: '20m',
            maxFiles: '15d',
            level: 'info'
        })
    ]
});

const errorLogger = winston.createLogger({
    format: combine(
        metadata(),
        timestamp(),
        loggerFormat
    ),
    transports: [
        new WinstonDailyRotate({
            dirname: './src/logs/errors',
            filename: 'errors-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            maxSize: '20m',
            maxFiles: '15d',
            level: 'error'
        })
    ]
});

export {infoLogger, errorLogger};
