import {exec, ExecException} from 'child_process';

const colors = require('colors/safe');

type ProcessName =
    | 'raspistill'
    | '/home/pi/projects/snapdrfid/snapdrfid'
    | '/home/pi/projects/snapd-amazon/snapd-amazon';

interface KillResponse {
    PID: string,
    status: 'success' | 'error'
}

export async function killAllProcesses(process: ProcessName) {

    // return Promise.resolve();

    return new Promise((resolve, reject) => {

        exec(`pidof ${process}`, (error: ExecException, stdout, stderr) => {

            if (error && error.code === 1) {
                console.log(colors.yellow.underline(`no active process for ${process}`));
                return resolve();
            }
            if (error && error.code !== 1) {
                return reject(error);
            }

            if (stdout) {
                const PIDArray = stdout.trim().split(' ');
                const promises = PIDArray.map(async (PID) => await kill(PID));

                return Promise.all(promises)
                    .then((data) => {
                        console.log(getConsoleCaption(process, data));

                        if (data.some((response) => response.status === 'error')) {
                            return reject(new Error('Not all processes were killed'));
                        }

                        return resolve();
                    })
                    .catch((err) => reject(err))
            }

            return resolve();
        })

    });
}

async function kill(PID): Promise<KillResponse> {
    return new Promise((resolve) => {
        exec(`kill ${PID}`, (error: ExecException, stdout, stderr) => {
            if (error) {
                return resolve({PID, status: 'error'});
            }

            return resolve({PID, status: 'success'});
        });
    });
}

function getConsoleCaption(processName: ProcessName, data: KillResponse[]) {
    const title = colors.yellow.underline(`killed processes for ${processName}:\n`);

    const status = data.map((response) => {
        if (response.status === 'success') {
            return colors.green(`PID: ${response.PID}, status: ${response.status}`)
        }
        return colors.red(`PID: ${response.PID}, status: ${response.status}`)
    })

    return `${title}${status.join('\n')}\n`
}
