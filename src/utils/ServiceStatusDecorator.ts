import {Container} from 'typedi';
import {initServiceStatus, IServiceStatus} from '../../globalTypes/ServiceStatus';

export class ServiceStatus {

    private serviceName: string;
    private status: IServiceStatus = initServiceStatus;

    constructor(serviceName: string) {
        this.serviceName = serviceName;
    }

    set(status: IServiceStatus) {
        this.status = {...this.status, ...status};
    }

    get() {
        return this.status;
    }

}

export function StatusDecorator(serviceName) {
    return function (object: Object, propertyName: string, index?: number) {
        const logger = new ServiceStatus(serviceName);
        Container.registerHandler({object, propertyName, index, value: containerInstance => logger});
    };
}
