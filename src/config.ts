import {v4 as uuidv4} from 'uuid';

export const config = {
    port: 3000,
    session: {
        genid: () => uuidv4(),
        secret: '3dCE84rey8RHz',
        resave: false,
        saveUninitialized: false,
        cookie: {
            expires: 24 * 60 * 60 * 1000,
            secure: false,
            httpOnly: true
        }
    },
    typeORMStore: {
        cleanupLimit: 2,
        ttl: 86400
    },
    socketIO: {
        cors: {origin: '*'}
    },
    cors: {
        credentials: true,
        origin: 'http://localhost:4200',
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE"
    }
}
