import {Body, Get, JsonController, Post, Res, Session} from 'routing-controllers';
import {User} from '../entity/User';
import {getCustomRepository} from 'typeorm';
import {ISignInRequestBody} from '../types/ISignInRequestBody';
import {ISignUpRequestBody} from '../types/ISignUpRequestBody';
import {UserRepository} from '../repository/UserRepository';
import {Tag} from '../entity/Tag';
import {UserRoles} from '../types/UserRoles';
import concatTag from '../utils/concatTag';
import {errorLogger} from '../utils/logger';

const bcrypt = require('bcrypt');

@JsonController('/api/auth')
export class AuthController {

    @Post('/signin')
    async signIn(
        @Session() session: any,
        @Body() body: ISignInRequestBody,
        @Res() response: any
    ) {
        try {
            const {password, login} = body;

            const user = await getCustomRepository(UserRepository)
                .findByMailOrTag(login);

            if (!user) {
                return response.status(400).send('Invalid login or Email');
            }

            const match = await bcrypt.compare(password, user.password);

            if (!match) {
                return response.status(400).send('Invalid login or Email');
            }

            session.userId = user.id;
            session.role = user.role;

            return response.json({user: user.loginData});

        } catch (e) {
            errorLogger.error(e.message, {event: 'AuthController: singIn()'})
            return response.status(500).send('Internal server Error');
        }

    }

    @Post('/signup')
    async signUp(
        @Session() session: Express.Session,
        @Body() body: ISignUpRequestBody,
        @Res() response: any
    ) {
        try {
            let {password, email, tagId} = body;

            // tagId = concatTag(tagId);

            const foundUser = await getCustomRepository(UserRepository).findByMail(email);
            let foundTag = await Tag.findOne({name: tagId});

            if (foundUser || foundTag?.user) {
                return response.status(400).send('User already exist');
            }

            const hash = await bcrypt.hash(password, 10);

            if (foundTag) {
                foundTag.name = tagId;
                await foundTag.save();
            } else {
                foundTag = new Tag();
                foundTag.name = tagId;
                await foundTag.save();
            }

            const user = new User();
            user.password = hash;
            user.email = email;
            user.tags = [foundTag];
            user.role = UserRoles.USER;
            await user.save();

            return response.status(200).send('Successfully registered');

        } catch (e) {
            errorLogger.error(e.message, {event: 'AuthController: singUp()'})
            return response.status(500).send('Internal server Error');
        }
    }

    @Get('/logout')
    async logout(
        @Session() session: Express.Session,
        @Res() response: any
    ) {
        session?.destroy(null);
        return response.end();
    }
}
