import {Get, JsonController} from 'routing-controllers';
import {EntityFromQuery} from 'typeorm-routing-controllers-extensions';
import {User} from '../entity/User';

@JsonController("/api/user")
export class UserController {
    @Get('/users')
    getUser(@EntityFromQuery("id") user: User) {
        return user;
    }

}
