import {Body, Get, JsonController, Post, Res} from 'routing-controllers';
import {Response} from 'express';
import path from 'path';
import {execSync} from 'child_process';
import {errorLogger} from '../utils/logger';
import {ISnapdServices} from '../../globalTypes/ISnapdServices';
import {ConfigService} from '../services/ConfigService';
import {ISnapdConfig} from '../../globalTypes/ISnapdConfig';
import {WifiService} from '../services/WifiService';
import {AppService} from '../services/AppService';

@JsonController()
export class AppController {

    constructor(
        private appService: AppService,
        private configService: ConfigService,
        private wifiSettingsService: WifiService
    ) {
    }

    @Get('/api/services/status')
    getServicesStatus(): ISnapdServices {
        return this.appService.getServicesStatus();
    }

    @Post('/api/reboot')
    reboot(@Res() response: Response) {
        try {
            execSync('sudo reboot');
            return '';
        } catch (e) {
            errorLogger.error(e.message, {event: 'AppController: reboot()'})
            return response.status(500).send('Cannot reboot Raspberrypi');
        }
    }

    @Post('/api/poweroff')
    powerOff(@Res() response: Response) {
        try {
            execSync('sudo poweroff');
            return '';
        } catch (e) {
            errorLogger.error(e.message, {event: 'AppController: powerOff()'})
            return response.status(500).send('Cannot Power Off Raspberrypi');
        }
    }

    @Get('/api/unitname/get')
    getUnitName(): ISnapdConfig {
        return this.configService.getCurrConfig<ISnapdConfig>('snapd');
    }

    @Post('/api/unitname/set')
    async setUnitName(
        @Res() response: Response,
        @Body() body: ISnapdConfig
    ) {
        try {
            await this.wifiSettingsService.updateHostapdFile(body.unitName);
            this.configService.updateCustomConfig<ISnapdConfig>('snapd', {unitName: body.unitName});

            return response.status(200).send('Unit name has been successfully changed');
        } catch (err) {
            return response.status(500).send(err.message);
        }
    }

    @Get("*")
    async getIndex(@Res() response: Response): Promise<any> {

        await new Promise((resolve) => {
            response.sendFile(path.join(__dirname, '../../dist/index.html'), (error) => {
                resolve(error);
            })
        });

        return response;
    }

}
