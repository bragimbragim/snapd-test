import {Get, JsonController, Res} from 'routing-controllers';
import {Response} from 'express';
import request from 'request';

@JsonController('/api/video')
export class VideoModuleController {

    constructor() {
    }

    @Get('/server/data')
    async getShutterData(@Res() response: Response) {
        const promise = new Promise((resolve, reject) => {
            request
                .get('http://localhost:8000/data',  (error, response, body) => {
                    if (error) {
                        reject(error);
                    }
                    if (body) {
                        resolve(body);
                    }
                })
        });

        return promise
            .then((data) => data)
            .catch((err) => {
                return response.status(500).send(err.message);
            })
    }

    @Get('/stream')
    async getVideoStream(@Res() response: Response) {
        return request.get('http://localhost:8000/stream.mjpg');
    }

}
