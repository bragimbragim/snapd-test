import {Get, JsonController, Post, Res} from 'routing-controllers'
import {Response} from 'express';
import {MemoryData} from '../../globalTypes/MemoryData';
import {MemoryService} from '../services/MemoryService';
import {IServiceStatus} from '../../globalTypes/ServiceStatus';


@JsonController('/api/memory')
export class MemoryController {

    constructor(private memoryService: MemoryService) {
    }

    @Get('/data')
    async getMemory(): Promise<MemoryData> {
        return await this.memoryService.getStatus() || null;
    }

    @Post('/reload')
    async reloadMemoryService(@Res() response: Response) {
        return await this.memoryService
            .init()
            .then((data: IServiceStatus) => data)
            .catch((err: Error) => response.status(500).send(err.message))
    }
}
