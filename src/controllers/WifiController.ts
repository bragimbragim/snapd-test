import {Body, Get, JsonController, Post, Res} from 'routing-controllers';
import {Response} from 'express';
import {WifiService} from '../services/WifiService';
import {IWifiRequest} from '../../globalTypes/IWifi';

@JsonController('/api/wifi')
export class WifiController {

    constructor(private wifiService: WifiService) {
    }

    @Get('/status')
    async getNetworkStatus(@Res() response: Response) {
        return await this.wifiService.getCurrentNetwork();
    }

    @Post('/connect/new')
    async connectToNewNetwork(
        @Res() response: Response,
        @Body() body: IWifiRequest
    ) {

        const {ssid, psk} = body;

        if (!psk || !ssid) {
            return response.status(404).send('Invalid data')
        }

        if (psk?.length > 63 || psk?.length < 8) {
            return response.status(404).send('Invalid password length. Must be 8..63')
        }

        const promise = this.wifiService.connectToNewNetwork(ssid, psk);

        return await promise
            .then((data: string) => data)
            .catch((err) => response.status(500).send(err.message))
    }

    @Post('/connect/saved')
    async connectToSavedNetwork(
        @Res() response: Response,
        @Body() body: { ssid: string }
    ) {

        if (!body.ssid) {
            return response.status(404).send('Invalid data');
        }

        const promise = this.wifiService.connectToSavedNetwork(body.ssid);

        return await promise
            .then((data: string) => data)
            .catch((err) => response.status(500).send(err.message))
    }

    @Post('/remove')
    async remove(
        @Res() response: Response,
        @Body() body: { ssid: string }
    ) {

        if (!body?.ssid) {
            return response.status(404).send('Invalid Data');
        }

        try {
            await this.wifiService.removeNetwork(body.ssid);

            return 'The network was successfully deleted'
        } catch (err) {
            return response.status(500).send(err.message);
        }

    }

    @Get('/list')
    async list(@Res() response: Response) {
        return await this.wifiService.getListOfNetworks();
    }

    @Get('/connection/status')
    async getConnectionStatus() {
        return await this.wifiService.getConnectionStatus();
    }

}
