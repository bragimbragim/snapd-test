import {ConnectedSocket, OnConnect, OnDisconnect, SocketController} from 'socket-controllers';
import {Socket} from 'socket.io-client';
import {ChildProcess, spawn} from 'child_process';
import request from 'request';
import {SocketEvent} from '../../../globalTypes/SocketEvent';
import {Camera} from '../../lib/RaspiCam';

@SocketController('/video')
export class VideoStreamController {

    private childProcess: ChildProcess;
    private firstClient: string;

    constructor(private camera: Camera) {
    }

    @OnConnect()
    connection(@ConnectedSocket() socket: Socket) {
        console.log('Client connected on video controller');

        if (!this.firstClient) {
            this.camera.busy = true;
            this.firstClient = socket.id;
            this.startServer()
                .then(() => socket.emit(SocketEvent.VIDEO.START))
                .catch((err: Error) => socket.emit(SocketEvent.VIDEO.ERROR, err.message))
        }
    }

    @OnDisconnect()
    disconnect(@ConnectedSocket() socket: Socket) {
        if (this.childProcess && this.firstClient === socket.id) {
            this.camera.busy = false;
            this.firstClient = null;
            this.childProcess.kill();
        }
        console.log('Client disconnected from video controller');
    }

    private startServer(): Promise<void | Error> {
        const MAX_PING_ATTEMPT = 10;
        let pingCount = 0;
        let interval;

        return new Promise((resolve, reject) => {
            this.childProcess = spawn(
                'python3',
                ['/home/pi/projects/snapd-focus/server2.py'],
                {stdio: 'inherit'}
            );
            this.childProcess.once('error', (err: any) => {
                clearInterval(interval);
                reject(new Error(err.message));
            });

            interval = setInterval(() => {
                if (pingCount > MAX_PING_ATTEMPT) {
                    clearInterval(interval);
                    reject(new Error('Unable to open video server'));
                }
                pingCount += 1;
                request
                    .get('http://localhost:8000/data')
                    .on('response', () => {
                        clearInterval(interval);
                        resolve();
                    })
                    .on('error', function (err) {
                        console.log(err.message);
                    })
            }, 1000);
        });
    }
}
