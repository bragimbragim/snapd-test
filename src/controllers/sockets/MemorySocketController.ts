import {ConnectedSocket, OnConnect, OnDisconnect, SocketController} from 'socket-controllers';
import {EmitterEvents} from '../../types/EmitterEvents';
import {Socket} from 'socket.io-client';
import {SocketEvent} from '../../../globalTypes/SocketEvent';
import {MemoryService} from '../../services/MemoryService';
import {MemoryData} from '../../../globalTypes/MemoryData';
import {LogSocket, MemorySocket} from '../../types/Socket';


@SocketController('/memory')
export class MemorySocketController {

    constructor(private memoryService: MemoryService) {
    }

    protected memoryDataHandler(socket: Socket, data: MemoryData) {
        socket.emit(SocketEvent.MEMORY.DATA, data);
    }

    @OnConnect()
    connection(@ConnectedSocket() socket: MemorySocket) {
        console.log('Client connected to memory controller');

        socket.memoryDataListener = this.memoryDataHandler.bind(undefined, socket);
        this.memoryService.on(EmitterEvents.MEMORY.DATA, socket.memoryDataListener);
    }

    @OnDisconnect()
    disconnect(@ConnectedSocket() socket: MemorySocket) {
        console.log('Client disconnected from memory controller');
        this.memoryService.removeListener(EmitterEvents.MEMORY.DATA, socket.memoryDataListener);
    }

}
