import {
    ConnectedSocket, EmitOnSuccess,
    OnDisconnect,
    OnMessage,
    SocketController
} from 'socket-controllers';
import {ReaderService} from '../../services/ReaderService';
import {EmitterEvents} from '../../types/EmitterEvents';
import {PhotoService} from '../../services/PhotoService';
import {CalibratePhotoModeService} from '../../services/photoModes/CalibratePhotoModeService';
import {Camera} from '../../lib/RaspiCam';
import {Socket} from 'socket.io-client';
import {SocketEvent} from '../../../globalTypes/SocketEvent';
import {errorLogger} from '../../utils/logger';


@SocketController('/timelapse')
export class TimelapseController {

    constructor(
        private camera: Camera,
        private readerService: ReaderService,
        private photoService: PhotoService,
        private fastCameraService: CalibratePhotoModeService
    ) {
    }

    private async startTimelapse() {
        this.fastCameraService.clearTempPhotos();
        this.photoService.updateMode('calibrate');
        await this.camera.startCalibrateCapture();
        this.readerService.startReading('calibrate');
    }

    private async stopTimelapse() {
        this.readerService.stopReading();
        await this.camera.stopCalibrateCapture();
        this.photoService.updateMode('standard');
    }

    @OnMessage(SocketEvent.TIMELAPSE.START)
    async start(@ConnectedSocket() socket: Socket) {
        try {

            await this.startTimelapse();

            this.readerService.once(EmitterEvents.TAG.APPEAR, () => {
                socket.emit(SocketEvent.TIMELAPSE.TAG_DETECTED);
            });

        } catch (err) {
            errorLogger.error(err.message, {event: 'TimelapseController: start()'})
            await this.stopTimelapse();
            socket.emit(SocketEvent.TIMELAPSE.ERROR, err.message);
        }
    }

    @OnMessage(SocketEvent.TIMELAPSE.STOP)
    @EmitOnSuccess(SocketEvent.TIMELAPSE.SUCCESS)
    async stop() {
        await this.stopTimelapse();
        this.fastCameraService.photos.shift();
        return this.fastCameraService.photos;
    }

    @OnDisconnect()
    async disconnect() {
        await this.stopTimelapse();
    }

}
