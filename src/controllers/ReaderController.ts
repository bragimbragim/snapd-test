import {Authorized, Body, Get, JsonController, Post, Res} from 'routing-controllers';
import {Response} from 'express';
import {IReaderConfig} from '../../globalTypes/IReaderConfig';
import {UserRole} from '../../client/shared/types/UserRole';
import {MinDistanceConfig} from '../configs/ReaderConfig';
import {ConfigService} from '../services/ConfigService';
import {ReaderService} from '../services/ReaderService';
import {ReaderStatus, ReaderStatusCode} from '../../globalTypes/ReaderStatus';
import {ReaderMode} from '../../globalTypes/ReaderMode';
import {PhotoService} from '../services/PhotoService';
import {ReaderControlCommand} from '../../globalTypes/ReaderControlCommand';
import {IError} from '../../client/shared/types/IError';
import {errorLogger} from '../utils/logger';
import {AppService} from '../services/AppService';

@JsonController('/api/reader')
export class ReaderController {

    constructor(
        private configService: ConfigService,
        private readerService: ReaderService,
        private appService: AppService,
        private photoService: PhotoService
    ) {
    }

    @Post('/enable')
    async enableReader(
        @Res() response: Response,
        @Body() body: { mode: ReaderMode }
    ): Promise<ReaderStatus | Response<Error>> {

        const {mode} = body;

        const status = this.readerService.getStatus();

        if (status.code === ReaderStatusCode.NOT_INITIALIZED) {
            return status;
        }

        if (status.code === ReaderStatusCode.GLOBAL_ERROR) {
            return status;
        }

        if (mode === status.mode && status.code !== ReaderStatusCode.INITIALIZED) {
            return status;
        }

        try {

            this.readerService.stopReading();
            this.readerService.updateStatus({mode});

            if (mode === ReaderMode.DETECT_ONLY) {
                this.photoService.disable();

                this.readerService.setOptions(MinDistanceConfig);
            }
            if (mode === ReaderMode.MAKE_PHOTO) {
                const currConfig = this.configService.getCurrConfig<IReaderConfig>('reader');
                this.photoService.enable();

                this.readerService.setOptions(currConfig);

                if (currConfig.autoStart) {
                    return await this.readerService.startReading()
                        .then(() => this.readerService.getStatus())
                        .catch((err) => response.status(500).send(err.message))
                }
            }

            return this.readerService.getStatus();
        } catch (err) {
            errorLogger.error(err.message, {event: 'ReaderController: enableReader()'})
            return response.status(500).send('Something was wrong');
        }

    }

    @Post('/reload')
    async reloadReader(@Res() response: Response): Promise<ReaderStatus | Response<Error>> {
        if (!this.appService.dependentModulesIsInit()) {
            return response.status(500).send('Dependent modules not initialized');
        }
        return this.readerService.init()
            .then((reader: ReaderStatus) => reader)
            .catch((err: Error) => {
                errorLogger.error(err.message, {event: 'ReaderController: reloadReader().catch'})
                return {
                    mode: this.readerService.getStatus().mode,
                    code: ReaderStatusCode.GLOBAL_ERROR,
                    error: err.message
                }
            });
    }

    @Post('/toggle')
    async toggleReader(
        @Res() response: Response,
        @Body() body: { control: ReaderControlCommand }
    ): Promise<ReaderStatus | Response<IError>> {
        try {
            const {control} = body;

            if (control === ReaderControlCommand.START_READ) {
                return await this.readerService.startReading()
                    .then(() => this.readerService.getStatus())
                    .catch((err) => {
                        console.log(err);
                        return response.status(500).send(err.message)
                    })
            }
            if (control === ReaderControlCommand.STOP_READ) {
                this.readerService.stopReading();
            }

            return this.readerService.getStatus();
        } catch (err) {
            errorLogger.error(err.message, {event: 'ReaderController: toggleReader()'})
            return response.status(500).send('Something was wrong')
        }
    }

    @Get('/status')
    getStatus(): ReaderStatus {
        return this.readerService.getStatus();
    }

    @Get('/options')
    @Authorized([UserRole.ADMIN])
    getOptions(@Res() response: Response): IReaderConfig {
        return this.configService
            .getCurrConfig<IReaderConfig>('reader');
    }

    @Post('/options')
    @Authorized([UserRole.ADMIN])
    setOptions(
        @Res() response: Response,
        @Body() body: IReaderConfig
    ) {
        try {
            const {code} = this.readerService.getStatus();
            let stopped: boolean = false;
            if (code === ReaderStatusCode.ACTIVE) {
                this.readerService.stopReading();
                stopped = true;
            }
            this.readerService.setOptions(body);
            this.configService.updateCustomConfig<IReaderConfig>('reader', body);

            if (stopped) {
                this.readerService.startReading();
            }

            return 'Reader settings have been successfully changed';
        } catch (err) {
            errorLogger.error(err.message, {event: 'ReaderController: setOptions()'})
            return response.status(500).send('Something was wrong');
        }
    }
}
