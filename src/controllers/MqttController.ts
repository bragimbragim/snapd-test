import {JsonController, Post, Res} from 'routing-controllers'
import {Response} from 'express';
import {IServiceStatus} from '../../globalTypes/ServiceStatus';
import {MqttService} from '../services/MqttService';

@JsonController('/api/mqtt')
export class MqttController {

    constructor(private mqttService: MqttService) {
    }

    @Post('/reload')
    async reloadMqttService(@Res() response: Response) {
        return await this.mqttService
            .init()
            .then((data: IServiceStatus) => data)
            .catch((err: Error) => response.status(500).send(err.message))
    }
}
