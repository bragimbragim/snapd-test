import {Authorized, Body, Get, Header, JsonController, Post, Res, Session} from 'routing-controllers'
import {UserRole} from '../../client/shared/types/UserRole';
import {Response} from 'express';
import {Photo} from '../entity/Photo';
import path from 'path';
import {EntityFromParam} from 'typeorm-routing-controllers-extensions';
import {getCustomRepository, getRepository} from 'typeorm';
import {UserRepository} from '../repository/UserRepository';
import {PhotoRepository} from '../repository/PhotoRepository';
import * as fs from 'fs';
import * as util from 'util';
import {IPhoto} from '../../client/shared/types/IPhoto';
import JSZip from 'jszip';
import {errorLogger} from '../utils/logger';

const readdir = util.promisify(fs.readdir);
const unlink = util.promisify(fs.unlink);
const imageThumbnail = require('image-thumbnail');


@JsonController('/api/gallery')
export class GalleryController {


    @Get('/photo/:id')
    @Header("Content-Type", "image/jpg")
    async getPhoto(
        @Res() response: Response,
        @EntityFromParam('id') photo: Photo
    ): Promise<Response> {

        await new Promise((resolve) => {
            response.sendFile(
                path.resolve(photo.path),
                (error) => resolve(error)
            );
        });

        return response;
    }


    @Get('/photo/:id/thumbnail')
    @Header("Content-Type", "image/jpg")
    async getPhotoThumbnail(
        @Res() response: Response,
        @EntityFromParam('id') photo: Photo
    ) {

        try {
            let options = {
                percentage: 25,
                jpegOptions: {force: true, quality: 20}
            }
            return await imageThumbnail(photo.path, options);
        } catch (err) {
            errorLogger.error(err.message, {event: 'GalleryController: getPhotoThumbnail()'})
            return response
                .status(500)
                .send('Can\'t load a Thumbnail for image');
        }
    }


    @Post('/photos/download')
    @Header("Content-Type", "application/json")
    async downloadPhotos(
        @Res() response: Response,
        @Body() body: { photos: IPhoto[] }
    ) {
        const zip = new JSZip();

        const {photos} = body;

        for (let i = 0; i < photos.length; i++) {
            const photo = fs.readFileSync(photos[i].path);
            const createdDate = new Date(photos[i].date);
            const fileName = createdDate.toLocaleDateString() + ' ' + createdDate.toLocaleTimeString();
            zip.file(
                fileName + '.jpg',
                photo,
                {createFolders: false, date: createdDate}
            );
        }
        return zip.generateAsync({type: 'nodebuffer'});
    }


    @Get('/photos')
    @Authorized([UserRole.USER])
    async getPhotos(
        @Res() response: Response,
        @Session() session: any
    ): Promise<IPhoto[]> {
        const photoRepository = getCustomRepository(PhotoRepository);
        return await photoRepository.getAllPhotosForUser(session.userId);
    }


    @Get('/photos/all')
    @Authorized([UserRole.ADMIN])
    async getAllPhotos(@Res() response: Response) {
        const photoRepository = getCustomRepository(PhotoRepository);
        return await photoRepository.getAllPhotosForAdmin();
    }


    @Post('/photos/delete')
    @Authorized([UserRole.ADMIN])
    async clearPhotos(
        @Res() response: Response,
        @Body() body: { photos: IPhoto[] }
    ) {
        const {photos} = body;

        try {
            const photosIds = photos.map((photo) => photo.id);
            await getRepository(Photo).delete(photosIds);

            const unlinkPromises = photos.map((photo) => unlink(photo.path));

            return Promise.all(unlinkPromises)
                .then(() => 'Photos were successfully deleted')
                .catch((err) => {
                    errorLogger.error(err.message, {event: 'GalleryController: clearAllPhotos().catch'})
                    return response.status(200).send('Photos were deleted only from database');
                });

        } catch (err) {
            errorLogger.error(err.message, {event: 'GalleryController: clearAllPhotos()'})
            return response.status(500).send('Internal server Error');
        }
    }


    @Post('/photos/delete/all')
    @Authorized([UserRole.ADMIN])
    async clearAllPhotos(@Res() response: Response) {

        try {
            await getRepository(Photo).clear();
            return this.clearPhotosDir('photos')
                .then(() => 'Photos were successfully deleted')
                .catch((err) => {
                    errorLogger.error(err.message, {event: 'GalleryController: clearAllPhotos().catch'})
                    return response.status(500).send(err.message)
                });
        } catch (err) {
            errorLogger.error(err.message, {event: 'GalleryController: clearAllPhotos()'})
            return response.status(500).send('Internal server Error');
        }
    }


    @Post('/photo/check/clear')
    @Authorized([UserRole.ADMIN])
    async clearCheckPhotos(@Res() response: Response) {
        try {
            return this.clearPhotosDir('photos/check')
                .then(() => 'Test photos were successfully deleted')
                .catch((err) => {
                    errorLogger.error(err.message, {event: 'GalleryController: clearCheckPhotosDir().catch'})
                    return response.status(500).send(err.message)
                });
        } catch (err) {
            errorLogger.error(err.message, {event: 'GalleryController: clearCheckPhotosDir()'})
            return response.status(500).send(err.message);
        }
    }


    private async clearPhotosDir(path: string): Promise<void[]> {
        const files = await readdir(path, {withFileTypes: true});
        const unlinkPromises = files
            .filter(file => file.isFile() && file.name !== '.gitignore')
            .map(file => unlink(`${path}/${file.name}`));

        return Promise.all(unlinkPromises);
    }
}
