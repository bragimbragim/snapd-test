import {Authorized, Get, JsonController, Post, QueryParam, Res} from 'routing-controllers';
import {Response} from 'express';
import {UserRole} from '../../client/shared/types/UserRole';
import {Log} from '../entity/Log';
import {getConnection, getRepository} from 'typeorm';
import {Time} from '../entity/Time';
import {Photo} from '../entity/Photo';

@JsonController('/api/log')
export class LogController {

    constructor() {
    }

    @Get('')
    @Authorized([UserRole.ADMIN])
    async getLog(
        @Res() response: Response,
        @QueryParam("limit") limit: number
    ) {
        return await getConnection()
            .createQueryBuilder(Log, 'logs')
            .orderBy('logs.time', 'DESC')
            .limit(limit)
            .getMany()
    }

    @Get('/tags')
    async getActiveTags() {
        return (await Time.find({timeLoose: null}))
            .filter(item => item.tag)
            .map(item => item.tag?.name);
    }

    @Post('/delete')
    async deleteLogs(@Res() response: Response) {
        try {
            await getRepository(Log).clear();
            return 'Logs were successfully deleted';
        } catch (err) {
            return response.status(500).send(err.message);
        }

    }

}
