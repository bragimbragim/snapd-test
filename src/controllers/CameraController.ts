import {Authorized, Body, Get, JsonController, Post, Res} from 'routing-controllers';
import {Response} from 'express';
import {ICameraConfig} from '../../globalTypes/ICameraConfig';
import {UserRole} from '../../client/shared/types/UserRole';
import {Camera} from '../lib/RaspiCam';
import {ConfigService} from '../services/ConfigService';
import {PhotoService} from '../services/PhotoService';
import {errorLogger} from '../utils/logger';

@JsonController('/api/camera')
export class CameraController {

    constructor(
        private camera: Camera,
        private configService: ConfigService,
        private photoService: PhotoService
    ) {
    }

    @Post('/reload')
    async reloadCamera(@Res() response: Response) {
        try {
            return await this.camera.init();
        } catch (e) {
            return response.status(500).send(e.message);
        }
    }

    @Post('/check')
    @Authorized([UserRole.ADMIN])
    async createDebugPhoto(@Res() response: Response) {

        const promise = new Promise(async (resolve, reject) => {
            await this.camera.open()
                .then(async () => {
                    const startTime = new Date().getTime();
                    await this.camera.raspistill.takeDebugPhoto()
                        .then((path: string) => {
                            const endTime = new Date().getTime();
                            return resolve({path, time: (endTime - startTime) / 1000})
                        })
                        .catch((err) => {
                            return reject(err)
                        })
                })
                .catch((err) => {
                    errorLogger.error(err.message, {event: 'CameraController: createCheckPhoto()'})
                    return reject(err)
                })
        });

        return promise
            .then((path) => path)
            .catch((err) => {
                return response.status(500).send(err.message)
            })
            .finally(() => this.camera.close())
    }

    @Get('/options')
    @Authorized([UserRole.ADMIN])
    getOptions(@Res() response: Response): ICameraConfig {
        return this.configService.getCurrConfig<ICameraConfig>('camera');
    }

    @Post('/options')
    @Authorized([UserRole.ADMIN])
    async setOptions(
        @Res() response: Response,
        @Body() body: ICameraConfig
    ) {
        try {

            this.configService.updateCustomConfig<ICameraConfig>('camera', body);
            this.camera.raspistill.updateOptions();

            return 'Camera settings have been successfully changed';
        } catch (e) {
            errorLogger.error(e.message, {event: 'CameraController: setOptions()'})
            return response.status(500).send('Something was wrong');
        }
    }
}
