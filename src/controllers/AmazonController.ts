import {Body, Get, JsonController, Post, Res} from 'routing-controllers'
import {Response} from 'express';
import {AmazonService} from '../services/AmazonService';
import {ConfigService} from '../services/ConfigService';
import {IAmazonConfig} from '../../globalTypes/IAmazonConfig';
import {IAmazonServiceData} from '../../globalTypes/IAmazonServiceData';
import {IServiceStatus} from '../../globalTypes/ServiceStatus';

@JsonController('/api/amazon')
export class AmazonController {

    constructor(
        private amazonService: AmazonService,
        private configService: ConfigService
    ) {
    }

    @Post('/reload')
    async reloadAmazonService(@Res() response: Response) {
        return await this.amazonService
            .init()
            .then((data: IServiceStatus) => data)
            .catch((err: Error) => response.status(500).send(err.message))
    }

    @Get('/service/data')
    getServiceData(@Res() response: Response): IAmazonServiceData {
        return {
            queueCount: this.amazonService.getQueueCount(),
            queueList: this.amazonService.getQueueList(),
            serviceStatus: this.amazonService.serviceStatus.get()
        };
    }

    @Get('/options')
    getOptions(@Res() response: Response) {
        return this.configService.getCurrConfig<IAmazonConfig>('amazon');
    }

    @Post('/options')
    setOptions(
        @Res() response: Response,
        @Body() body: IAmazonConfig
    ) {
        try {
            this.configService.updateCustomConfig<IAmazonConfig>('amazon', body);
            return response.status(200).send('Settings have been successfully applied');
        } catch (err) {
            return response.status(500).send(err.message);
        }
    }

    @Post('/queue/clear')
    async clearQueue(@Res() response: Response) {
        return this.amazonService.clearQueue()
            .then((data: string) => {
                return data;
            })
            .catch((err: Error) => {
                return response.status(500).send(err.message);
            });
    }
}
