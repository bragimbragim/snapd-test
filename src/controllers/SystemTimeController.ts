import {Authorized, Body, Get, JsonController, Post, Res} from 'routing-controllers';
import {Response} from 'express';
import {UserRole} from '../../client/shared/types/UserRole';
import {TimeService} from '../services/TimeService';
import {SystemDate} from '../../globalTypes/SystemDate';
import {errorLogger} from '../utils/logger';

@JsonController('/api/time')
export class SystemTimeController {

    constructor(private timeService: TimeService) {
    }

    @Get('')
    @Authorized([UserRole.ADMIN])
    async getTime(@Res() response: Response): Promise<string | Response<Error>> {
        try {
            return await this.timeService.getTime();
        } catch (err) {
            errorLogger.error(err.message, {event: 'TimeController: getTime()'})
            return response.status(500).send('RTC Problems');
        }
    }

    @Post('')
    @Authorized([UserRole.ADMIN])
    async setTime(
        @Res() response: Response,
        @Body() body: { date: SystemDate }
    ) {
        try {
            await this.timeService.updateDate(body.date);
            return 'Time was successfully updated. You should re-login';
        } catch (err) {
            errorLogger.error(err.message, {event: 'TimeController: setTime()'})
            return response.status(500).send('RTC Problems');
        }
    }
}
