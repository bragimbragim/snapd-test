import "reflect-metadata";
import {createConnection} from "typeorm";
import {useExpressServer, useContainer} from "routing-controllers";
import {Container} from 'typedi';
import {useSocketServer, useContainer as useSocketContainer} from 'socket-controllers';

import authChecker from './utils/authChecker';
import connectionOptions from '../ormconfig';
import {TypeormStore} from 'connect-typeorm';
import {Session} from './entity/Session';
import {AppService} from './services/AppService'; // Must be above controllers imports

import {VideoStreamController} from './controllers/sockets/VideoStreamController';
import {TimelapseController} from './controllers/sockets/TimelapseController';
import {MemorySocketController} from './controllers/sockets/MemorySocketController';

import {AuthController} from './controllers/AuthController';
import {GalleryController} from './controllers/GalleryController';
import {CameraController} from './controllers/CameraController';
import {VideoModuleController} from './controllers/VideoModuleController';
import {UserController} from './controllers/UserController';
import {ReaderController} from './controllers/ReaderController';
import {SystemTimeController} from './controllers/SystemTimeController';
import {LogController} from './controllers/LogController';
import {AmazonController} from './controllers/AmazonController';
import {MemoryController} from './controllers/MemoryController';
import {MqttController} from './controllers/MqttController';
import {WifiController} from './controllers/WifiController';

import {AppController} from './controllers/AppContoller';

import {config} from './config';
import {errorLogger} from './utils/logger';

const colors = require('colors/safe')
const semver = require('semver');

const express = require('express');
const session = require('express-session')

let app = require('express')()
const server = require("http").Server(app)
const io = require('socket.io')(server, config.socketIO)
const parseJson = require('parse-json');

process.title = 'Snapd';

createConnection(connectionOptions).then(async connection => {

    useContainer(Container);
    useSocketContainer(Container);

    const sessionRepository = connection.getRepository(Session);

    app.use(session({
        ...config.session,
        store: new TypeormStore(config.typeORMStore)
            .connect(sessionRepository)
    }));
    app.use(express.static('dist'));
    app.use('/photos/check', express.static('./photos/check'));
    app.use('/photos/timelapse', express.static('./photos/timelapse'));

    useSocketServer(io, {
        controllers: [
            VideoStreamController,
            TimelapseController,
            MemorySocketController
        ]
    });

    useExpressServer(app, {
        cors: config.cors,
        controllers: [
            AppController,
            AuthController,
            GalleryController,
            CameraController,
            VideoModuleController,
            UserController,
            ReaderController,
            SystemTimeController,
            LogController,
            AmazonController,
            MemoryController,
            MqttController,
            WifiController
        ],
        authorizationChecker: authChecker
    });

    const appService = Container.get(AppService);
    await appService.init();

    console.log('semver valid: ', semver.gte('1.11.1', '1.1.11'));
    console.log(parseJson(JSON.stringify({x: 1})));

    server.listen(config.port)
    console.log(colors.underline.blue(`\nServer start at port: ${config.port}`))


}).catch(err => {
    console.log(colors.red(`Global: ${err.message}`));
    errorLogger.error(err.message, {event: 'CreateConnection: catch'})
})



